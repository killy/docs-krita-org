# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-30 15:40+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<generated>:1
msgid "Mix with background color."
msgstr "Blanda med bakgrundsfärg."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:1
msgid "The Spray Brush Engine manual page."
msgstr "Manualsidan för det penselgränssnittet retuschspruta."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:16
msgid "Spray Brush Engine"
msgstr "Penselgränssnittet retuschspruta"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:11
msgid "Brush Engine"
msgstr "Penselgränssnitt"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:11
msgid "Airbrush"
msgstr "Retuschspruta"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:19
msgid ".. image:: images/icons/spraybrush.svg"
msgstr ".. image:: images/icons/spraybrush.svg"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:20
msgid "A brush that can spray particles around in its brush area."
msgstr "En pensel som kan spreja partiklar omkring sitt penselområde."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:23
msgid "Options"
msgstr "Alternativ"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:25
msgid ":ref:`option_spray_area`"
msgstr ":ref:`option_spray_area`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:26
msgid ":ref:`option_spray_shape`"
msgstr ":ref:`option_spray_shape`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:27
msgid ":ref:`option_brush_tip` (Used as particle if spray shape is not active)"
msgstr ""
":ref:`option_brush_tip` (Used as particle if spray shape is not active)"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:28
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:29
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:30
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:31
msgid ":ref:`option_shape_dyna`"
msgstr ":ref:`option_shape_dyna`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:32
msgid ":ref:`option_color_spray`"
msgstr ":ref:`option_color_spray`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:33
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:34
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:39
msgid "Spray Area"
msgstr "Spreja område"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:41
msgid "The area in which the particles are sprayed."
msgstr "Område där partiklarna sprejas."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:43
msgid "Diameter"
msgstr "Diameter"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:44
msgid "The size of the area."
msgstr "Områdets storlek."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:45
msgid "Aspect Ratio"
msgstr "Proportion"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:46
msgid "It's aspect ratio: 1.0 is fully circular."
msgstr "Dess proportion: 1,0 är fullständigt cirkulärt."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:47
msgid "Angle"
msgstr "Vinkel"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:48
msgid ""
"The angle of the spray size: works nice with aspect ratios other than 1.0."
msgstr "Sprejområdets vinkel: fungerar bra med proportioner skilda från 1,0."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:49
msgid "Scale"
msgstr "Skala"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:50
msgid "Scales the diameter up."
msgstr "Skalar upp diametern."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:52
msgid "Spacing"
msgstr "Mellanrum"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:52
msgid "Increases the spacing of the diameter's spray."
msgstr "Ökar avståndet på diameterns sprej."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:55
msgid "Particles"
msgstr "Partiklar"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:57
msgid "Count"
msgstr "Antal"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:58
msgid "Use a specified amount of particles."
msgstr "Använd ett angivet antal partiklar."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:59
msgid "Density"
msgstr "Täthet"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:60
msgid "Use a % amount of particles."
msgstr "Använd ett procentuellt antal partiklar."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:61
msgid "Jitter Movement"
msgstr "Skakningsrörelse"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:62
msgid "Jitters the spray area around for extra randomness."
msgstr "Skakar omkring sprejområdet för extra slumpmässighet."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:64
msgid "Gaussian Distribution"
msgstr "Normalfördelning"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:64
msgid ""
"Focuses the particles to paint in the center instead of evenly random over "
"the spray area."
msgstr ""
"Fokuserar partiklarna att måla i centrum istället för slumpmässigt över hela "
"sprejområdet."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:69
msgid "Spray Shape"
msgstr "Sprejform"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:71
msgid ""
"If activated, this will generate a special particle. If not, the brush-tip "
"will be the particle."
msgstr ""
"Om aktiverad, genererar det en specialpartikel. Om inte, är penselspetsen "
"partikeln."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:74
msgid "Can be..."
msgstr "Kan vara ..."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:76
msgid "Ellipse"
msgstr "Ellips"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:77
msgid "Rectangle"
msgstr "Rektangel"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:78
msgid "Anti-aliased Pixel"
msgstr "Kantutjämnad bildpunkt"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:79
msgid "Pixel"
msgstr "Bildpunkter"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:80
msgid "Shape"
msgstr "Form"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:80
msgid "Image"
msgstr "Bild"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:82
msgid "Width & Height"
msgstr "Bredd och höjd"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:83
msgid "Decides the width and height of the particle."
msgstr "Bestämmer partikelns bredd och höjd."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:84
msgid "Proportional"
msgstr "Proportionell"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:85
msgid "Locks Width & Height to be the same."
msgstr "Låser bredd och höjd att vara likadana."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:87
msgid "Texture"
msgstr "Struktur"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:87
msgid "Allows you to pick an image for the :guilabel:`Image shape`."
msgstr "Låter dig välja en bild för :guilabel:`Image shape`."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:92
msgid "Shape Dynamics"
msgstr "Formdynamik"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:94
msgid "Random Size"
msgstr "Slumpmässig storlek"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:95
msgid ""
"Randomizes the particle size between 1x1 px and the given size of the "
"particle in brush-tip or spray shape."
msgstr ""
"Gör partikelstorleken slumpmässig mellan 1x1 bildpunkter och partikelns "
"angivna storlek i penselspetsen eller sprejformen."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:96
msgid "Fixed Rotation"
msgstr "Fast rotation"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:97
msgid "Gives a fixed rotation to the particle to work from."
msgstr "Ger partikeln en fast rotation att arbeta från."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:98
msgid "Randomized Rotation"
msgstr "Slumpmässig rotation"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:99
msgid "Randomizes the rotation."
msgstr "Gör rotationen slumpmässig."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:100
msgid "Follow Cursor Weight"
msgstr "Följ markörvikt"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:101
msgid ""
"How much the pressure affects the rotation of the particles. At 1.0 and high "
"pressure it'll seem as if the particles are exploding from the middle."
msgstr ""
"Hur mycket trycket påverkar partiklarnas rotation. Vid 1,0 och högre tryck "
"verkar det som om partiklarna exploderar från mitten."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:103
msgid "Angle Weight"
msgstr "Vinkelvikt"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:103
msgid "How much the spray area angle affects the particle angle."
msgstr "Hur mycket sprejvinkeln påverkar partikelvinkeln."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:108
msgid "Color Options"
msgstr "Färgalternativ"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:110
msgid "Random HSV"
msgstr "Slumpmässig HSV"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:111
msgid ""
"Randomize the HSV with the strength of the sliders. The higher, the more the "
"color will deviate from the foreground color, with the direction indicating "
"clock or counter clockwise."
msgstr ""
"Gör HSV slumpmässig med skjutreglagens värden. Ju större, desto mer avviker "
"färgen från förgrundsfärgen, där riktningen indikerar medurs eller moturs."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:112
msgid "Random Opacity"
msgstr "Slumpmässig ogenomskinlighet"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:113
msgid "Randomizes the opacity."
msgstr "Gör genomskinligheten slumpmässig."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:114
msgid "Color Per Particle"
msgstr "Färg per partikel"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:115
msgid "Has the color options be per particle instead of area."
msgstr "Låter färgalternativen gälla per partikel istället för område."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:116
msgid "Sample Input Layer."
msgstr "Sampla indatalager."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:117
msgid ""
"Will use the underlying layer as reference for the colors instead of the "
"foreground color."
msgstr ""
"Använder underliggande lager som referens för färger istället för "
"förgrundsfärgen."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:118
msgid "Fill Background"
msgstr "Fyll bakgrund"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:119
msgid "Fills the area before drawing the particles with the background color."
msgstr "Fyller området innan partikeln ritas med bakgrundsfärgen."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:121
msgid ""
"Gives the particle a random color between foreground/input/random HSV and "
"the background color."
msgstr ""
"Ger partikeln en slumpmässig färg mellan förgrundsfärgen, inmatad färg, "
"slumpmässig HSV och bakgrundsfärgen."
