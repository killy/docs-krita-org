# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:24+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<generated>:1
msgid "Anti-aliasing"
msgstr "Kantutjämning"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: vänsterknapp"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: högerknapp"

#: ../../<rst_epilog>:70
msgid ""
".. image:: images/icons/polygonal_select_tool.svg\n"
"   :alt: toolselectpolygon"
msgstr ""
".. image:: images/icons/polygonal_select_tool.svg\n"
"   :alt: Polygonmarkeringsverktyg"

#: ../../reference_manual/tools/polygonal_select.rst:1
msgid "Krita's polygonal selection tool reference."
msgstr "Referens för Kritas polygonmarkeringsverktyg."

#: ../../reference_manual/tools/polygonal_select.rst:11
msgid "Tools"
msgstr "Verktyg"

#: ../../reference_manual/tools/polygonal_select.rst:11
msgid "Polygon"
msgstr "Polygon"

#: ../../reference_manual/tools/polygonal_select.rst:11
msgid "Selection"
msgstr "Markering"

#: ../../reference_manual/tools/polygonal_select.rst:11
msgid "Polygonal Selection"
msgstr "Polygonmarkering"

#: ../../reference_manual/tools/polygonal_select.rst:16
msgid "Polygonal Selection Tool"
msgstr "Polygonmarkeringsverktyg"

#: ../../reference_manual/tools/polygonal_select.rst:18
msgid "|toolselectpolygon|"
msgstr "|toolselectpolygon|"

#: ../../reference_manual/tools/polygonal_select.rst:20
msgid ""
"This tool, represented by a polygon with a dashed border, allows you to "
"make :ref:`selections_basics` of a polygonal area point by point. Click "
"where you want each point of the Polygon to be. Double click to end your "
"polygon and finalize your selection area."
msgstr ""
"Verktyget, som representeras av en polygon med en streckad kant, låter dig "
"göra :ref:`selections_basics` för ett polygonområde punkt för punkt. Klicka "
"där varje punkt i polygonen ska vara. Dubbelklicka för att avsluta polygonen "
"och slutföra markeringsområdet."

#: ../../reference_manual/tools/polygonal_select.rst:24
msgid "Hotkeys and Sticky keys"
msgstr "Snabbtangenter och klistriga tangenter"

#: ../../reference_manual/tools/polygonal_select.rst:26
msgid ""
":kbd:`R` sets the selection to 'replace' in the tool options, this is the "
"default mode."
msgstr ""
":kbd:`R` ställer in markeringen till 'ersätt' i verktygsalternativen, vilket "
"är standardinställningen."

#: ../../reference_manual/tools/polygonal_select.rst:27
msgid ":kbd:`A` sets the selection to 'add' in the tool options."
msgstr ":kbd:`A` ställer in markeringen till 'addera' i verktygsalternativen."

#: ../../reference_manual/tools/polygonal_select.rst:28
msgid ":kbd:`S` sets the selection to 'subtract' in the tool options."
msgstr ""
":kbd:`A` ställer in markeringen till 'subtrahera' i verktygsalternativen."

#: ../../reference_manual/tools/polygonal_select.rst:29
msgid ""
":kbd:`Shift +` |mouseleft| sets the subsequent selection to 'add'. You can "
"release the :kbd:`Shift` key while dragging, but it will still be set to "
"'add'. Same for the others."
msgstr ""
":kbd:`Skift +` vänster musknapp in markeringen till 'addera'. Man kan släppa "
"upp tangenten :kbd:`Skift` medan man drar, och den ställs ändå in till 'lägg "
"till'. Samma sak för de andra."

#: ../../reference_manual/tools/polygonal_select.rst:30
msgid ":kbd:`Alt +` |mouseleft| sets the subsequent selection to 'subtract'."
msgstr ""
":kbd:`Alt +` vänster musknapp ställer in efterföljande markering till "
"'subtrahera'."

#: ../../reference_manual/tools/polygonal_select.rst:31
msgid ":kbd:`Ctrl +` |mouseleft| sets the subsequent selection to 'replace'."
msgstr ""
":kbd:`Ctrl +` vänster musknapp ställer in efterföljande markering till "
"'ersätt'."

#: ../../reference_manual/tools/polygonal_select.rst:32
msgid ""
":kbd:`Shift + Alt +` |mouseleft| sets the subsequent selection to "
"'intersect'."
msgstr ""
":kbd:`Shift + Alt +` vänster musknapp ställer in efterföljande markering "
"till 'snitt'."

#: ../../reference_manual/tools/polygonal_select.rst:36
msgid "Hovering over a selection allows you to move it."
msgstr "Att hålla musen över en markering göt det möjlig att flytta den."

#: ../../reference_manual/tools/polygonal_select.rst:37
msgid ""
"|mouseright| will open up a selection quick menu with amongst others the "
"ability to edit the selection."
msgstr ""
"Att klicka med höger musknapp visar en snabbvalsmeny med bland annat "
"möjlighet att redigera markeringen."

#: ../../reference_manual/tools/polygonal_select.rst:41
msgid ""
"You can switch the behavior of the :kbd:`Alt` key to use the :kbd:`Ctrl` key "
"instead by toggling the switch in the :ref:`general_settings`."
msgstr ""
"Man kan ändra beteende hos tangenten :kbd:`Alt` så att tangenten :kbd:`Ctrl` "
"används istället genom att ställa om inställningen under :ref:"
"`general_settings`."

#: ../../reference_manual/tools/polygonal_select.rst:44
msgid "Tool Options"
msgstr "Verktygsalternativ"

#: ../../reference_manual/tools/polygonal_select.rst:47
msgid ""
"This toggles whether or not to give selections feathered edges. Some people "
"prefer hard-jagged edges for their selections."
msgstr ""
"Ändrar om markeringar ska få vävkanter. Vissa människor föredrar skarpa "
"naggade kanter för sina markeringar."
