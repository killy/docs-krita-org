# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-17 20:07+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/filters.rst:5
msgid "Filters"
msgstr "Filter"

#: ../../reference_manual/filters.rst:7
msgid ""
"Filters are little scripts or operations you can run on your drawing. You "
"can visualize them as real-world camera filters that can make a photo darker "
"or blurrier. Or perhaps like a coffee filter, where only water and coffee "
"gets through, and the ground coffee stays behind."
msgstr ""
"Filter är små skript eller operationer som kan utföras på målningen. Man kan "
"föreställa sig dem som kamerafilter i verkligheten som kan göra ett foto "
"mörkare eller suddigare, eller kanske som ett kaffefilter, där bara vatten "
"och kaffe kommer igenom, och kaffepulvret blir kvar."

#: ../../reference_manual/filters.rst:9
msgid ""
"Filters are unique to digital painting in terms of complexity, and their "
"part of the painting pipeline. Some artists only use filters to adjust their "
"colors a little. Others, using Filter Layers and Filter Masks use them to "
"dynamically update a part of an image to be filtered. This way, they can "
"keep the original underneath without changing the original image. This is a "
"part of a technique called 'non-destructive' editing."
msgstr ""
"Filter är unika i digitalmålning med avseende på komplexitet, och deras del "
"i målningsprocessen. Vissa konstnärer använder bara filter för att justera "
"sina färger lite grand. Andra använder filterlager och filtermasker för att "
"dynamiskt uppdatera delar av en bild som ska filtreras. På så sätt kan de "
"behålla originalet nedanför utan att ändra originalbilden. Det är en del av "
"en teknik som kallas 'icke-destruktiv' redigering."

#: ../../reference_manual/filters.rst:11
msgid ""
"Filters can be accessed via the :guilabel:`Filters` menu. Krita has two "
"types of filters: Internal and G'MIC filters."
msgstr ""
"Filter kan kommas åt via menyn :guilabel:`Filter`. Krita har två typer av "
"filter: interna och G'MIC filter."

#: ../../reference_manual/filters.rst:13
msgid ""
"Internal filters are often multithreaded, and can thus be used with the "
"filter brush or the adjustment filters."
msgstr ""
"Interna filter är ofta flertrådiga, och kan sålunda användas med "
"filterpenseln eller justeringsfiltren."
