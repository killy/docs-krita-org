# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-06 03:34+0200\n"
"PO-Revision-Date: 2019-09-03 18:41+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/blending_modes/binary.rst:1
msgid "Page about the binary blending modes in Krita:"
msgstr "Sida om de binära blandningslägena i Krita:"

#: ../../reference_manual/blending_modes/binary.rst:10
#: ../../reference_manual/blending_modes/binary.rst:14
msgid "Binary"
msgstr "Binär"

#: ../../reference_manual/blending_modes/binary.rst:16
msgid ""
"Binary modes are special class of blending modes which utilizes binary "
"operators for calculations. Binary modes are unlike every other blending "
"modes as these modes have a fractal attribute with falloff similar to other "
"blending modes. Binary modes can be used for generation of abstract art "
"using layers with very smooth surface. All binary modes have capitalized "
"letters to distinguish themselves from other blending modes."
msgstr ""
"Binärlägen är särskild klass av blandningslägen, som använder binära"
" operatorer för beräkningar. Binärlägen är annorlunda än alla andra"
" blandningslägen, eftersom de har en fraktalegenskap med fall som liknar"
" andra blandningslägen. Binärlägen kan användas för att skapa abstrakt konst"
" genom att använda lager med mycket jämn yta. Alla binärlägen använder stora"
" bokstäver för att skilja dem från andra blandningslägen."

#: ../../reference_manual/blending_modes/binary.rst:18
msgid ""
"To clarify on how binary modes works, convert decimal values to binary "
"values, then treat 1 or 0 as T or F respectively, and use binary operation "
"to get the end result, and then convert the result back to decimal."
msgstr ""
"För att klargöra hur binärlägen fungerar, konvertera decimala värden till"
" binära värden, och behandla sedan 1 och 0 som S eller F, och använd binära"
" operatorer för att få slutresultatet, och konvertera sedan resultatet"
" tillbaka till decimalt."

#: ../../reference_manual/blending_modes/binary.rst:22
msgid ""
"Binary blending modes do not work on float images or negative numbers! So, "
"don't report bugs about using binary modes on unsupported color space."
msgstr ""
"Binära blandningslägen fungerar inte för flyttalsbilder eller negativa tal."
" Rapportera alltså inte fel om att använda binärlägen för färgrymder som inte"
" stöds."

#: ../../reference_manual/blending_modes/binary.rst:24
#: ../../reference_manual/blending_modes/binary.rst:28
msgid "AND"
msgstr "OCH"

#: ../../reference_manual/blending_modes/binary.rst:30
msgid ""
"Performs the AND operation for the base and blend layer. Similar to multiply "
"blending mode."
msgstr ""
"Utför en OCH-operation för bas- och blandningslagret. Liknar blandningsläget"
" multiplikation."

#: ../../reference_manual/blending_modes/binary.rst:35
msgid ".. image:: images/blending_modes/binary/Blend_modes_AND_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_AND_map.png"

#: ../../reference_manual/blending_modes/binary.rst:35
#: ../../reference_manual/blending_modes/binary.rst:40
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **AND**."
msgstr "Vänster: **Baslager**. Mitten: **Blandningslager**. Höger: **OCH**."

#: ../../reference_manual/blending_modes/binary.rst:40
msgid ""
".. image:: images/blending_modes/binary/Blending_modes_AND_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_AND_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:41
#: ../../reference_manual/blending_modes/binary.rst:46
msgid "CONVERSE"
msgstr "MOTSATS"

#: ../../reference_manual/blending_modes/binary.rst:48
msgid ""
"Performs the inverse of IMPLICATION operation for the base and blend layer. "
"Similar to screen mode with blend layer and base layer inverted."
msgstr ""
"Utför inversen av en FÖLJD-operation för bas- och blandningslagret. Liknar"
" skärmläget med blandningslager och baslager inverterade."

#: ../../reference_manual/blending_modes/binary.rst:53
msgid ".. image:: images/blending_modes/binary/Blend_modes_CONVERSE_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_CONVERSE_map.png"

#: ../../reference_manual/blending_modes/binary.rst:53
#: ../../reference_manual/blending_modes/binary.rst:58
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **CONVERSE**."
msgstr ""
"Vänster: **Baslager**. Mitten: **Blandningslager**. Höger: **MOTSATS**."

#: ../../reference_manual/blending_modes/binary.rst:58
msgid ""
".. image:: images/blending_modes/binary/Blending_modes_CONVERSE_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_CONVERSE_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:60
#: ../../reference_manual/blending_modes/binary.rst:64
msgid "IMPLICATION"
msgstr "FÖLJD"

#: ../../reference_manual/blending_modes/binary.rst:66
msgid ""
"Performs the IMPLICATION operation for the base and blend layer. Similar to "
"screen mode with base layer inverted."
msgstr ""
"Utför en FÖLJD-operation för bas- och blandningslagret. Liknar skärmläget med"
" baslagret inverterat."

#: ../../reference_manual/blending_modes/binary.rst:71
msgid ".. image:: images/blending_modes/binary/Blend_modes_IMPLIES_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_IMPLIES_map.png"

#: ../../reference_manual/blending_modes/binary.rst:71
#: ../../reference_manual/blending_modes/binary.rst:76
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **IMPLICATION**."
msgstr "Vänster: **Baslager**. Mitten: **Blandningslager**. Höger: **FÖLJD**."

#: ../../reference_manual/blending_modes/binary.rst:76
msgid ""
".. image:: images/blending_modes/binary/Blending_modes_IMPLIES_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_IMPLIES_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:78
#: ../../reference_manual/blending_modes/binary.rst:82
msgid "NAND"
msgstr "ICKE OCH"

#: ../../reference_manual/blending_modes/binary.rst:84
msgid ""
"Performs the inverse of AND operation for base and blend layer. Similar to "
"the inverted multiply mode."
msgstr ""
"Utför en invers OCH-operation för bas- och blandningslagret. Liknar"
" blandningsläget inverterad multiplikation."

#: ../../reference_manual/blending_modes/binary.rst:89
msgid ".. image:: images/blending_modes/binary/Blend_modes_NAND_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_NAND_map.png"

#: ../../reference_manual/blending_modes/binary.rst:89
#: ../../reference_manual/blending_modes/binary.rst:94
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **NAND**."
msgstr ""
"Vänster: **Baslager**. Mitten: **Blandningslager**. Höger: **ICKE OCH**."

#: ../../reference_manual/blending_modes/binary.rst:94
msgid ""
".. image:: images/blending_modes/binary/Blending_modes_NAND_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_NAND_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:96
#: ../../reference_manual/blending_modes/binary.rst:100
msgid "NOR"
msgstr "ICKE ELLER"

#: ../../reference_manual/blending_modes/binary.rst:102
msgid ""
"Performs the inverse of OR operation for base and blend layer. Similar to "
"the inverted screen mode."
msgstr ""
"Utför en invers ELLER-operation för bas- och blandningslagret. Liknar det"
" inverterade skärmläget."

#: ../../reference_manual/blending_modes/binary.rst:107
msgid ".. image:: images/blending_modes/binary/Blend_modes_NOR_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_NOR_map.png"

#: ../../reference_manual/blending_modes/binary.rst:107
#: ../../reference_manual/blending_modes/binary.rst:112
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **NOR**."
msgstr ""
"Vänster: **Baslager**. Mitten: **Blandningslager**. Höger: **ICKE ELLER**."

#: ../../reference_manual/blending_modes/binary.rst:112
msgid ""
".. image:: images/blending_modes/binary/Blending_modes_NOR_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_NOR_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:114
#: ../../reference_manual/blending_modes/binary.rst:118
msgid "NOT CONVERSE"
msgstr "ICKE MOTSATS"

#: ../../reference_manual/blending_modes/binary.rst:120
msgid ""
"Performs the inverse of CONVERSE operation for base and blend layer. Similar "
"to the multiply mode with base layer and blend layer inverted."
msgstr ""
"Utför inversen av en MOTSATS-operation för bas- och blandningslagret. Liknar"
" multiplikationsläget med baslager och blandningslager inverterade."

#: ../../reference_manual/blending_modes/binary.rst:125
msgid ""
".. image:: images/blending_modes/binary/Blend_modes_NOT_CONVERSE_map.png"
msgstr ""
".. image:: images/blending_modes/binary/Blend_modes_NOT_CONVERSE_map.png"

#: ../../reference_manual/blending_modes/binary.rst:125
#: ../../reference_manual/blending_modes/binary.rst:130
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **NOT CONVERSE**."
msgstr ""
"Vänster: **Baslager**. Mitten: **Blandningslager**. Höger: **ICKE MOTSATS**."

#: ../../reference_manual/blending_modes/binary.rst:130
msgid ""
".. image:: images/blending_modes/binary/"
"Blending_modes_NOT_CONVERSE_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/"
"Blending_modes_NOT_CONVERSE_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:132
#: ../../reference_manual/blending_modes/binary.rst:136
msgid "NOT IMPLICATION"
msgstr "ICKE FÖLJD"

#: ../../reference_manual/blending_modes/binary.rst:138
msgid ""
"Performs the inverse of IMPLICATION operation for base and blend layer. "
"Similar to the multiply mode with the blend layer inverted."
msgstr ""
"Utför inversen av en FÖLJD-operation för bas- och blandningslagret. Liknar"
" multiplikationsläget med blandningslagret inverterat."

#: ../../reference_manual/blending_modes/binary.rst:143
msgid ""
".. image:: images/blending_modes/binary/Blend_modes_NOT_IMPLICATION_map.png"
msgstr ""
".. image:: images/blending_modes/binary/Blend_modes_NOT_IMPLICATION_map.png"

#: ../../reference_manual/blending_modes/binary.rst:143
#: ../../reference_manual/blending_modes/binary.rst:148
msgid ""
"Left: **Base Layer**. Middle: **Blend Layer**. Right: **NOT IMPLICATION**."
msgstr ""
"Vänster: **Baslager**. Mitten: **Blandningslager**. Höger: **ICKE FÖLJD**."

#: ../../reference_manual/blending_modes/binary.rst:148
msgid ""
".. image:: images/blending_modes/binary/"
"Blending_modes_NOT_IMPLICATION_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_NOT_IMPLICATION_Gradien"
"ts.png"

#: ../../reference_manual/blending_modes/binary.rst:150
#: ../../reference_manual/blending_modes/binary.rst:154
msgid "OR"
msgstr "ELLER"

#: ../../reference_manual/blending_modes/binary.rst:156
msgid ""
"Performs the OR operation for base and blend layer. Similar to screen mode."
msgstr ""
"Utför ELLER-operation för bas- och blandningslagret. Liknar skärmläget."

#: ../../reference_manual/blending_modes/binary.rst:161
msgid ".. image:: images/blending_modes/binary/Blend_modes_OR_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_OR_map.png"

#: ../../reference_manual/blending_modes/binary.rst:161
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **OR**."
msgstr "Vänster: **Baslager**. Mitten: **Blandningslager**. Höger: **ELLER**."

#: ../../reference_manual/blending_modes/binary.rst:166
msgid ".. image:: images/blending_modes/binary/Blending_modes_OR_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_OR_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:166
#: ../../reference_manual/blending_modes/binary.rst:179
#: ../../reference_manual/blending_modes/binary.rst:184
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **XOR**."
msgstr ""
"Vänster: **Baslager**. Mitten: **Blandningslager**. Höger: **EXKLUSIV ELLER**."

#: ../../reference_manual/blending_modes/binary.rst:168
#: ../../reference_manual/blending_modes/binary.rst:172
msgid "XOR"
msgstr "EXKLUSIV ELLER"

#: ../../reference_manual/blending_modes/binary.rst:174
msgid ""
"Performs the XOR operation for base and blend layer. This mode has a special "
"property that if you duplicate the blend layer twice, you get the base layer."
msgstr ""
"Utför EXKLUSIV ELLER-operation för bas- och blandningslagret. Läget har den"
" speciella egenskapen att om blandningslagret dupliceras två gånger, får man"
" baslagret."

#: ../../reference_manual/blending_modes/binary.rst:179
msgid ".. image:: images/blending_modes/binary/Blend_modes_XOR_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_XOR_map.png"

#: ../../reference_manual/blending_modes/binary.rst:184
msgid ""
".. image:: images/blending_modes/binary/Blending_modes_XOR_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_XOR_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:186
#: ../../reference_manual/blending_modes/binary.rst:190
msgid "XNOR"
msgstr "ICKE EXKLUSIV ELLER"

#: ../../reference_manual/blending_modes/binary.rst:192
msgid ""
"Performs the XNOR operation for base and blend layer. This mode has a "
"special property that if you duplicate the blend layer twice, you get the "
"base layer."
msgstr ""
"Utför ICKE EXKLUSIV ELLER-operation för bas- och blandningslagret. Läget har"
" den speciella egenskapen att om blandningslagret dupliceras två gånger, får"
" man baslagret."

#: ../../reference_manual/blending_modes/binary.rst:197
msgid ".. image:: images/blending_modes/binary/Blend_modes_XNOR_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_XNOR_map.png"

#: ../../reference_manual/blending_modes/binary.rst:197
#: ../../reference_manual/blending_modes/binary.rst:202
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **XNOR**."
msgstr ""
"Vänster: **Baslager**. Mitten: **Blandningslager**. Höger: **ICKE EXKLUSIV"
" ELLER**."

#: ../../reference_manual/blending_modes/binary.rst:202
msgid ""
".. image:: images/blending_modes/binary/Blending_modes_XNOR_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_XNOR_Gradients.png"


