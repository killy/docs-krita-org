# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 11:32+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/dockers/digital_color_mixer.rst:1
msgid "Overview of the digital color mixer docker."
msgstr "Översikt av den digitala färgblandarpanelen."

#: ../../reference_manual/dockers/digital_color_mixer.rst:11
#: ../../reference_manual/dockers/digital_color_mixer.rst:16
msgid "Digital Color Mixer"
msgstr "Digital färgblandare"

#: ../../reference_manual/dockers/digital_color_mixer.rst:11
msgid "Color"
msgstr "Färg"

#: ../../reference_manual/dockers/digital_color_mixer.rst:11
msgid "Color Mixing"
msgstr "Färgblandning"

#: ../../reference_manual/dockers/digital_color_mixer.rst:11
msgid "Color Selector"
msgstr "Färgväljare"

#: ../../reference_manual/dockers/digital_color_mixer.rst:19
msgid ".. image:: images/dockers/Krita_Digital_Color_Mixer_Docker.png"
msgstr ".. image:: images/dockers/Krita_Digital_Color_Mixer_Docker.png"

#: ../../reference_manual/dockers/digital_color_mixer.rst:20
msgid "This docker allows you to do simple mathematical color mixing."
msgstr "Panelen låter dig göra enkel matematisk färgblandning."

#: ../../reference_manual/dockers/digital_color_mixer.rst:22
msgid "It works as follows:"
msgstr "Det fungerar på följande sätt:"

#: ../../reference_manual/dockers/digital_color_mixer.rst:24
msgid "You have on the left side the current color."
msgstr "På vänster sida finns aktuell färg."

#: ../../reference_manual/dockers/digital_color_mixer.rst:26
msgid ""
"Next to that there are six columns. Each of these columns consists of three "
"rows: The lowest row is the color that you are mixing the current color "
"with. Ticking this button allows you to set a different color using a "
"palette and the mini-color wheel. The slider above this mixing color "
"represent the proportions of the mixing color and the current color. The "
"higher the slider, the less of the mixing color will be used in mixing. "
"Finally, the result color. Clicking this will change your current color to "
"the result color."
msgstr ""
"Intill den finns sex kolumner. Var och en av kolumnerna består av tre rader: "
"Den understa raden är färgen som den aktuella färgen blandas med. Att klicka "
"på knappen gör det möjligt att ställa in en annan färg med användning av en "
"palett och minifärghjulet. Skjutreglaget ovanför blandningsfärgen "
"representerar proportionen av blandningsfärgen och den aktuella färgen. Ju "
"högre reglaget är, desto mindre av blandningsfärgen används i blandningen. "
"Slutligen, resultatfärgen. Att klicka på den ändrar aktuell färg till "
"resultatfärgen."
