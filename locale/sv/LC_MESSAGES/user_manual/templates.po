# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-10 21:48+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../user_manual/templates.rst:1
msgid "How to use document templates in Krita."
msgstr "Hur dokumentmallar används i Krita."

#: ../../user_manual/templates.rst:12
msgid "Template"
msgstr "Mall"

#: ../../user_manual/templates.rst:17
msgid "Templates"
msgstr "Mallar"

#: ../../user_manual/templates.rst:20
msgid ".. image:: images/Krita_New_File_Template_A.png"
msgstr ".. image:: images/Krita_New_File_Template_A.png"

#: ../../user_manual/templates.rst:21
msgid ""
"Templates are just .kra files which are saved in a special location so it "
"can be pulled up by Krita quickly. This is like the :guilabel:`Open Existing "
"Document and Untitled Document` but then with a nicer place in the UI."
msgstr ""
"Mallar är bara .kra-filer som sparas på ett speciellt ställe så att Krita "
"snabbt kan ta fram dem. Det är som :guilabel:`Öppna befintligt dokument som "
"namnlöst dokument` men med en bättre plats i användargränssnittet."

#: ../../user_manual/templates.rst:23
msgid ""
"You can make your own template file from any .kra file, by using :guilabel:"
"`create template from image` in the file menu. This will add your current "
"document as a new template, including all its properties along with the "
"layers and layer contents."
msgstr ""
"Man kan skapa sin egen mallfil från vilken .kra-fil som helst genom att "
"använda :guilabel:`Skapa mall från bild` i arkivmenyn. Det lägger till det "
"aktuella dokumentet som en ny mall, inklusive alla dess egenskaper "
"tillsammans med lagren och lagerinnehållet."

#: ../../user_manual/templates.rst:25
msgid "We have the following defaults:"
msgstr "Vi har följande standardmallar:"

#: ../../user_manual/templates.rst:28
msgid "Comic Templates"
msgstr "Seriemallar"

#: ../../user_manual/templates.rst:30
msgid ""
"These templates are specifically designed for you to just get started with "
"drawing comics. The comic template relies on a system of vectors and clones "
"of those vector layers which automatically reflect any changes made to the "
"vector layers. In between these two, you can draw your picture, and not fear "
"them drawing over the panel. Use :guilabel:`Inherit Alpha` to clip the "
"drawing by the panel."
msgstr ""
"Dessa mallar är särskilt konstruerade för att man ska komma igång med att "
"rita serier. Serimallar förlitar sig på ett system av vektorer och "
"dubbletter av vektorlagren som automatiskt speglar alla ändringar som görs "
"av vektorlagren. Man kan rita sin bild mellan de båda, och inte vara rädd "
"för att rita över rutan. Använd :guilabel:`Ärv alfa` för att beskära "
"teckningen till rutan."

#: ../../user_manual/templates.rst:32
msgid "European Bande Desinée Template."
msgstr "Europeisk bande dessinée mall."

#: ../../user_manual/templates.rst:33
msgid ""
"This one is reminiscent of the system used by for example TinTin or Spirou "
"et Fantasio. These panels focus on wide images, and horizontal cuts."
msgstr ""
"Denna påminner om systemet som exempelvis används av Tintin eller Spirou. "
"Rutorna fokuserar på breda bilder och horisontella skärningar."

#: ../../user_manual/templates.rst:34
msgid "US-style comics Template."
msgstr "Seriemall med amerikansk stil"

#: ../../user_manual/templates.rst:35
msgid ""
"This one is reminiscent of old DC and Marvel comics, such as Batman or "
"Captain America. Nine images for quick story progression."
msgstr ""
"Denna påminner om gamla serier från DC och Marvel, såsom Batman eller "
"Captain America. Nio rutor för att berättelsen snabbt ska gå framåt."

#: ../../user_manual/templates.rst:36
msgid "Manga Template."
msgstr "Manga-mall."

#: ../../user_manual/templates.rst:37
msgid ""
"This one is based on Japanese comics, and focuses on a thin vertical gutter "
"and a thick horizontal gutter, ensuring that the reader finished the "
"previous row before heading to the next."
msgstr ""
"Denna är baserad på japanska serier, och fokuserar på ett tunt vertikalt "
"mellanrum och ett brett horisontellt mellanrum, vilket säkerställer att "
"läsarna är klara med föregående rad innan de fortsätter med nästa."

#: ../../user_manual/templates.rst:39
msgid "Waffle Iron Grid"
msgstr "Våffeljärnsmönster"

#: ../../user_manual/templates.rst:39
msgid "12 little panels at your disposal."
msgstr "Tolv små rutor till ditt förfogande."

#: ../../user_manual/templates.rst:42
msgid "Design Templates"
msgstr "Designmallar"

#: ../../user_manual/templates.rst:44
msgid ""
"These are templates for design and have various defaults with proper ppi at "
"your disposal:"
msgstr ""
"De är mallar för design, och har diverse standardvärden med riktiga b/t till "
"ditt förfogande."

#: ../../user_manual/templates.rst:46
msgid "Cinema 16:10"
msgstr "Biograf 16:10"

#: ../../user_manual/templates.rst:47
msgid "Cinema 2.93:1"
msgstr "Biograf 2,93:1"

#: ../../user_manual/templates.rst:48
msgid "Presentation A3-landscape"
msgstr "Presentation liggande A3"

#: ../../user_manual/templates.rst:49
msgid "Presentation A4 portrait"
msgstr "Presentation stående A4"

#: ../../user_manual/templates.rst:50
msgid "Screen 4:3"
msgstr "Skärm 4:3"

#: ../../user_manual/templates.rst:51
msgid "Web Design"
msgstr "Webbdesign"

#: ../../user_manual/templates.rst:54
msgid "DSLR templates"
msgstr "Mallar för digital spegelreflexkamera"

#: ../../user_manual/templates.rst:56
msgid "These have some default size for photos"
msgstr "De har några standardstorlekar för foton"

#: ../../user_manual/templates.rst:58
msgid "Canon 55D"
msgstr "Canon 55D"

#: ../../user_manual/templates.rst:59
msgid "Canon 5DMK3"
msgstr "Canon 5DMK3"

#: ../../user_manual/templates.rst:60
msgid "Nikon D3000"
msgstr "Nikon D3000"

#: ../../user_manual/templates.rst:61
msgid "Nikon D5000"
msgstr "Nikon D5000"

#: ../../user_manual/templates.rst:62
msgid "Nikon D7000"
msgstr "Nikon D7000"

#: ../../user_manual/templates.rst:65
msgid "Texture Templates"
msgstr "Strukturmallar"

#: ../../user_manual/templates.rst:67
msgid "These are for making 3D textures, and are between 1024, to 4092."
msgstr ""
"De är till för att skapa tredimensionella strukturer, och är mellan 1024 och "
"4092."
