# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-30 20:36+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../general_concepts/file_formats/file_jpeg.rst:1
msgid "The JPEG file format as exported by Krita."
msgstr "JPEG-filformatet som exporteras av Krita."

#: ../../general_concepts/file_formats/file_jpeg.rst:10
msgid "jpeg"
msgstr "JPEG"

#: ../../general_concepts/file_formats/file_jpeg.rst:10
msgid "jpg"
msgstr "jpg"

#: ../../general_concepts/file_formats/file_jpeg.rst:10
msgid "*.jpg"
msgstr "*.jpg"

#: ../../general_concepts/file_formats/file_jpeg.rst:16
msgid "\\*.jpg"
msgstr "\\*.jpg"

#: ../../general_concepts/file_formats/file_jpeg.rst:18
msgid ""
"``.jpg``, ``.jpeg`` or ``.jpeg2000`` are a family of file-formats designed "
"to encode photographs."
msgstr ""
"Familjen av filformat ``.jpg``, ``.jpeg`` eller ``.jpeg2000`` är konstruerad "
"för att koda fotografier."

#: ../../general_concepts/file_formats/file_jpeg.rst:20
msgid ""
"Photographs have the problem that they have a lot of little gradients, which "
"means that you cannot index the file like you can with :ref:`file_gif` and "
"expect the result to look good. What JPEG instead does is that it converts "
"the file to a perceptual color space (:ref:`YCrCb <model_ycrcb>`), and then "
"compresses the channels that encode the colors, while keeping the channel "
"that holds information about the relative lightness uncompressed. This works "
"really well because human eye-sight is not as sensitive to colorfulness as "
"it is to relative lightness. JPEG also uses other :ref:`lossy "
"<lossy_compression>` compression techniques, like using cosine waves to "
"describe image contrasts."
msgstr ""
"Fotografier har problemet att de har många små toningar, vilket betyder att "
"man inte kan indexera filen som man kan med :ref:`file_gif` och förvänta sig "
"att resultatet ser bra ut. Vad JPEG gör istället är att konvertera filen "
"till en perceptuell färgrymd (:ref:`YCrCb <model_ycrcb>`), och komprimerar "
"därefter kanalerna som kodar färgerna, medan kanalen som innehåller "
"information om den relativa ljusstyrkan okomprimerad. Det fungerar riktigt "
"bra eftersom det mänskliga ögat inte är så känsligt för färginnehåll som "
"relativ ljusstyrka. Andra :ref:`lossy <lossy_compression>` "
"komprimeringstekniker används också av JPEG, såsom att använda cosinusvågor "
"för att beskriva bildkontraster."

#: ../../general_concepts/file_formats/file_jpeg.rst:22
msgid ""
"However, it does mean that JPEG should be used in certain cases. For images "
"with a lot of gradients, like full scale paintings, JPEG performs better "
"than :ref:`file_png` and :ref:`file_gif`."
msgstr ""
"Dock betyder det inte att JPEG ska användas i vissa fall. För bilder med "
"många toningar, som fullskalemålningar, ger JPEG bättre prestanda än :ref:"
"`file_png` och :ref:`file_gif`."

#: ../../general_concepts/file_formats/file_jpeg.rst:24
msgid ""
"But for images with a lot of sharp contrasts, like text and comic book "
"styles, PNG is a much better choice despite a larger file size. For "
"grayscale images, :ref:`file_png` and :ref:`file_gif` will definitely be "
"more efficient."
msgstr ""
"Men för bilder många skarpa kontraster, som text och serietidningsstilar, är "
"PNG ett mycket bättre val trots en större filstorlek. För gråskalebilder är :"
"ref:`file_png` och :ref:`file_gif` definitivt mycket effektivare."

#: ../../general_concepts/file_formats/file_jpeg.rst:26
msgid ""
"Because JPEG uses lossy compression, it is not advised to save over the same "
"JPEG multiple times. The lossy compression will cause the file to reduce in "
"quality each time you save it. This is a fundamental problem with lossy "
"compression methods. Instead use a lossless file format, or a working file "
"format while you are working on the image."
msgstr ""
"Eftersom JPEG använder destruktiv komprimering är det inte lämpligt att "
"spara samma JPEG flera gånger. Den destruktiva komprimeringen gör att filens "
"kvalitet försämras varje gång den sparas. Det är ett fundamentalt problem "
"med destruktiva komprimeringsmetoder. Använd istället ett förlustfritt "
"filformat, eller ett arbetsfilformat medan du arbetar med bilden."
