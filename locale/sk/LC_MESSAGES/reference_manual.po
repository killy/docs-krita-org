# translation of docs_krita_org_reference_manual.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-13 10:17+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual.rst:5
msgid "Reference Manual"
msgstr "Referenčná príručka"

#: ../../reference_manual.rst:7
msgid "A quick run-down of all of the tools that are available"
msgstr "Rýchly prehľad všetkých dostupných nástrojov"

#: ../../reference_manual.rst:9
msgid "Contents:"
msgstr "Obsah:"
