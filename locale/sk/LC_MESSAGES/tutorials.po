# translation of docs_krita_org_tutorials.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_tutorials\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-13 12:28+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../tutorials.rst:5
msgid "Tutorials and Howto's"
msgstr "Cvičenia a návody"

#: ../../tutorials.rst:7
msgid ""
"Learn through developer and user generated tutorials to see Krita in action."
msgstr ""
"Učte sa pomocou cvičení, ktoré vytvorili vývojári a používatelia a pozrite "
"si Kritu v akcii."

#: ../../tutorials.rst:9
msgid "Contents:"
msgstr "Obsah:"
