# translation of docs_krita_org_reference_manual___dockers___snap_settings_docker.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___dockers___snap_settings_docker\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-04-02 10:12+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<generated>:1
msgid "Guides"
msgstr "Vodiace čiary"

#: ../../reference_manual/dockers/snap_settings_docker.rst:1
msgid "Overview of the snap settings docker."
msgstr ""

#: ../../reference_manual/dockers/snap_settings_docker.rst:11
msgid "Snap"
msgstr ""

#: ../../reference_manual/dockers/snap_settings_docker.rst:16
msgid "Snap Settings"
msgstr "Nastavenia uchopenia"

#: ../../reference_manual/dockers/snap_settings_docker.rst:20
msgid ""
"This docker has been removed in Krita 3.0. For more information on how to do "
"this instead, consult the :ref:`snapping page <snapping>`."
msgstr ""

#: ../../reference_manual/dockers/snap_settings_docker.rst:23
#, fuzzy
#| msgid ".. image:: images/en/Krita_Snap_Settings_Docker.png"
msgid ".. image:: images/dockers/Krita_Snap_Settings_Docker.png"
msgstr ".. image:: images/en/Krita_Snap_Settings_Docker.png"

#: ../../reference_manual/dockers/snap_settings_docker.rst:24
msgid ""
"This is docker only applies for Vector Layers. Snapping determines where a "
"vector shape will snap. The little number box is for snapping to a grid."
msgstr ""

#: ../../reference_manual/dockers/snap_settings_docker.rst:26
msgid "Node"
msgstr "Uzol"

#: ../../reference_manual/dockers/snap_settings_docker.rst:27
msgid "For snapping to other vector nodes."
msgstr ""

#: ../../reference_manual/dockers/snap_settings_docker.rst:28
msgid "Extensions of Line"
msgstr ""

#: ../../reference_manual/dockers/snap_settings_docker.rst:29
msgid ""
"For snapping to a point that could have been part of a line, had it been "
"extended."
msgstr ""

#: ../../reference_manual/dockers/snap_settings_docker.rst:30
msgid "Bounding Box"
msgstr "Ohraničovací rámček"

#: ../../reference_manual/dockers/snap_settings_docker.rst:31
msgid "For snapping to the bounding box of a vector shape."
msgstr ""

#: ../../reference_manual/dockers/snap_settings_docker.rst:32
msgid "Orthogonal"
msgstr "Ortogonálne"

#: ../../reference_manual/dockers/snap_settings_docker.rst:33
msgid "For snapping to only horizontal or vertical lines."
msgstr ""

#: ../../reference_manual/dockers/snap_settings_docker.rst:34
msgid "Intersection"
msgstr "Prienik"

#: ../../reference_manual/dockers/snap_settings_docker.rst:35
msgid "For snapping to other vector lines."
msgstr ""

#: ../../reference_manual/dockers/snap_settings_docker.rst:37
msgid "Guides don't exist in Krita, therefore this one is useless."
msgstr ""
