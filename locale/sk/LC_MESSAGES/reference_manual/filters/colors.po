# translation of docs_krita_org_reference_manual___filters___colors.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___filters___colors\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-04-02 10:27+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/filters/colors.rst:None
#, fuzzy
#| msgid ".. image:: images/en/Krita-color-to-alpha.png"
msgid ".. image:: images/filters/Krita-color-to-alpha.png"
msgstr ".. image:: images/en/Krita-color-to-alpha.png"

#: ../../reference_manual/filters/colors.rst:1
msgid "Overview of the color filters."
msgstr ""

#: ../../reference_manual/filters/colors.rst:11
msgid "Filters"
msgstr ""

#: ../../reference_manual/filters/colors.rst:16
msgid "Color"
msgstr "Farba"

#: ../../reference_manual/filters/colors.rst:18
msgid ""
"Similar to the Adjust filters, the color filters are image wide color "
"operations."
msgstr ""

#: ../../reference_manual/filters/colors.rst:20
#: ../../reference_manual/filters/colors.rst:24
msgid "Color to Alpha"
msgstr "Farby do alfy"

#: ../../reference_manual/filters/colors.rst:26
msgid ""
"This filter allows you to make one single color transparent (alpha). By "
"default when you run this filter white is selected, you can choose a color "
"that you want to make transparent from the color selector."
msgstr ""

#: ../../reference_manual/filters/colors.rst:29
#, fuzzy
#| msgid ".. image:: images/en/Color-to-alpha.png"
msgid ".. image:: images/filters/Color-to-alpha.png"
msgstr ".. image:: images/en/Color-to-alpha.png"

#: ../../reference_manual/filters/colors.rst:30
msgid ""
"The Threshold indicates how much other colors will be considered mixture of "
"the removed color and non-removed colors. For example, with threshold set to "
"255, and the removed color set to white, a 50% gray will be considered a "
"mixture of black+white, and thus transformed in a 50% transparent black."
msgstr ""

#: ../../reference_manual/filters/colors.rst:36
msgid ""
"This filter is really useful in separating line art from the white "
"background."
msgstr ""

#: ../../reference_manual/filters/colors.rst:41
msgid "Color Transfer"
msgstr "Prenos farby"

#: ../../reference_manual/filters/colors.rst:43
msgid ""
"This filter converts the colors of the image to colors from the reference "
"image. This is a quick way to change a color combination of an artwork to an "
"already saved image or a reference image."
msgstr ""

#: ../../reference_manual/filters/colors.rst:47
#, fuzzy
#| msgid ".. image:: images/en/Color-transfer.png"
msgid ".. image:: images/filters/Color-transfer.png"
msgstr ".. image:: images/en/Color-transfer.png"

#: ../../reference_manual/filters/colors.rst:51
msgid "Maximize Channel"
msgstr "Maximalizovať kanál"

#: ../../reference_manual/filters/colors.rst:53
msgid ""
"This filter checks for all the channels of a each single color and set all "
"but the highest value to 0."
msgstr ""

#: ../../reference_manual/filters/colors.rst:58
msgid "Minimize Channel"
msgstr "Minimalizovať kanál"

#: ../../reference_manual/filters/colors.rst:60
msgid ""
"This is reverse to Maximize channel, it checks all the channels of a each "
"single color and sets all but the lowest to 0."
msgstr ""
