msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../general_concepts/file_formats/file_gif.rst:1
msgid "The GIF file format in Krita."
msgstr ""

#: ../../general_concepts/file_formats/file_gif.rst:10
msgid "GIF"
msgstr ""

#: ../../general_concepts/file_formats/file_gif.rst:10
msgid "*.gif"
msgstr ""

#: ../../general_concepts/file_formats/file_gif.rst:15
msgid "\\*.gif"
msgstr ""

#: ../../general_concepts/file_formats/file_gif.rst:17
msgid ""
"``.gif`` is a file format mostly known for the fact that it can save "
"animations. It's a fairly old format, and it does its compression by :ref:"
"`indexing <bit_depth>` the colors to a maximum of 256 colors per frame. "
"Because we can technically design an image for 256 colors and are always "
"able save over an edited GIF without any kind of extra degradation, this is "
"a :ref:`lossless <lossless_compression>` compression technique."
msgstr ""

#: ../../general_concepts/file_formats/file_gif.rst:19
msgid ""
"This means that it can handle most grayscale images just fine and without "
"losing any visible quality. But for color images that don't animate it might "
"be better to use :ref:`file_jpg` or :ref:`file_png`."
msgstr ""
