msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:1
msgid "The Quick Brush Engine manual page."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:10
#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:15
msgid "Quick Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:10
msgid "Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:19
msgid ".. image:: images/icons/quickbrush.svg"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:20
msgid ""
"A Brush Engine inspired by the common artist's workflow where a simple big "
"brush, like a marker, is used to fill large areas quickly, the Quick Brush "
"engine is an extremely simple, but quick brush, which can give the best "
"performance of all Brush Engines."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:22
msgid ""
"It can only change size, blending mode and spacing, and this allows for "
"making big optimisations that aren't possible with other brush engines."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:25
msgid ":ref:`blending_modes`"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:26
msgid ":ref:`option_spacing`"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:27
msgid ":ref:`option_size`"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:30
msgid "Brush"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:32
msgid "The only parameter specific to this brush."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:34
msgid "Diameter"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:35
msgid ""
"The size. This brush engine can only make round dabs, but it can make them "
"really fast despite size."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:37
msgid "Spacing"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:37
msgid ""
"The spacing between the dabs. This brush engine is particular in that it's "
"faster with a lower spacing, unlike all other brush engines."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:40
msgid "`Phabricator Task <https://phabricator.kde.org/T3492>`_"
msgstr ""
