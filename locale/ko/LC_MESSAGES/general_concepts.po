# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# Shinjo Park <kde@peremen.name>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-22 16:21+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 18.12.3\n"

#: ../../general_concepts.rst:5
msgid "General Concepts"
msgstr "일반적 개념"

#: ../../general_concepts.rst:7
msgid ""
"Learn about general art and technology concepts that are not specific to "
"Krita."
msgstr "Krita에 한정된 것이 아닌 일반적인 미술과 기술 개념을 알아봅니다."

#: ../../general_concepts.rst:9
msgid "Contents:"
msgstr "목차:"
