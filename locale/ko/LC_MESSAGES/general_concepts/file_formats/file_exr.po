# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# Shinjo Park <kde@peremen.name>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-05-30 00:38+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 18.12.3\n"

#: ../../general_concepts/file_formats/file_exr.rst:1
msgid "The EXR file format as exported by Krita."
msgstr "Krita에서 내보내는 EXR 파일 형식입니다."

#: ../../general_concepts/file_formats/file_exr.rst:10
msgid "EXR"
msgstr "EXR"

#: ../../general_concepts/file_formats/file_exr.rst:10
msgid "HDR Fileformat"
msgstr "HDR 파일 형식"

#: ../../general_concepts/file_formats/file_exr.rst:10
msgid "OpenEXR"
msgstr "OpenEXR"

#: ../../general_concepts/file_formats/file_exr.rst:10
msgid "*.exr"
msgstr "*.exr"

#: ../../general_concepts/file_formats/file_exr.rst:15
msgid "\\*.exr"
msgstr "\\*.exr"

#: ../../general_concepts/file_formats/file_exr.rst:17
#, fuzzy
#| msgid ""
#| ".exr is the prime file format for saving and loading :ref:`floating point "
#| "bit depths <bit_depth>`, and due to the library made to load and save "
#| "these images being fully open source, the main interchange format as well."
msgid ""
"``.exr`` is the prime file format for saving and loading :ref:`floating "
"point bit depths <bit_depth>`, and due to the library made to load and save "
"these images being fully open source, the main interchange format as well."
msgstr ""
".exr 형식은 :ref:`floating point bit depths <bit_depth>` 이미지를 불러오고 저"
"장할 때 가장 자주 사용하는 파일 형식이며, 해당 이미지를 다루는 라이브러리가 "
"완전한 오픈 소스이기 때문에 데이터 교환 형식으로도 사용합니다."

#: ../../general_concepts/file_formats/file_exr.rst:19
#, fuzzy
#| msgid ""
#| "Floating point bit-depths are used by the computer graphics industry to "
#| "record scene referred values, which can be made via a camera or a "
#| "computer renderer. Scene referred values means that the file can have "
#| "values whiter than white, which in turn means that such a file can record "
#| "lighting conditions, such as sunsets very accurately. These exr files can "
#| "then be used inside a renderer to create realistic lighting."
msgid ""
"Floating point bit-depths are used by the computer graphics industry to "
"record scene referred values, which can be made via a camera or a computer "
"renderer. Scene referred values means that the file can have values whiter "
"than white, which in turn means that such a file can record lighting "
"conditions, such as sunsets very accurately. These EXR files can then be "
"used inside a renderer to create realistic lighting."
msgstr ""
"컴퓨터 그래픽에서 부동 소수점 색 농도는 장면에서 참조하는 값을 저장하는 데 사"
"용하며, 카메라나 컴퓨터 렌더러로 생성됩니다. 장면에서 참조하는 값은 흰색보다 "
"더 흰색의 값을 포함할 수 있으며, 이를 사용하여 석양과 같은 조명 조건을 매우 "
"정확하게 기록할 수 있습니다. EXR 파일은 렌더러에서 실사 조명을 구현하는 데 사"
"용합니다."

#: ../../general_concepts/file_formats/file_exr.rst:21
#, fuzzy
#| msgid ""
#| "Krita can load and save exr for the purpose of paint-over (yes, Krita can "
#| "paint with scene referred values) and interchange with applications like "
#| "Blender, Mari, Nuke and Natron."
msgid ""
"Krita can load and save EXR for the purpose of paint-over (yes, Krita can "
"paint with scene referred values) and interchange with applications like "
"Blender, Mari, Nuke and Natron."
msgstr ""
"Krita는 EXR 파일을 읽고 쓸 수 있으며(장면에서 참조하는 값 지원) Blender, "
"Mari, Nuke, Natron 등 다른 프로그램과도 데이터를 교환할 수 있습니다."
