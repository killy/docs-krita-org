# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 11:08+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../general_concepts/file_formats/file_psd.rst:1
msgid "The Photoshop file format as exported by Krita."
msgstr "Het bestandsformaat Photoshop zoals geëxporteerd door Krita."

#: ../../general_concepts/file_formats/file_psd.rst:10
msgid "*.psd"
msgstr "*.psd"

#: ../../general_concepts/file_formats/file_psd.rst:10
msgid "PSD"
msgstr "PSD"

#: ../../general_concepts/file_formats/file_psd.rst:10
msgid "Photoshop Document"
msgstr "Photoshop Document"

#: ../../general_concepts/file_formats/file_psd.rst:15
msgid "\\*.psd"
msgstr "\\*.psd"

#: ../../general_concepts/file_formats/file_psd.rst:17
msgid ""
"``.psd`` is Photoshop's internal file format. For some reason, people like "
"to use it as an interchange format, even though it is not designed for this."
msgstr ""
"``.psd`` is het interne bestandsformaat van Photoshop. Om de een of andere "
"reden, willen mensen het gebruiken als een uitwisselingsformaat, zelfs al is "
"het daarvoor niet ontworpen."

#: ../../general_concepts/file_formats/file_psd.rst:19
msgid ""
"``.psd``, unlike actual interchange formats like :ref:`file_pdf`, :ref:"
"`file_tif`, :ref:`file_exr`, :ref:`file_ora` and :ref:`file_svg` doesn't "
"have an official spec online. Which means that it needs to be reverse "
"engineered. Furthermore, as an internal file format, it doesn't have much of "
"a philosophy to its structure, as it's only purpose is to save what "
"Photoshop is busy with, or rather, what all the past versions of Photoshop "
"have been busy with. This means that the inside of a PSD looks somewhat like "
"Photoshop's virtual brains, and PSD is in general a very disliked file-"
"format."
msgstr ""

#: ../../general_concepts/file_formats/file_psd.rst:21
msgid ""
"Due to ``.psd`` being used as an interchange format, this leads to confusion "
"amongst people using these programs, as to why not all programs support "
"opening these. Sometimes, you might even see users saying that a certain "
"program is terrible because it doesn't support opening PSDs properly. But as "
"PSD is an internal file-format without online specs, it is impossible to "
"have any program outside it support it 100%."
msgstr ""

#: ../../general_concepts/file_formats/file_psd.rst:23
msgid ""
"Krita supports loading and saving raster layers, blending modes, "
"layerstyles, layer groups, and transparency masks from PSD. It will likely "
"never support vector and text layers, as these are just too difficult to "
"program properly."
msgstr ""

#: ../../general_concepts/file_formats/file_psd.rst:25
msgid ""
"We recommend using any other file format instead of PSD if possible, with a "
"strong preference towards :ref:`file_ora` or :ref:`file_tif`."
msgstr ""

#: ../../general_concepts/file_formats/file_psd.rst:27
msgid ""
"As a working file format, PSDs can be expected to become very heavy and most "
"websites won't accept them."
msgstr ""
