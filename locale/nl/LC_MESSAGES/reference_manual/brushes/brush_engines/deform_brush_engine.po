# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-28 15:01+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<generated>:1
msgid "Use Undeformed Image"
msgstr "Niet vervormde afbeelding gebruiken"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:1
msgid "The Deform Brush Engine manual page."
msgstr "De handleidingpagina vervormendepenseel-engine."

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:16
msgid "Deform Brush Engine"
msgstr "Vervormendepenseel-engine"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:11
msgid "Brush Engine"
msgstr "Penseel-engine"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:11
msgid "Deform"
msgstr "Deformeren"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:11
msgid "Liquify"
msgstr "Liquify"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:19
msgid ".. image:: images/icons/deformbrush.svg"
msgstr ".. image:: images/icons/deformbrush.svg"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:20
msgid ""
"The Deform Brush is a brush that allows you to pull and push pixels around. "
"It's quite similar to the :ref:`liquify_mode`, but where liquify has higher "
"quality, the deform brush has the speed."
msgstr ""
"De vervormendepenseel is een penseel die u in staat stelt om willekeurig "
"pixels te trekken en te duwen. Het is tamelijk gelijk aan het :ref:"
"`liquify_mode`, maar waar liquefy een hogere kwaliteit heeft, heeft de "
"vervormendepenseel snelheid."

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:24
msgid "Options"
msgstr "Opties"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:26
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:27
msgid ":ref:`option_deform`"
msgstr ":ref:`option_deform`"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:28
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:29
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:30
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:31
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:32
msgid ":ref:`option_airbrush`"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:38
msgid "Deform Options"
msgstr "Vervormingsopties"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:42
msgid ".. image:: images/brushes/Krita_deform_brush_examples.png"
msgstr ".. image:: images/brushes/Krita_deform_brush_examples.png"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:42
msgid ""
"1: undeformed, 2: Move, 3: Grow, 4: Shrink, 5: Swirl Counter Clock Wise, 6: "
"Swirl Clockwise, 7: Lens Zoom In, 8: Lens Zoom Out"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:44
msgid "These decide what strangeness may happen underneath your brush cursor."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:46
msgid "Grow"
msgstr "Groeien"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:47
msgid "This bubbles up the area underneath the brush-cursor."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:48
msgid "Shrink"
msgstr "Krimpen"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:49
msgid "This pinches the Area underneath the brush-cursor."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:50
msgid "Swirl Counter Clock Wise"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:51
msgid "Swirls the area counter clock wise."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:52
msgid "Swirl Clock Wise"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:53
msgid "Swirls the area clockwise."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:54
msgid "Move"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:55
msgid "Nudges the area to the painting direction."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:56
msgid "Color Deformation"
msgstr "Kleurdeformatie"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:57
msgid "This seems to randomly rearrange the pixels underneath the brush."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:58
msgid "Lens Zoom In"
msgstr "Inzoomen van de lens"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:59
msgid "Literally paints a enlarged version of the area."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:61
msgid "Lens Zoom Out"
msgstr "Uitzoomen van de lens"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:61
msgid "Paints a minimized version of the area."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:65
msgid ".. image:: images/brushes/Krita_deform_brush_colordeform.png"
msgstr ".. image:: images/brushes/Krita_deform_brush_colordeform.png"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:65
msgid "Showing color deform."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:68
msgid "Deform Amount"
msgstr "Vervormingshoeveelheid:"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:68
msgid "Defines the strength of the deformation."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:72
msgid ".. image:: images/brushes/Krita_deform_brush_bilinear.png"
msgstr ".. image:: images/brushes/Krita_deform_brush_bilinear.png"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:72
#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:74
msgid "Bilinear Interpolation"
msgstr "Bi-lineaire interpolatie"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:75
msgid "Smoothens the result. This causes calculation errors in 16bit."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:77
msgid "Use Counter"
msgstr "Teller gebruiken"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:77
msgid "Slows down the deformation subtlety."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:81
msgid ".. image:: images/brushes/Krita_deform_brush_useundeformed.png"
msgstr ".. image:: images/brushes/Krita_deform_brush_useundeformed.png"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:81
msgid "Without 'use undeformed' to the left and with to the right."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:84
msgid ""
"Samples from the previous version of the image instead of the current. This "
"works better with some deform options than others. Move for example seems to "
"almost stop working, but it works really well with Grow."
msgstr ""
