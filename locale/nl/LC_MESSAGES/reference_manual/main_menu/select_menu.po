# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 23:02+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../reference_manual/main_menu/select_menu.rst:1
msgid "The select menu in Krita."
msgstr "Het menu Selecteren in Krita."

#: ../../reference_manual/main_menu/select_menu.rst:11
msgid "Selection"
msgstr "Selectie"

#: ../../reference_manual/main_menu/select_menu.rst:16
msgid "Select Menu"
msgstr "Menu Selecteren"

#: ../../reference_manual/main_menu/select_menu.rst:19
msgid "Select All"
msgstr "Alles selecteren"

#: ../../reference_manual/main_menu/select_menu.rst:21
msgid "Selects the whole layer."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:22
msgid "Deselect"
msgstr "Selectie opheffen"

#: ../../reference_manual/main_menu/select_menu.rst:24
msgid "Deselects everything (except for active Selection Mask)."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:25
msgid "Reselect"
msgstr "Opnieuw selecteren"

#: ../../reference_manual/main_menu/select_menu.rst:27
msgid "Reselects the previously deselected selection."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:28
msgid "Invert Selection"
msgstr "Selectie omkeren"

#: ../../reference_manual/main_menu/select_menu.rst:30
msgid "Inverts the selection."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:31
msgid "Convert to Vector Selection."
msgstr "Naar vectorselectie converteren."

#: ../../reference_manual/main_menu/select_menu.rst:33
msgid ""
"This converts a raster selection to a vector selection. Any layers of "
"transparency there might have been are removed."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:34
msgid "Convert to Raster Selection."
msgstr "Naar rasterselectie converteren."

#: ../../reference_manual/main_menu/select_menu.rst:36
msgid "This converts a vector selection to a raster selection."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:37
msgid "Convert Shapes to Vector Selection"
msgstr "Vormen naar vectorselectie converteren"

#: ../../reference_manual/main_menu/select_menu.rst:39
msgid "Convert vector shape to vector selection."
msgstr "Vectorvorm converteren naar vectorselectie"

#: ../../reference_manual/main_menu/select_menu.rst:40
msgid "Convert to shape"
msgstr "Converteren naar vorm"

#: ../../reference_manual/main_menu/select_menu.rst:42
msgid "Converts vector selection to vector shape."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:43
msgid "Display Selection"
msgstr "Selectie tonen"

#: ../../reference_manual/main_menu/select_menu.rst:45
msgid "Display the selection. If turned off selections will be invisible."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:46
msgid "Show Global Selection Mask"
msgstr "Globale selectiemasker tonen"

#: ../../reference_manual/main_menu/select_menu.rst:48
msgid ""
"Shows the global selection as a selection mask in the layers docker. This is "
"necessary to be able to select it for painting on."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:49
msgid "Scale"
msgstr "Schaal"

#: ../../reference_manual/main_menu/select_menu.rst:51
msgid "Scale the selection."
msgstr "De selectie schalen."

#: ../../reference_manual/main_menu/select_menu.rst:52
msgid "Select from Color Range"
msgstr "Uit kleurbereik selecteren"

#: ../../reference_manual/main_menu/select_menu.rst:54
msgid "Select from a certain color range."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:55
msgid "Select Opaque"
msgstr "Dekking selecteren"

#: ../../reference_manual/main_menu/select_menu.rst:57
msgid ""
"Select all opaque (non-transparent) pixels in the current active layer. If "
"there's already a selection, this will add the new selection to the old one, "
"allowing you to select the opaque pixels of multiple layers into one "
"selection. Semi-transparent (or semi-opaque) pixels will be semi-selected."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:58
msgid "Feather Selection"
msgstr "Veer-selectie"

#: ../../reference_manual/main_menu/select_menu.rst:60
msgid ""
"Feathering in design means to soften sharp borders. So this adds a soft "
"border to the existing selection."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:61
msgid "Grow Selection"
msgstr "Selectie vergroten"

#: ../../reference_manual/main_menu/select_menu.rst:63
msgid "Make the selection a few pixels bigger."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:64
msgid "Shrink Selection"
msgstr "Selectie verkleinen"

#: ../../reference_manual/main_menu/select_menu.rst:66
msgid "Make the selection a few pixels smaller."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:67
msgid "Border Selection"
msgstr "Selectie omranden"

#: ../../reference_manual/main_menu/select_menu.rst:69
msgid ""
"Take the current selection and remove the insides so you only have a border "
"selected."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:70
msgid "Smooth"
msgstr "Glad"

#: ../../reference_manual/main_menu/select_menu.rst:72
msgid "Make the selection a little smoother. This removes jiggle."
msgstr ""
