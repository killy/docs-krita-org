# Lithuanian translations for Krita Manual package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-19 03:36+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: lt\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"

#: ../../<rst_epilog>:32
msgid ""
".. image:: images/icons/polyline_tool.svg\n"
"   :alt: toolpolyline"
msgstr ""

#: ../../reference_manual/tools/polyline.rst:1
msgid "Krita's polyline tool reference."
msgstr ""

#: ../../reference_manual/tools/polyline.rst:10
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/polyline.rst:10
msgid "Polyline"
msgstr ""

#: ../../reference_manual/tools/polyline.rst:15
msgid "Polyline Tool"
msgstr ""

#: ../../reference_manual/tools/polyline.rst:17
msgid "|toolpolyline|"
msgstr ""

#: ../../reference_manual/tools/polyline.rst:20
msgid ""
"Polylines are drawn like :ref:`polygon_tool`, with the difference that the "
"double-click indicating the end of the polyline does not connect the last "
"vertex to the first one."
msgstr ""
