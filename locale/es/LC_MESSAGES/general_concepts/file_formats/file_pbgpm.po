# Spanish translations for docs_krita_org_general_concepts___file_formats___file_pbgpm.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___file_formats___file_pbgpm\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-02-19 20:20+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.2\n"

#: ../../<generated>:1
msgid ".ppm"
msgstr ".ppm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:1
msgid "The PBM, PGM and PPM file formats as exported by Krita."
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
#, fuzzy
#| msgid ".pbm"
msgid "*.pbm"
msgstr ".pbm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
#, fuzzy
#| msgid ".pgm"
msgid "*.pgm"
msgstr ".pgm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
#, fuzzy
#| msgid ".ppm"
msgid "*.ppm"
msgstr ".ppm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "PBM"
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "PGM"
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "PPM"
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:17
#, fuzzy
#| msgid "\\*.pbm, \\*.pgm, \\*.ppm"
msgid "\\*.pbm, \\*.pgm and \\*.ppm"
msgstr "\\*.pbm, \\*.pgm, \\*.ppm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:18
msgid ""
"``.pbm``, ``.pgm`` and ``.ppm`` are a series of file-formats with a similar "
"logic to them. They are designed to save images in a way that the result can "
"be read as an ASCII file, from back when email clients couldn't read images "
"reliably."
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:20
msgid ""
"They are very old file formats, and not used outside of very specialized "
"usecases, such as embedding images inside code."
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:22
msgid ".pbm"
msgstr ".pbm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:23
#, fuzzy
#| msgid "one-bit and can only show strict black and white."
msgid "One-bit and can only show strict black and white."
msgstr "1 bit y solo puede mostrar blanco y negro estrictos."

#: ../../general_concepts/file_formats/file_pbgpm.rst:24
msgid ".pgm"
msgstr ".pgm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:25
#, fuzzy
#| msgid "can show 255 values of gray(8bit)."
msgid "Can show 255 values of gray (8bit)."
msgstr "puede mostrar 255 valores de gris (8 bits)."

#: ../../general_concepts/file_formats/file_pbgpm.rst:27
#, fuzzy
#| msgid "can show 8bit rgb values."
msgid "Can show 8bit rgb values."
msgstr "puede mostrar valores rgb de 8 bits."
