# Spanish translations for docs_krita_org_reference_manual___preferences___shortcut_settings.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___preferences___shortcut_settings\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-06-20 17:02+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<generated>:1
msgid "Saving, loading and sharing custom shortcuts"
msgstr ""

#: ../../reference_manual/preferences/shortcut_settings.rst:1
msgid "Configuring shortcuts in Krita."
msgstr ""

#: ../../reference_manual/preferences/shortcut_settings.rst:11
msgid "Preferences"
msgstr "Preferencias"

#: ../../reference_manual/preferences/shortcut_settings.rst:11
msgid "Settings"
msgstr "Preferencias"

#: ../../reference_manual/preferences/shortcut_settings.rst:11
msgid "Shortcuts"
msgstr "Accesos rápidos"

#: ../../reference_manual/preferences/shortcut_settings.rst:16
msgid "Shortcut Settings"
msgstr "Preferencias de accesos rápidos"

#: ../../reference_manual/preferences/shortcut_settings.rst:18
msgid ""
"Most of Krita's shortcuts are configured in the menu section :menuselection:"
"`Settings --> Configure Krita --> Configure Shortcuts`. The shortcuts "
"configured here are simple key combinations, for example the :kbd:`Ctrl + X` "
"shortcut to cut. Shortcuts can also be sequences of key combinations (e.g. :"
"kbd:`Shift + S` shortcut then the :kbd:`B` key). Krita also has a special "
"interface for configuring the mouse and stylus events sent to the canvas, "
"found under :ref:`canvas_input_settings`."
msgstr ""

#: ../../reference_manual/preferences/shortcut_settings.rst:21
msgid "Menu Items"
msgstr "Elementos del menú"

#: ../../reference_manual/preferences/shortcut_settings.rst:23
msgid "Search bar"
msgstr "Barra de búsqueda"

#: ../../reference_manual/preferences/shortcut_settings.rst:24
msgid ""
"Entering text here will search for matching shortcuts in the shortcut list."
msgstr ""

#: ../../reference_manual/preferences/shortcut_settings.rst:25
msgid "Shortcut List"
msgstr "Lista de accesos rápidos"

#: ../../reference_manual/preferences/shortcut_settings.rst:26
msgid ""
"Shortcuts are organized into sections. Each shortcut can be given a primary "
"and alternate key combination"
msgstr ""

#: ../../reference_manual/preferences/shortcut_settings.rst:28
msgid "Load/Save Shortcuts Profiles"
msgstr ""

#: ../../reference_manual/preferences/shortcut_settings.rst:28
msgid ""
"The bottom row of buttons contains commands for exporting and import "
"keyboard shortcuts."
msgstr ""

#: ../../reference_manual/preferences/shortcut_settings.rst:31
msgid ".. image:: images/preferences/Krita_Configure_Shortcuts.png"
msgstr ".. image:: images/preferences/Krita_Configure_Shortcuts.png"

#: ../../reference_manual/preferences/shortcut_settings.rst:33
msgid "Configuration"
msgstr "Configuración"

#: ../../reference_manual/preferences/shortcut_settings.rst:36
msgid "Primary and alternate shortcuts"
msgstr "Accesos rápidos primarios y alternativos"

#: ../../reference_manual/preferences/shortcut_settings.rst:36
msgid ""
"Each shortcut is assigned a default, which may be empty. The user can assign "
"up to two custom shortcuts, known as primary and alternate shortcuts. Simply "
"click on a \"Custom\" button and type the key combination you wish to assign "
"to the shortcut. If the key combination is already in use for another "
"shortcut, the dialog will prompt the user to resolve the conflict."
msgstr ""

#: ../../reference_manual/preferences/shortcut_settings.rst:39
msgid "Shortcut schemes"
msgstr "Esquemas de accesos rápidos"

#: ../../reference_manual/preferences/shortcut_settings.rst:39
msgid ""
"Many users migrate to Krita from other tools with different default "
"shortcuts. Krita users may change the default shortcuts to mimic these other "
"programs.  Currently, Krita ships with defaults for Photoshop and Paint Tool "
"Sai. Additional shortcut schemes can be placed in the ~/.config/krita/input/ "
"folder."
msgstr ""

#: ../../reference_manual/preferences/shortcut_settings.rst:42
msgid ""
"Users may wish to export their shortcuts to use across machines, or even "
"share with other users. This can be done with the save/load drop-down. Note: "
"the shortcuts can be saved and overridden manually by backingup the text "
"file kritashortcutsrc located in ~/.config/krita/.  Additionally, the user "
"can export a custom shortcut scheme file generated by merging the existing "
"scheme defaults with the current customizations."
msgstr ""
