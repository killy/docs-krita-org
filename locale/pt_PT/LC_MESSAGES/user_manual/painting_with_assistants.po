# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 13:59+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en Trimétrica guilabel image kbd assistanttool\n"
"X-POFile-SpellExtra: images Assistants alt dimétrica eye\n"
"X-POFile-SpellExtra: Assistantstrimetric Assistantsfish gif ref\n"
"X-POFile-SpellExtra: Axonométrica Dimétrica Kritabasicassistants\n"
"X-POFile-SpellExtra: pointperspective Assistantsoblique\n"
"X-POFile-SpellExtra: Assistantsvanishingpointlogic Bézier Krita Fish\n"
"X-POFile-SpellExtra: Assistantsdimetric assistants\n"
"X-POFile-IgnoreConsistency: Oblique\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/assistants/Assistants_fish-eye_2_02.png"
msgstr ".. image:: images/assistants/Assistants_fish-eye_2_02.png"

#: ../../user_manual/painting_with_assistants.rst:2
msgid "How to use the painting assistants in Krita to draw perspectives."
msgstr ""
"Como usar os assistentes de pintura no Krita para desenhar perspectivas."

#: ../../user_manual/painting_with_assistants.rst:12
msgid "Painting Assistants"
msgstr "Assistentes de Pintura"

#: ../../user_manual/painting_with_assistants.rst:17
msgid "Painting with Assistants"
msgstr "Pintura com Assistentes"

#: ../../user_manual/painting_with_assistants.rst:19
msgid ""
"The assistant system allows you to have a little help while drawing straight "
"lines or circles."
msgstr ""
"O sistema de assistentes permite-lhe ter uma pequena ajuda a desenhar linhas "
"rectas ou círculos."

#: ../../user_manual/painting_with_assistants.rst:22
msgid ""
"They can function as a preview shape, or you can snap onto them with the "
"freehand brush tool. In the tool options of free hand brush, you can toggle :"
"guilabel:`Snap to Assistants` to turn on snapping."
msgstr ""
"Elas poderão funcionar como uma forma de antevisão ou podê-las-á ajustar com "
"a ferramenta de pincel à mão. Nas opções da ferramenta do pincel à mão, "
"poderá comutar a opção :guilabel:`Ajustar aos Assistentes` para activar o "
"ajuste."

#: ../../user_manual/painting_with_assistants.rst:30
msgid ""
".. image:: images/assistants/Krita_basic_assistants.png\n"
"   :alt: Krita's vanishing point assistants in action"
msgstr ""
".. image:: images/assistants/Krita_basic_assistants.png\n"
"   :alt: Os assistentes do ponto de fuga do Krita em acção"

#: ../../user_manual/painting_with_assistants.rst:30
msgid "Krita's vanishing point assistants in action"
msgstr "Os assistentes do ponto de fuga do Krita em acção"

#: ../../user_manual/painting_with_assistants.rst:32
msgid "The following assistants are available in Krita:"
msgstr "Estão disponíveis os seguintes assistentes no Krita:"

#: ../../user_manual/painting_with_assistants.rst:35
msgid "Types"
msgstr "Tipos"

#: ../../user_manual/painting_with_assistants.rst:37
msgid ""
"There are several types in Krita. You can select a type of assistant via the "
"tool options docker."
msgstr ""
"Existem diversos tipos no Krita. Poderá seleccionar um dado tipo de "
"assistente através da área de opções da ferramenta."

#: ../../user_manual/painting_with_assistants.rst:43
msgid "Ellipse"
msgstr "Elipse"

#: ../../user_manual/painting_with_assistants.rst:45
msgid "An assistant for drawing ellipses and circles."
msgstr "Um assistente para desenhar elipses e círculos."

#: ../../user_manual/painting_with_assistants.rst:47
msgid ""
"This assistant consists of three points: the first two are the axis of the "
"ellipse, and the last one is to determine its width."
msgstr ""
"Este assistente consiste em três pontos: os dois primeiros são o eixo da "
"elipse e o último serve para determinar a sua largura."

#: ../../user_manual/painting_with_assistants.rst:51
msgid ""
"The same an ellipse, but allows for making ellipses that are concentric to "
"each other."
msgstr ""
"O mesmo que a elipse, mas permite criar elipses que são concêntricas entre "
"si."

#: ../../user_manual/painting_with_assistants.rst:52
msgid "Concentric Ellipse"
msgstr "Elipse Concêntrica"

#: ../../user_manual/painting_with_assistants.rst:54
#: ../../user_manual/painting_with_assistants.rst:144
msgid ""
"If you press the :kbd:`Shift` key while holding the first two handles, they "
"will snap to perfectly horizontal or vertical lines. Press the :kbd:`Shift` "
"key while holding the third handle, and it'll snap to a perfect circle."
msgstr ""
"Se carregar em :kbd:`Shift` enquanto segura as duas primeiras pegas, elas "
"ajustar-se-ão em linhas perfeitamente horizontais ou verticais. Carregue em :"
"kbd:`Shift` enquanto segura a terceira pega, para que se ajuste a um círculo "
"perfeito."

#: ../../user_manual/painting_with_assistants.rst:61
#: ../../user_manual/painting_with_assistants.rst:154
msgid "Perspective"
msgstr "Perspectiva"

#: ../../user_manual/painting_with_assistants.rst:63
msgid "This ruler takes four points and creates a perspective grid."
msgstr "Esta régua pega em quatro pontos e cria uma grelha de perspectiva."

#: ../../user_manual/painting_with_assistants.rst:65
msgid ""
"This grid can be used with the 'perspective' sensor, which can influence "
"brushes."
msgstr ""
"Esta grelha poderá ser usada com o sensor de 'perspectiva', o qual poderá "
"influenciar os pincéis."

#: ../../user_manual/painting_with_assistants.rst:68
msgid ""
"If you press the :kbd:`Shift` key while holding any of the corner handles, "
"they'll snap to one of the other corner handles, in sets."
msgstr ""
"Se carregar no :kbd:`Shift` enquanto segura alguma das pegas dos cantos, as "
"mesmas ajustar-se-ão a uma das outras pegas dos cantos em conjuntos."

#: ../../user_manual/painting_with_assistants.rst:74
#: ../../user_manual/painting_with_assistants.rst:78
msgid "Ruler"
msgstr "Régua"

#: ../../user_manual/painting_with_assistants.rst:76
msgid "There are three assistants in this group:"
msgstr "Existem três assistentes neste grupo:"

#: ../../user_manual/painting_with_assistants.rst:79
msgid "Helps create a straight line between two points."
msgstr "Ajuda a criar uma linha recta entre dois pontos."

#: ../../user_manual/painting_with_assistants.rst:81
msgid "Infinite Ruler"
msgstr "Régua Infinita"

#: ../../user_manual/painting_with_assistants.rst:81
msgid ""
"Extrapolates a straight line beyond the two visible points on the canvas."
msgstr ""
"Extrapola uma linha recta para lá dos dois pontos visíveis na área de "
"desenho."

#: ../../user_manual/painting_with_assistants.rst:84
msgid ""
"This ruler allows you to draw a line parallel to the line between the two "
"points anywhere on the canvas."
msgstr ""
"Esta régua permite-lhe desenhar uma linha paralela à linha entre os dois "
"pontos, em qualquer ponto da área de desenho."

#: ../../user_manual/painting_with_assistants.rst:85
msgid "Parallel Ruler"
msgstr "Régua Paralela"

#: ../../user_manual/painting_with_assistants.rst:87
msgid ""
"If you press the :kbd:`Shift` key while holding the first two handles, they "
"will snap to perfectly horizontal or vertical lines."
msgstr ""
"Se carregar no :kbd:`Shift` enquanto segura as duas primeiras pegas, as "
"mesmas ajustar-se-ão a linhas perfeitamente horizontais ou verticais."

#: ../../user_manual/painting_with_assistants.rst:93
msgid "Spline"
msgstr "Curva"

#: ../../user_manual/painting_with_assistants.rst:95
msgid ""
"This assistant allows you to position and adjust four points to create a "
"cubic bezier curve. You can then draw along the curve, snapping your brush "
"stroke directly to the curve line. Perfect curves every time!"
msgstr ""
"Este assistente permite-lhe posicionar e ajustar quatro pontos para criar "
"uma curva cúbica de Bézier. Poderá então desenhar ao longo da curva, "
"ajustando directamente o traço do seu pincel à linha da curva. Curvas "
"perfeitas a qualquer hora!"

#: ../../user_manual/painting_with_assistants.rst:99
msgid ""
"If you press the :kbd:`Shift` key while holding the first two handles, they "
"will snap to perfectly horizontal or vertical lines. Press the :kbd:`Shift` "
"key while holding the third or fourth handle, they will snap relative to the "
"handle they are attached to."
msgstr ""
"Se carregar em :kbd:`Shift` enquanto segura as duas primeiras pegas, elas "
"ajustar-se-ão em linhas perfeitamente horizontais ou verticais. Carregue em :"
"kbd:`Shift` enquanto segura a terceira ou quarta pegas, para que se ajustem "
"em relação à pega a que estão associadas."

#: ../../user_manual/painting_with_assistants.rst:107
msgid "Vanishing Point"
msgstr "Ponto de Desaparecimento"

#: ../../user_manual/painting_with_assistants.rst:109
msgid ""
"This assistant allows you to create a vanishing point, typically used for a "
"horizon line. A preview line is drawn and all your snapped lines are drawn "
"to this line."
msgstr ""
"Este assistente permite-lhe criar um ponto de fuga, tipicamente usado para "
"uma linha de horizonte. É desenhada uma linha de antevisão e todas as suas "
"linhas ajustadas são desenhadas em direcção a esta linha."

#: ../../user_manual/painting_with_assistants.rst:112
msgid ""
"It is one point, with four helper points to align it to previously created "
"perspective lines."
msgstr ""
"É um ponto com quatro pontos auxiliares para o alinhar às linhas de "
"perspectiva criadas anteriormente."

#: ../../user_manual/painting_with_assistants.rst:115
msgid "They are made and manipulated with the :ref:`assistant_tool`."
msgstr "Elas são criadas e manipuladas com a :ref:`assistant_tool`."

#: ../../user_manual/painting_with_assistants.rst:117
msgid ""
"If you press the :kbd:`Shift` key while holding the center handle, they will "
"snap to perfectly horizontal or vertical lines depending on the position of "
"where it previously was."
msgstr ""
"Se carregar em :kbd:`Shift` enquanto segura a pega do centro, elas ajustar-"
"se-ão em linhas perfeitamente horizontais ou verticais, dependendo da "
"posição onde estava anteriormente."

#: ../../user_manual/painting_with_assistants.rst:123
msgid "The vanishing point assistant also shows several general lines."
msgstr "O assistente do ponto de fuga também mostra diversas linhas gerais."

#: ../../user_manual/painting_with_assistants.rst:125
msgid ""
"When you've just created, or when you've just moved a vanishing point "
"assistant, it will be selected. This means you can modify the amount of "
"lines shown in the tool options of the :ref:`assistant_tool`."
msgstr ""
"Quando tiver acabado de criar ou de mover um assistente do ponto de fuga, o "
"mesmo ficará seleccionado. Isto significa que poderá modificar a quantidade "
"de linhas apresentadas nas opções da ferramenta :ref:`assistant_tool`."

#: ../../user_manual/painting_with_assistants.rst:130
msgid "Fish Eye Point"
msgstr "Ponto de Olho-de-Peixe"

#: ../../user_manual/painting_with_assistants.rst:132
msgid ""
"Like the vanishing point assistant, this assistant is per a set of parallel "
"lines in a 3d space. So to use it effectively, use two, where the second is "
"at a 90 degrees angle of the first, and add a vanishing point to the center "
"of both. Or combine one with a parallel ruler and a vanishing point, or even "
"one with two vanishing points. The possibilities are quite large."
msgstr ""
"Como o assistente do ponto de fuga, este assistente aplica-se a um conjunto "
"de linhas paralelas num espaço 3D. Como tal, para a usar com eficácia, use "
"duas, onde a segunda tem um ângulo de 90 graus face à primeira, e adicione "
"um ponto de fuga ao centro de ambas. Ou então poderá combinar uma com uma "
"régua paralela e um ponto de fuga, ou mesmo uma com dois pontos de fuga. As "
"possibilidades são muito abrangentes."

#: ../../user_manual/painting_with_assistants.rst:139
msgid ""
"This assistant will not just give feedback/snapping between the vanishing "
"points, but also give feedback to the relative left and right of the "
"assistant. This is so you can use it in edge-cases like panoramas with "
"relative ease."
msgstr ""
"Este assistente não só lhe dará alguma reacção/ajuste entre os pontos de "
"fuga, mas também lhe dará reacções em relação à esquerda e à direita do "
"assistente. Isto serve para que o use em casos-limites, como os panoramas, "
"com relativa facilidade."

#: ../../user_manual/painting_with_assistants.rst:149
msgid "Tutorials"
msgstr "Tutoriais"

#: ../../user_manual/painting_with_assistants.rst:151
msgid ""
"Check out this in depth discussion and tutorial on https://www.youtube.com/"
"watch?v=OhEv2pw3EuI"
msgstr ""
"Consulte esta discussão aprofundada e o tutorial em https://www.youtube.com/"
"watch?v=OhEv2pw3EuI"

#: ../../user_manual/painting_with_assistants.rst:154
msgid "Technical Drawing"
msgstr "Desenho Técnico"

#: ../../user_manual/painting_with_assistants.rst:157
msgid "Setting up Krita for technical drawing-like perspectives"
msgstr "Configuração do Krita para perspectivas de desenho técnico"

#: ../../user_manual/painting_with_assistants.rst:159
msgid ""
"So now that you've seen the wide range of drawing assistants that Krita "
"offers, here is an example of how using these assistants you can set up "
"Krita for technical drawing."
msgstr ""
"Até agora, já foi possível ver a ampla gama de assistentes de desenho que o "
"Krita. Aqui está um exemplo de como usar estes assistentes lhe permitirá "
"usar o Krita para o desenho técnico."

#: ../../user_manual/painting_with_assistants.rst:163
msgid ""
"This tutorial below should give you an idea of how to set up the assistants "
"for specific types of technical views."
msgstr ""
"Este tutorial abaixo dever-lhe-á dar uma ideia de como configurar os "
"assistentes para tipos específicos de vistas técnicas."

#: ../../user_manual/painting_with_assistants.rst:166
msgid ""
"If you want to instead do the true projection, check out :ref:`the "
"projection category <cat_projection>`."
msgstr ""
"Se quiser por outro lado fazer uma projecção verdadeira, consulte a :ref:"
"`categoria da projecção <cat_projection>`."

#: ../../user_manual/painting_with_assistants.rst:169
msgid "Orthographic"
msgstr "Ortográfica"

#: ../../user_manual/painting_with_assistants.rst:171
msgid ""
"Orthographic is a mode where you try to look at something from the left or "
"the front. Typically, you try to keep everything in exact scale with each "
"other, unlike perspective deformation."
msgstr ""
"O modo ortográfico é onde tenta olhar para algo do lado esquerdo ou de "
"frente. Tipicamente, pretende manter tudo com a escala exacta entre si, ao "
"contrário da deformação em perspectiva."

#: ../../user_manual/painting_with_assistants.rst:175
msgid ""
"The key assistant you want to use here is the Parallel Ruler. You can set "
"these up horizontally or vertically, so you always have access to a Grid."
msgstr ""
"O assistente-chave que pretende usar aqui é a Régua Paralela. Podê-las-á "
"configurar na horizontal ou na vertical, pelo que terá sempre acesso a uma "
"Grelha."

#: ../../user_manual/painting_with_assistants.rst:180
msgid "Axonometric"
msgstr "Axonométrica"

#: ../../user_manual/painting_with_assistants.rst:182
msgid "All of these are set up using three Parallel Rulers."
msgstr "Todas estas são configuradas com três Réguas Paralelas."

#: ../../user_manual/painting_with_assistants.rst:185
msgid ".. image:: images/assistants/Assistants_oblique.png"
msgstr ".. image:: images/assistants/Assistants_oblique.png"

#: ../../user_manual/painting_with_assistants.rst:187
msgid ""
"For oblique, set two parallel rulers to horizontal and vertical, and one to "
"an angle, representing depth."
msgstr ""
"Para a oblíqua, configure duas réguas paralelas na horizontal e na vertical, "
"mais uma com um dado ângulo, representando a profundidade."

#: ../../user_manual/painting_with_assistants.rst:188
msgid "Oblique"
msgstr "Oblíqua"

#: ../../user_manual/painting_with_assistants.rst:191
msgid ".. image:: images/assistants/Assistants_dimetric.png"
msgstr ".. image:: images/assistants/Assistants_dimetric.png"

#: ../../user_manual/painting_with_assistants.rst:193
msgid ""
"Isometric perspective has technically all three rulers set up at 120° from "
"each other. Except when it's game isometric, then it's a type of dimetric "
"projection where the diagonal values are a 116.565° from the main. The "
"latter can be easily set up by snapping the assistants to a grid."
msgstr ""
"A perspectiva isométrica tem tecnicamente todas as três réguas configuradas "
"a 120° umas das outras. Excepto no caso da isometria em jogos, então é um "
"tipo de projecção dimétrica onde os valores na diagonal estão a 116,565° da "
"principal. A última poderá ser facilmente configurada se ajustar os "
"assistentes a uma grelha."

#: ../../user_manual/painting_with_assistants.rst:197
msgid "Dimetric & Isometric"
msgstr "Dimétrica & Isométrica"

#: ../../user_manual/painting_with_assistants.rst:200
msgid ".. image:: images/assistants/Assistants_trimetric.png"
msgstr ".. image:: images/assistants/Assistants_trimetric.png"

#: ../../user_manual/painting_with_assistants.rst:202
msgid ""
"Is when all the angles are slightly different. Often looks like a slightly "
"angled isometric."
msgstr ""
"É quando todos os ângulos são ligeiramente diferentes. Normalmente parece-se "
"com uma isométrica, mas com um ligeiro ângulo."

#: ../../user_manual/painting_with_assistants.rst:203
msgid "Trimetric"
msgstr "Trimétrica"

#: ../../user_manual/painting_with_assistants.rst:206
msgid "Linear Perspective"
msgstr "Perspectiva Linear"

#: ../../user_manual/painting_with_assistants.rst:209
msgid ".. image:: images/assistants/Assistants_1_point_perspective.png"
msgstr ".. image:: images/assistants/Assistants_1_point_perspective.png"

#: ../../user_manual/painting_with_assistants.rst:211
msgid ""
"A 1 point perspective is set up using 1 vanishing point, and two crossing "
"perpendicular parallel rulers."
msgstr ""
"É configurada uma perspectiva com 1 ponto, usando 1 ponto de fuga e duas "
"réguas paralelas perpendiculares."

#: ../../user_manual/painting_with_assistants.rst:212
msgid "1 Point Perspective"
msgstr "Perspectiva de 1 Ponto"

#: ../../user_manual/painting_with_assistants.rst:215
msgid ".. image:: images/assistants/Assistants_2_point_perspective.png"
msgstr ".. image:: images/assistants/Assistants_2_point_perspective.png"

#: ../../user_manual/painting_with_assistants.rst:217
msgid ""
"A 2 point perspective is set up using 2 vanishing point and 1 vertical "
"parallel ruler. Often, putting the vanishing points outside the frame a "
"little can decrease the strength of it."
msgstr ""
"É configurada uma perspectiva com 2 pontos, usando 2 pontos de fuga e uma "
"régua paralela perpendicular. Normalmente, se colocar os pontos de fuga um "
"pouco fora da área de desenho poderá reduzir a potência da mesma."

#: ../../user_manual/painting_with_assistants.rst:219
msgid "2 Point Perspective"
msgstr "Perspectiva de 2 Pontos"

#: ../../user_manual/painting_with_assistants.rst:222
msgid ".. image:: images/assistants/Assistants_2_pointperspective_02.png"
msgstr ".. image:: images/assistants/Assistants_2_pointperspective_02.png"

#: ../../user_manual/painting_with_assistants.rst:224
msgid ".. image:: images/assistants/Assistants_3_point_perspective.png"
msgstr ".. image:: images/assistants/Assistants_3_point_perspective.png"

#: ../../user_manual/painting_with_assistants.rst:226
msgid "3 Point Perspective"
msgstr "Perspectiva de 3 Pontos"

#: ../../user_manual/painting_with_assistants.rst:226
msgid "A 3 point perspective is set up using 3 vanishing point rulers."
msgstr "É configurada uma perspectiva com 3 réguas de pontos de fuga."

#: ../../user_manual/painting_with_assistants.rst:229
msgid "Logic of the vanishing point"
msgstr "Lógica do ponto de fuga"

#: ../../user_manual/painting_with_assistants.rst:231
msgid ""
"There's a little secret that perspective tutorials don't always tell you, "
"and that's that a vanishing point is the point where any two parallel lines "
"meet. This means that a 1 point perspective and 2 point perspective are "
"virtually the same."
msgstr ""
"Existe um pequeno segredo que os tutoriais sobre perspectivas nem sempre "
"ensinam, e que um ponto de fuga é o ponto onde quaisquer duas linhas "
"paralelas se encontram. Isto significa que uma perspectiva de 1 ponto e "
"outra de 2 pontos são virtualmente iguais."

#: ../../user_manual/painting_with_assistants.rst:233
msgid ""
"We can prove this via a little experiment. That good old problem: drawing a "
"rail-road."
msgstr ""
"Podemos provar isto com uma pequena experiência. Aquele antigo problema "
"típico: desenhar um caminho de ferro."

#: ../../user_manual/painting_with_assistants.rst:236
msgid ".. image:: images/assistants/Assistants_vanishing_point_logic_01.png"
msgstr ".. image:: images/assistants/Assistants_vanishing_point_logic_01.png"

#: ../../user_manual/painting_with_assistants.rst:237
msgid ""
"You are probably familiar with the problem: How to determine where the next "
"beam is going to be, as perspective projection will make them look closer "
"together."
msgstr ""
"Provavelmente já estará familiarizado com o problema: Como determinar onde "
"ficará o próximo raio, dado que a projecção em perspectiva os tornará cada "
"vez mais próximos entre si."

#: ../../user_manual/painting_with_assistants.rst:240
msgid ""
"Typically, the solution is to draw a line in the middle and then draw lines "
"diagonally across. After all, those lines are parallel, meaning that the "
"exact same distance is used."
msgstr ""
"Tipicamente a solução é desenhar uma linha no meio e depois desenhar as "
"linhas na diagonal. No fim de tudo, essas linhas são paralelas, o que "
"significa que é usada a mesma distância com exactidão."

#: ../../user_manual/painting_with_assistants.rst:243
msgid ".. image:: images/assistants/Assistants_vanishing_point_logic_02.png"
msgstr ".. image:: images/assistants/Assistants_vanishing_point_logic_02.png"

#: ../../user_manual/painting_with_assistants.rst:244
msgid ""
"But because they are parallel, we can use a vanishing point assistant "
"instead, and we use the alignment handles to align it to the diagonal of the "
"beam, and to the horizontal (here marked with red)."
msgstr ""
"Mas, dado que são paralelas, podemos usar um assistente de ponto de fuga, "
"onde iremos usar as pegas de alinhamento para as alinhar à diagonal do raio "
"e à horizontal (aqui marcadas a vermelho)."

#: ../../user_manual/painting_with_assistants.rst:247
msgid ""
"That diagonal can then in turn be used to determine the position of the "
"beams:"
msgstr ""
"Essa diagonal poderá por seu turno ser usada para determinar a posição dos "
"raios:"

#: ../../user_manual/painting_with_assistants.rst:251
msgid ".. image:: images/assistants/Assistants_vanishing_point_logic_03.png"
msgstr ".. image:: images/assistants/Assistants_vanishing_point_logic_03.png"

#: ../../user_manual/painting_with_assistants.rst:252
msgid ""
"Because any given set of lines has a vanishing point (outside of the ones "
"flat on the view-plane), there can be an infinite amount of vanishing points "
"in a linear perspective. Therefore, Krita allows you to set vanishing points "
"yourself instead of forcing you to only use a few."
msgstr ""
"Dado que qualquer conjunto de linhas tem um ponto de fuga (fora das que se "
"encontram no plano de visualização), poderá existir um número infinito de "
"pontos de fuga numa perspectiva linear. Como tal, o Krita permite-lhe "
"definir você mesmo os pontos de fuga em vez de o forçar a usar apenas alguns."

#: ../../user_manual/painting_with_assistants.rst:255
msgid "Fish Eye perspective"
msgstr "Perspectiva de olho-de-peixe"

#: ../../user_manual/painting_with_assistants.rst:257
msgid ""
"Fish eye perspective works much the same as the linear perspective, the big "
"difference being that in a fish-eye perspective, any parallel set of lines "
"has two vanishing points, each for one side."
msgstr ""
"A perspectiva de olho-de-peixe funciona de forma muito semelhante à da "
"perspectiva linear, com a grande diferença que, numa perspectiva em olho-de-"
"peixe, qualquer conjunto de linhas paralelas tem dois pontos de fuga, um "
"para cada lado."

#: ../../user_manual/painting_with_assistants.rst:261
msgid ""
"So, to set them up, the easiest way is one horizontal, one vertical, on the "
"same spot, and one vanishing point assistant in the middle."
msgstr ""
"Como tal, para os configurar, a forma mais simples é um na horizontal, outro "
"na vertical, no mesmo ponto, e um assistente do ponto de fuga no meio."

#: ../../user_manual/painting_with_assistants.rst:265
msgid ".. image:: images/assistants/Fish-eye.gif"
msgstr ".. image:: images/assistants/Fish-eye.gif"

#: ../../user_manual/painting_with_assistants.rst:266
msgid ""
"But, you can also make one horizontal one that is just as big as the other "
"horizontal one, and put it halfway:"
msgstr ""
"Mas também poderá criar um novo horizontal que seja tão grande como o outro "
"horizontal, e colocá-lo a meio caminho:"
