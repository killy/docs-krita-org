# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:06+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: animationdocker en Cup guilabel image tweening\n"
"X-POFile-SpellExtra: menuselection kbd Animationimportsprites\n"
"X-POFile-SpellExtra: Animationimportdone Libre images alt DPI\n"
"X-POFile-SpellExtra: timelinedocker OSX Animationseteverything icons\n"
"X-POFile-SpellExtra: Kickstarter gif program renderanimation ref\n"
"X-POFile-SpellExtra: onionskindocker Kritamouseleft\n"
"X-POFile-SpellExtra: Animationsplitspritesheet filtercolortoalpha\n"
"X-POFile-SpellExtra: VirtualDub mouseright\n"
"X-POFile-SpellExtra: Introductiontoanimationwalkcycle Art Krita\n"
"X-POFile-SpellExtra: nomeficheiroXXX Kritamouseright\n"
"X-POFile-SpellExtra: Introductiontoanimation Open mouseleft animation\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: botão direito"

#: ../../user_manual/animation.rst:1
msgid "Detailed guide on the animation workflow in Krita."
msgstr "Guia detalhado sobre o fluxo de animações no Krita."

#: ../../user_manual/animation.rst:13
msgid "Animation"
msgstr "Animação"

#: ../../user_manual/animation.rst:18
msgid "Animation with Krita"
msgstr "Animação com o Krita"

#: ../../user_manual/animation.rst:20
msgid ""
"Thanks to the 2015 Kickstarter, :program:`Krita 3.0` now has animation. In "
"specific, :program:`Krita` has frame-by-frame raster animation. There's "
"still a lot of elements missing from it, like tweening, but the basic "
"workflow is there."
msgstr ""
"Graças ao programa Kickstarter de 2015, o :program:`Krita 3.0` agora tem "
"suporta para animações. Em específico, o :program:`Krita` tem a animação de "
"imagens imagem-a-imagem. Existem ainda bastantes elementos em falta nele, "
"como o 'tweening' (preenchimento de imagens intermédias), mas o fluxo básico "
"está aqui."

#: ../../user_manual/animation.rst:25
msgid ""
"To access the animation features, the easiest way is to change your "
"workspace to Animation. This will make the animation dockers and workflow "
"appear."
msgstr ""
"Para aceder às funcionalidade de animação, a forma mais fácil é mudar o seu "
"espaço de trabalho para Animação. Isto fará com que as áreas de animação "
"apareçam."

#: ../../user_manual/animation.rst:30
msgid "Animation curves"
msgstr "Curvas da animação"

#: ../../user_manual/animation.rst:32
msgid ""
"To create an animation curve (currently only for opacity) expand the :"
"guilabel:`New Frame` button in the :guilabel:`Animation` dock and click :"
"guilabel:`Add Opacity Keyframe`. You can now edit the keyframed value for "
"opacity directly in the “Layers” dock, adding more keyframes will by default "
"fade from the last to the next upcoming keyframe in the timeline over the "
"frames between them. See :ref:`animation curves <animation_curves_docker>` "
"for details."
msgstr ""
"Para criar uma curva de animação (de momento só para a opacidade), expanda o "
"botão :guilabel:`Nova Imagem` na área de :guilabel:`Animação` e carregue em :"
"guilabel:`Adicionar uma Imagem-Chave da Opacidade`. Poderá agora editar o "
"valor da imagem-chave da opacidade directamente na área de “Camadas”; se "
"adicionar mais imagens-chave, irá por omissão desvanecer da última imagem-"
"chave até à próxima na linha temporal, sobre as imagens entre elas. Veja "
"mais detalhes nas :ref:`curvas de animação <animation_curves_docker>`."

#: ../../user_manual/animation.rst:40
msgid "Workflow"
msgstr "Fluxograma"

#: ../../user_manual/animation.rst:42
msgid ""
"In traditional animation workflow, what you do is that you make *key "
"frames*, which contain the important poses, and then draw frames in between "
"(\\ *tweening* in highly sophisticated animator's jargon)."
msgstr ""
"No fluxo de trabalho tradicional da animação, o que você costuma fazer é "
"criar *imagens-chave*, que contêm as poses importantes, e depois desenhar "
"imagens no meio (\\ *tweening* no idioma dos animadores sofisticados)."

#: ../../user_manual/animation.rst:46
msgid "For this workflow, there are three important dockers:"
msgstr "Para este fluxo, existem três áreas importantes:"

#: ../../user_manual/animation.rst:48
msgid ""
"The :ref:`timeline_docker`. View and control all of the frames in your "
"animation. The timeline docker also contains functions to manage your "
"layers. The layer that are created in the timeline docker also appear on the "
"normal Layer docker."
msgstr ""
"A :ref:`timeline_docker`. Veja e controle todas as imagens na sua animação. "
"A área da linha temporal também contém funções para gerir as suas camadas. "
"As camadas que forem criadas na área da linha temporal também aparecem na "
"área de Camadas normal."

#: ../../user_manual/animation.rst:52
msgid ""
"The :ref:`animation_docker`. This docker contains the play buttons as the "
"ability to change the frame-rate, playback speed and useful little options "
"like :guilabel:`auto-key framing`."
msgstr ""
"A :ref:`animation_docker`. Esta área contém alguns botões de reprodução, "
"como a capacidade de mudar a taxa de imagens, a velocidade de reprodução e "
"mais algumas pequenas opções úteis como as :guilabel:`imagens-chave "
"automáticas`."

#: ../../user_manual/animation.rst:55
msgid ""
"The :ref:`onion_skin_docker`. This docker controls the look of the onion "
"skin, which in turn is useful for seeing the previous frame."
msgstr ""
"A :ref:`onion_skin_docker`. Esta área controla a aparência da 'pele de "
"cebola', que por sua vez é útil para ver a imagem anterior."

#: ../../user_manual/animation.rst:60
msgid "Introduction to animation: How to make a walkcycle"
msgstr "Introdução à animação: Como criar um homem a andar"

#: ../../user_manual/animation.rst:62
msgid ""
"The best way to get to understand all these different parts is to actually "
"use them. Walk cycles are considered the most basic form of a full "
"animation, because of all the different parts involved with them. Therefore, "
"going over how one makes a walkcycle should serve as a good introduction."
msgstr ""
"A melhor forma de compreender todas estas diferentes componentes é usá-las "
"de facto. As pessoas a andar são consideradas a forma mais básica de uma "
"animação completa, por causa de todas as diferentes partes envolvidas com "
"elas. Como tal, fazer uma pessoa a andar deverá servir como uma boa "
"introdução."

#: ../../user_manual/animation.rst:69
msgid "Setup"
msgstr "Configuração"

#: ../../user_manual/animation.rst:71
msgid "First, we make a new file:"
msgstr "Primeiro, iremos criar um novo ficheiro:"

#: ../../user_manual/animation.rst:74
msgid ".. image:: images/animation/Introduction_to_animation_01.png"
msgstr ".. image:: images/animation/Introduction_to_animation_01.png"

#: ../../user_manual/animation.rst:75
msgid ""
"On the first tab, we type in a nice ratio like 1280x1024, set the dpi to 72 "
"(we're making this for screens after all) and title the document 'walkcycle'."
msgstr ""
"Na primeira página, iremos indicar umas proporções aceitáveis, como o "
"1280x1024, configurar a resolução em DPI como 72 (estamos a criar isto para "
"ecrãs) e intitular 'homem a andar'."

#: ../../user_manual/animation.rst:79
msgid ""
"In the second tab, we choose a nice background color, and set the background "
"to canvas-color. This means that Krita will automatically fill in any "
"transparent bits with the background color. You can change this in :"
"menuselection:`Image --> Image Properties`. This seems to be most useful to "
"people doing animation, as the layer you do animation on MUST be semi-"
"transparent to get onion skinning working."
msgstr ""
"Na segunda página, escolhemos uma bonita cor de fundo e configuramos o fundo "
"com a cor da área de desenho. Isto significa que o Krita irá preencher "
"automaticamente todas as partes transparentes com a cor de fundo. Poderá "
"alterar isto em :menuselection:`Imagem --> Propriedades da Imagem`. Isto "
"parece ser mais útil para as pessoas que fazem as animações, dado que a "
"camada onde costumam fazer a animação DEVERÁ ser semi-transparente para ter "
"a 'pele de cebola' a funcionar."

#: ../../user_manual/animation.rst:82
msgid ""
"Krita has a bunch of functionality for meta-data, starting at the :guilabel:"
"`Create Document` screen. The title will be automatically used as a "
"suggestion for saving and the description can be used by databases, or for "
"you to leave comments behind. Not many people use it individually, but it "
"can be useful for working in larger groups."
msgstr ""
"O Krita tem um grande conjunto de funcionalidades para os meta-dados, a "
"começar pelo ecrã para :guilabel:`Criar um Documento`. O título será usado "
"automaticamente como uma sugestão para a gravação e a descrição poderá ser "
"usada pelas bases de dados, ou então para você poder deixar alguns "
"comentários. Não é muito usados pelas pessoas individualmente, mas poderá "
"ser bastante útil ao trabalhar em grandes grupos."

#: ../../user_manual/animation.rst:84
msgid "Then hit :guilabel:`Create`!"
msgstr "Depois, carregue em :guilabel:`Criar`!"

#: ../../user_manual/animation.rst:86
msgid ""
"Then, to get all the necessary tools for animation, select the workspace "
"switcher:"
msgstr ""
"Depois, para obter todas as ferramentas necessárias para a animação, "
"seleccione o selector de espaços de trabalho:"

#: ../../user_manual/animation.rst:91
msgid ".. image:: images/animation/Introduction_to_animation_02.png"
msgstr ".. image:: images/animation/Introduction_to_animation_02.png"

#: ../../user_manual/animation.rst:91
msgid "The red arrow points at the workspace switcher."
msgstr "A seta vermelha aponta para o selector de espaços de trabalho."

#: ../../user_manual/animation.rst:93
msgid "And select the animation workspace."
msgstr "E seleccionar o espaço de trabalho da animação."

#: ../../user_manual/animation.rst:95
msgid "Which should result in this:"
msgstr "O que deverá resultar nisto:"

#: ../../user_manual/animation.rst:98
msgid ".. image:: images/animation/Introduction_to_animation_03.png"
msgstr ".. image:: images/animation/Introduction_to_animation_03.png"

#: ../../user_manual/animation.rst:99
msgid ""
"The animation workspace adds the timeline, animation and onion skin dockers "
"at the bottom."
msgstr ""
"O espaço de trabalho da animação adiciona a linha temporal, as áreas de "
"animação e da 'pele de cebola' no fundo."

#: ../../user_manual/animation.rst:103
msgid "Animating"
msgstr "Animação"

#: ../../user_manual/animation.rst:105
msgid ""
"We have two transparent layers set up. Let's name the bottom one "
"'environment' and the top 'walkcycle' by double clicking their names in the "
"layer docker."
msgstr ""
"Temos duas camadas transparentes configuradas. Vamos chamar à de baixo "
"'ambiente' e à de topo 'homem a andar', fazendo duplo-click sobre os nomes "
"deles na área de camadas."

#: ../../user_manual/animation.rst:110
msgid ".. image:: images/animation/Introduction_to_animation_04.png"
msgstr ".. image:: images/animation/Introduction_to_animation_04.png"

#: ../../user_manual/animation.rst:111
msgid ""
"Use the straight line tool to draw a single horizontal line. This is the "
"ground."
msgstr ""
"Use a ferramenta da linha recta para desenhar uma simples linha horizontal. "
"Isto é o chão."

#: ../../user_manual/animation.rst:115
msgid ".. image:: images/animation/Introduction_to_animation_05.png"
msgstr ".. image:: images/animation/Introduction_to_animation_05.png"

#: ../../user_manual/animation.rst:116
msgid ""
"Then, select the 'walkcycle' layer and draw a head and torso (you can use "
"any brush for this)."
msgstr ""
"Depois, seleccione a camada do 'homem a andar' e desenhe uma cabeça e tronco "
"(poderá usar qualquer pincel para esse fim)."

#: ../../user_manual/animation.rst:118
msgid ""
"Now, selecting a new frame will not make a new frame automatically. Krita "
"doesn't actually see the 'walkcycle' layer as an animated layer at all!"
msgstr ""
"Agora, seleccionar uma nova imagem não irá criar uma imagem automática nova. "
"O Krita não vê actualmente a camada do 'homem a andar' como uma camada "
"animada de todo!"

#: ../../user_manual/animation.rst:123
msgid ".. image:: images/animation/Introduction_to_animation_06.png"
msgstr ".. image:: images/animation/Introduction_to_animation_06.png"

#: ../../user_manual/animation.rst:124
msgid ""
"We can make it animatable by adding a frame to the timeline. |mouseright| a "
"frame in the timeline to get a context menu. Choose :guilabel:`New Frame`."
msgstr ""
"Podemos torná-la apta para animação, adicionando uma imagem à linha "
"temporal. Use o |mouseright| sobre uma imagem na linha temporal para obter "
"um menu de contexto. Escolha :guilabel:`Nova Imagem`."

#: ../../user_manual/animation.rst:128
msgid ".. image:: images/animation/Introduction_to_animation_07.png"
msgstr ".. image:: images/animation/Introduction_to_animation_07.png"

#: ../../user_manual/animation.rst:129
msgid ""
"You can see it has become an animated layer because of the onion skin icon "
"showing up in the timeline docker."
msgstr ""
"Poderá ver que se tornou uma camada animada pelo ícone da pele de cebola que "
"aparece na área da linha temporal."

#: ../../user_manual/animation.rst:133
msgid ".. image:: images/animation/Introduction_to_animation_08.png"
msgstr ".. image:: images/animation/Introduction_to_animation_08.png"

#: ../../user_manual/animation.rst:134
msgid ""
"Use the :guilabel:`Copy Frame` button to copy the first frame onto the "
"second. Then, use the with the :kbd:`Shift + ↑` shortcut to move the frame "
"contents up."
msgstr ""
"Use o botão :guilabel:`Copiar a Imagem` para copiar a primeira imagem para a "
"segunda. Depois, use-a com a combinação :kbd:`Shift + ↑` para subir o "
"conteúdo da imagem."

#: ../../user_manual/animation.rst:137
msgid "We can see the difference by turning on the onionskinning:"
msgstr "Agora poderá ver a diferença se activar a 'pele de cebola':"

#: ../../user_manual/animation.rst:140
msgid ".. image:: images/animation/Introduction_to_animation_09.png"
msgstr ".. image:: images/animation/Introduction_to_animation_09.png"

#: ../../user_manual/animation.rst:141
msgid "Now, you should see the previous frame as red."
msgstr "Agora deverá ver a imagem anterior a vermelho."

#: ../../user_manual/animation.rst:144
msgid ""
"Krita sees white as a color, not as transparent, so make sure the animation "
"layer you are working on is transparent in the bits where there's no "
"drawing. You can fix the situation by use the :ref:`filter_color_to_alpha` "
"filter, but prevention is best."
msgstr ""
"O Krita vê o branco como uma cor, não como transparente, por isso certifique-"
"se que a camada de animação onde está a trabalhar é transparente nas partes "
"em que não existem desenhos. Poderá corrigir a situação se usar o filtro :"
"ref:`filter_color_to_alpha`, mas mais vale prevenir."

#: ../../user_manual/animation.rst:147
msgid ".. image:: images/animation/Introduction_to_animation_10.png"
msgstr ".. image:: images/animation/Introduction_to_animation_10.png"

#: ../../user_manual/animation.rst:148
msgid ""
"Future frames are drawn in green, and both colors can be configured in the "
"onion skin docker."
msgstr ""
"As imagens futuras são desenhadas a verde, sendo que ambas as cores podem "
"ser configuradas na área da 'pele de cebola'."

#: ../../user_manual/animation.rst:152
msgid ".. image:: images/animation/Introduction_to_animation_11.png"
msgstr ".. image:: images/animation/Introduction_to_animation_11.png"

#: ../../user_manual/animation.rst:153
msgid ""
"Now, we're gonna draw the two extremes of the walkcycle. These are the pose "
"where both legs are as far apart as possible, and the pose where one leg is "
"full stretched and the other pulled in, ready to take the next step."
msgstr ""
"Agora, iremos desenhar os dois extremos do passeio da pessoa. Estas são a "
"pose em que ambas as pernas estão o mais afastadas possíveis, e a pose onde "
"uma perna está completamente esticada e a outra está preparada para dar o "
"próximo passo."

#: ../../user_manual/animation.rst:158
msgid ""
"Now, let's copy these two... We could do that with the :kbd:`Ctrl + drag` "
"shortcut, but here comes a tricky bit:"
msgstr ""
"Agora, vamos copiar estes dois... Podemos fazê-lo com o :kbd:`Ctrl + "
"arrastamento`, mas assim aparece um pequeno truque:"

#: ../../user_manual/animation.rst:162
msgid ".. image:: images/animation/Introduction_to_animation_12.png"
msgstr ".. image:: images/animation/Introduction_to_animation_12.png"

#: ../../user_manual/animation.rst:163
msgid ""
":kbd:`Ctrl +` |mouseleft| also selects and deselects frames, so to copy..."
msgstr ""
"O :kbd:`Ctrl +` |mouseleft| também selecciona e deselecciona as imagens; por "
"isso, para copiar..."

#: ../../user_manual/animation.rst:165
msgid ":kbd:`Ctrl +` |mouseleft| to select all the frames you want to select."
msgstr ""
":kbd:`Ctrl +` |mouseleft| para seleccionar todas as imagens que deseja "
"seleccionar."

#: ../../user_manual/animation.rst:166
msgid ""
":kbd:`Ctrl + drag`. You need to make sure the first frame is 'orange', "
"otherwise it won't be copied along."
msgstr ""
":kbd:`Ctrl + arrastamento`. Tem de se certificar que a primeira imagem é a "
"'laranja', caso contrário não será copiada em conjunto."

#: ../../user_manual/animation.rst:169
msgid "Now then..."
msgstr "E agora..."

#: ../../user_manual/animation.rst:174
msgid ".. image:: images/animation/Introduction_to_animation_13.png"
msgstr ".. image:: images/animation/Introduction_to_animation_13.png"

#: ../../user_manual/animation.rst:174
msgid "squashed the timeline docker a bit to save space"
msgstr "esmagou a área da linha temporal um bocado para ganhar espaço"

#: ../../user_manual/animation.rst:176
msgid "Copy frame 0 to frame 2."
msgstr "Copie a imagem 0 para a imagem 2."

#: ../../user_manual/animation.rst:177
msgid "Copy frame 1 to frame 3."
msgstr "Copie a imagem 1 para a imagem 3."

#: ../../user_manual/animation.rst:178
msgid "In the animation docker, set the frame-rate to 4."
msgstr "Na área de animação, configure a taxa de imagens como 4."

#: ../../user_manual/animation.rst:179
msgid "Select all frames in the timeline docker by dragging-selecting them."
msgstr ""
"Seleccione todas as imagens na área da linha temporal, seleccionando-as por "
"arrastamento."

#: ../../user_manual/animation.rst:180
msgid "Press play in the animation docker."
msgstr "Carregue em Reproduzir na área de animação."

#: ../../user_manual/animation.rst:181
msgid "Enjoy your first animation!"
msgstr "Desfrute da sua primeira animação!"

#: ../../user_manual/animation.rst:184
msgid "Expanding upon your rough walkcycle"
msgstr "Expandir o seu homem a andar básico"

#: ../../user_manual/animation.rst:187
msgid ".. image:: images/animation/Introduction_to_animation_14.png"
msgstr ".. image:: images/animation/Introduction_to_animation_14.png"

#: ../../user_manual/animation.rst:188
msgid ""
"You can quickly make some space by the :kbd:`Alt + drag` shortcut on any "
"frame. This'll move that frame and all others after it in one go."
msgstr ""
"Poderá criar rapidamente algum espaço se fizer :kbd:`Alt + arrastamento` de "
"qualquer imagem. Isto irá mover essa imagem e todas as que se seguem de uma "
"vez."

#: ../../user_manual/animation.rst:191
msgid "Then draw inbetweens on each frame that you add."
msgstr "Depois desenhe, nos espaços intermédios, em cada imagem que adicionou."

#: ../../user_manual/animation.rst:194
msgid ".. image:: images/animation/Introduction_to_animation_16.png"
msgstr ".. image:: images/animation/Introduction_to_animation_16.png"

#: ../../user_manual/animation.rst:195
msgid ""
"You'll find that the more frames you add, the more difficult it becomes to "
"keep track of the onion skins."
msgstr ""
"Irá reparar que, quanto mais imagens adicionar, mais difícil se tornará "
"acompanhar as 'peles de cebola'."

#: ../../user_manual/animation.rst:197
msgid ""
"You can modify the onion skin by using the onion skin docker, where you can "
"change how many frames are visible at once, by toggling them on the top row. "
"The bottom row is for controlling transparency, while below there you can "
"modify the colors and extremity of the coloring."
msgstr ""
"Poderá modificar a 'pele de cebola', usando a área respectiva, onde poderá "
"modificar quantas imagens estão visíveis de uma vez, comutando-as na fila "
"superior. A fila inferior serve para controlar a transparência, onde abaixo "
"dela poderá modificar as cores e o nível da coloração."

#: ../../user_manual/animation.rst:203
msgid ".. image:: images/animation/Introduction_to_animation_15.png"
msgstr ".. image:: images/animation/Introduction_to_animation_15.png"

#: ../../user_manual/animation.rst:205
msgid "Animating with multiple layers"
msgstr "Animação com várias camadas"

#: ../../user_manual/animation.rst:207
msgid ""
"Okay, our walkcycle is missing some hands, let's add them on a separate "
"layer. So we make a new layer, and name it hands and..."
msgstr ""
"Ok, o nosso homem a andar não tem braços; vamos adicioná-los numa camada "
"separada. Como tal, vamos criar uma nova camada, chamar-lhe 'braços' e..."

#: ../../user_manual/animation.rst:211
msgid ".. image:: images/animation/Introduction_to_animation_17.png"
msgstr ".. image:: images/animation/Introduction_to_animation_17.png"

#: ../../user_manual/animation.rst:212
msgid ""
"Our walkcycle is gone from the timeline docker! This is a feature actually. "
"A full animation can have so many little parts that an animator might want "
"to remove the layers they're not working on from the timeline docker. So you "
"manually have to add them."
msgstr ""
"O nosso boneco a passear saiu da área da linha temporal! Isto é uma "
"funcionalidade, de facto. Uma animação completa poderá ter tantas partes "
"pequenas que um animador poderá querer remover as camadas onde não estão a "
"trabalhar da área da linha temporal. Como tal, terá de as adicionar "
"manualmente."

#: ../../user_manual/animation.rst:218
msgid ".. image:: images/animation/Introduction_to_animation_18.png"
msgstr ".. image:: images/animation/Introduction_to_animation_18.png"

#: ../../user_manual/animation.rst:219
msgid ""
"You can show any given layer in the timeline by doing |mouseright| on the "
"layer in the layer docker, and toggling :guilabel:`Show in Timeline`."
msgstr ""
"Poderá mostrar qualquer camada indicada na linha temporal se usar o |"
"mouseright| sobre a camada, na área de camadas, e activar a opção :guilabel:"
"`Mostrar na Linha Temporal`."

#: ../../user_manual/animation.rst:223
msgid ".. image:: images/animation/Introduction_to_animation_19.png"
msgstr ".. image:: images/animation/Introduction_to_animation_19.png"

#: ../../user_manual/animation.rst:225
msgid "Exporting"
msgstr "Exportação"

#: ../../user_manual/animation.rst:227
msgid "When you are done, select :menuselection:`File --> Render Animation`."
msgstr ""
"Quando terminar, seleccione a opção :menuselection:`Ficheiro --> Desenhar a "
"Animação`."

#: ../../user_manual/animation.rst:230
msgid ".. image:: images/animation/Introduction_to_animation_20.png"
msgstr ".. image:: images/animation/Introduction_to_animation_20.png"

#: ../../user_manual/animation.rst:231
msgid ""
"It's recommended to save out your file as a png, and preferably in its own "
"folder. Krita can currently only export png sequences."
msgstr ""
"Recomenda-se que grave o seu ficheiro como um PNG, e de preferência na sua "
"própria pasta. O Krita só consegue exportar sequências em PNG."

#: ../../user_manual/animation.rst:235
msgid ".. image:: images/animation/Introduction_to_animation_21.png"
msgstr ".. image:: images/animation/Introduction_to_animation_21.png"

#: ../../user_manual/animation.rst:236
msgid ""
"When pressing done, you can see the status of the export in the status bar "
"below."
msgstr ""
"Quando carregar em Terminar, poderá ver o estado da exportação na barra de "
"estado abaixo."

#: ../../user_manual/animation.rst:240
msgid ".. image:: images/animation/Introduction_to_animation_22.png"
msgstr ".. image:: images/animation/Introduction_to_animation_22.png"

#: ../../user_manual/animation.rst:241
msgid ""
"The images should be saved out as filenameXXX.png, giving their frame number."
msgstr ""
"As imagens deverão ficar gravadas como nomeficheiroXXX.png, sendo-lhe "
"atribuído o seu número de imagem."

#: ../../user_manual/animation.rst:244
msgid ""
"Then use something like Gimp (Linux, OSX, Windows), ImageMagick (Linux, OSX, "
"Windows), or any other gif creator to make a gif out of your image sequence:"
msgstr ""
"Depois use algo como o Gimp (Linux, OSX, Windows), o ImageMagick (Linux, "
"OSX, Windows) ou outro criador de GIF's para compor um GIF a partir da sua "
"sequência de imagens:"

#: ../../user_manual/animation.rst:249
msgid ".. image:: images/animation/Introduction_to_animation_walkcycle_02.gif"
msgstr ".. image:: images/animation/Introduction_to_animation_walkcycle_02.gif"

#: ../../user_manual/animation.rst:250
msgid ""
"For example, you can use `VirtualDub <http://www.virtualdub.org/>`_\\ "
"(Windows) and open all the frames and then go to :menuselection:`File --> "
"Export --> GIF`."
msgstr ""
"Por exemplo, poderá usar o `VirtualDub <http://www.virtualdub.org/>`__\\ "
"(Windows), abrir todas as imagens e depois ir a :menuselection:`Ficheiro --> "
"Exportar --> GIF`."

#: ../../user_manual/animation.rst:254
msgid "Enjoy your walkcycle!"
msgstr "Desfrute do seu homem a andar!"

#: ../../user_manual/animation.rst:258
msgid ""
"Krita 3.1 has a render animation feature. If you're using the 3.1 beta, "
"check out the :ref:`render_animation` page for more information!"
msgstr ""
"O Krita 3.1 tem uma funcionalidade para desenhar a animação. Se estiver a "
"usar a versão 3.1 beta, consulte a página :ref:`render_animation` para obter "
"mais informações!"

#: ../../user_manual/animation.rst:261
msgid "Importing animation frames"
msgstr "Importar as imagens da animação"

#: ../../user_manual/animation.rst:263
msgid "You can import animation frames in Krita 3.0."
msgstr "Poderá importar imagens da animação no Krita 3.0."

#: ../../user_manual/animation.rst:265
msgid ""
"First let us take a sprite sheet from Open Game Art. (This is the Libre "
"Pixel Cup male walkcycle)"
msgstr ""
"Primeiro vamos obter uma folha de imagens da Open Game Art. (Este é o homem "
"a andar do Libre Pixel Cup)"

#: ../../user_manual/animation.rst:268
msgid ""
"And we'll use :menuselection:`Image --> Split Image` to split up the sprite "
"sheet."
msgstr ""
"E iremos usar a opção :menuselection:`Imagem --> Dividir a Imagem` para "
"decompor a folha de imagens."

#: ../../user_manual/animation.rst:271
msgid ".. image:: images/animation/Animation_split_spritesheet.png"
msgstr ".. image:: images/animation/Animation_split_spritesheet.png"

#: ../../user_manual/animation.rst:272
msgid ""
"The slices are even, so for a sprite sheet of 9 sprites, use 8 vertical "
"slices and 0 horizontal slices. Give it a proper name and save it as png."
msgstr ""
"As fatias têm o mesmo tamanho; por isso, para uma folha de imagens com 9 "
"imagens, use 8 fatias verticais e 0 fatias horizontais. Atribua-lhes um nome "
"adequado e grave-as como PNG."

#: ../../user_manual/animation.rst:274
msgid ""
"Then, make a new canvas, and select :menuselection:`File --> Import "
"Animation Frames`. This will give you a little window. Select :guilabel:`Add "
"images`. This should get you a file browser where you can select your images."
msgstr ""
"Depois, crie uma nova área de desenho e seleccione a opção :menuselection:"
"`Ficheiro --> Importar as Imagens da Animação`. Isto abrir-lhe-á uma pequena "
"janela. Seleccione a opção :guilabel:`Adicionar imagens`. Isto dever-lhe-á "
"apresentar um selector de ficheiros para escolher as suas imagens."

#: ../../user_manual/animation.rst:277
msgid ".. image:: images/animation/Animation_import_sprites.png"
msgstr ".. image:: images/animation/Animation_import_sprites.png"

#: ../../user_manual/animation.rst:278
msgid "You can select multiple images at once."
msgstr "Poderá seleccionar várias imagens de uma vez."

#: ../../user_manual/animation.rst:281
msgid ".. image:: images/animation/Animation_set_everything.png"
msgstr ".. image:: images/animation/Animation_set_everything.png"

#: ../../user_manual/animation.rst:282
msgid ""
"The frames are currently automatically ordered. You can set the ordering "
"with the top-left two drop-down boxes."
msgstr ""
"As imagens aparecem automaticamente ordenadas de momento. Poderá definir a "
"ordenação com as duas listas do canto superior-esquerdo."

#: ../../user_manual/animation.rst:285
msgid "Start"
msgstr "Início"

#: ../../user_manual/animation.rst:286
msgid "Indicates at which point the animation should be imported."
msgstr "Indica em que ponto deverá ser importada a animação."

#: ../../user_manual/animation.rst:288
msgid ""
"Indicates the difference between the imported animation and the document "
"frame rate. This animation is 8 frames big, and the fps of the document is "
"24 frames, so there should be a step of 3 to keep it even. As you can see, "
"the window gives feedback on how much fps the imported animation would be "
"with the currently given step."
msgstr ""
"Indica a diferença entre a animação importada e a taxa de imagens do "
"documento. Esta animação tem 8 imagens de tamanho, e a taxa de imagens do "
"documento é de 24 imagens, pelo que deverá haver um passo igual 3 para as "
"manter sincronizadas. Como poderá ver, a janela dar-lhe-á a reacção de "
"quantas imagens-por-segundo a animação importada ficaria com o passo "
"indicado actualmente."

#: ../../user_manual/animation.rst:292
msgid "Step"
msgstr "Passo"

#: ../../user_manual/animation.rst:294
msgid ""
"Press :guilabel:`OK`, and your animation should be imported as a new layer."
msgstr ""
"Carregue em :guilabel:`OK`, para que a sua animação seja importada como uma "
"nova camada."

#: ../../user_manual/animation.rst:297
msgid ".. image:: images/animation/Animation_import_done.png"
msgstr ".. image:: images/animation/Animation_import_done.png"

#: ../../user_manual/animation.rst:299
msgid "Reference"
msgstr "Referência"

#: ../../user_manual/animation.rst:301
msgid "https://community.kde.org/Krita/Docs/AnimationGuiFeaturesList"
msgstr "https://community.kde.org/Krita/Docs/AnimationGuiFeaturesList"

#: ../../user_manual/animation.rst:302
msgid ""
"`The source for the libre pixel cup male walkmediawiki cycle <https://"
"opengameart.org/content/liberated-pixel-cup-lpc-base-assets-sprites-map-"
"tiles>`_"
msgstr ""
"`A fonte da personagem masculina do Libre Pixel Cup <https://opengameart.org/"
"content/liberated-pixel-cup-lpc-base-assets-sprites-map-tiles>`_"
