# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:06+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: toolbeziercurve Bézier icons Krita shift image\n"
"X-POFile-SpellExtra: Kritamouseleft kbd Kritamouseright guilabel program\n"
"X-POFile-SpellExtra: Del Enter bézier images beziercurve alt mouseright\n"
"X-POFile-SpellExtra: mouseleft\n"

#: ../../<generated>:1
msgid "Activate Angle Snap"
msgstr "Activar o Ajuste do Ângulo"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: botão direito"

#: ../../<rst_epilog>:34
msgid ""
".. image:: images/icons/bezier_curve.svg\n"
"   :alt: toolbeziercurve"
msgstr ""
".. image:: images/icons/bezier_curve.svg\n"
"   :alt: ferramenta de curva bézier"

#: ../../reference_manual/tools/path.rst:1
msgid "Krita's path tool reference."
msgstr "A referência da ferramenta de caminhos do Krita."

#: ../../reference_manual/tools/path.rst:11
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/path.rst:11
msgid "Vector"
msgstr "Vector"

#: ../../reference_manual/tools/path.rst:11
msgid "Path"
msgstr "Caminho"

#: ../../reference_manual/tools/path.rst:11
msgid "Bezier Curve"
msgstr "Curva Bézier"

#: ../../reference_manual/tools/path.rst:11
msgid "Pen"
msgstr "Caneta"

#: ../../reference_manual/tools/path.rst:17
msgid "Bezier Curve Tool"
msgstr "Ferramenta da Curva Bézier"

#: ../../reference_manual/tools/path.rst:19
msgid "|toolbeziercurve|"
msgstr "|toolbeziercurve|"

#: ../../reference_manual/tools/path.rst:21
msgid ""
"You can draw curves by using this tool. Click the |mouseleft| to indicate "
"the starting point of the curve, then click again for consecutive control "
"points of the curve."
msgstr ""
"Poderá desenhar curvas se usar esta ferramenta. Carregue no |mouseleft| para "
"indicar o ponto inicial da curva, carregando depois para os pontos de "
"controlo consecutivos da curva."

#: ../../reference_manual/tools/path.rst:23
msgid ""
":program:`Krita` will show a blue line with two handles when you add a "
"control point. You can drag these handles to change the direction of the "
"curve in that point."
msgstr ""
"O :program:`Krita` irá mostrar uma linha azul com duas pegas quando "
"adicionar um ponto de controlo. Poderá arrastar estas pegas para mudar a "
"direcção da curva nesse ponto."

#: ../../reference_manual/tools/path.rst:25
msgid ""
"On a vector layer, you can click on a previously inserted control point to "
"modify it. With an intermediate control point (i.e. a point that is not the "
"starting point and not the ending point), you can move the direction handles "
"separately to have the curve enter and leave the point in different "
"directions. After editing a point, you can just click on the canvas to "
"continue adding points to the curve."
msgstr ""
"Numa camada vectorial, poderá carregar sobre um ponto de controlo "
"previamente introduzido para o modificar. Com um ponto de controlo "
"intermédio (i.e., um ponto que não é nem o inicial nem o final), poderá "
"mover as pegas de direcção em separado para ter a curva a entrar e a sair do "
"ponto em diferentes direcções. Depois de editar um ponto, poderá "
"simplesmente carregar sobre a área de desenho para continuar a adicionar "
"pontos à curva."

#: ../../reference_manual/tools/path.rst:27
msgid ""
"Pressing the :kbd:`Del` key will remove the currently selected control point "
"from the curve. Double-click the |mouseleft| on any point of the curve or "
"press the :kbd:`Enter` key to finish drawing, or press the :kbd:`Esc` key to "
"cancel the entire curve. You can use the :kbd:`Ctrl` key while keeping the |"
"mouseleft| pressed to move the entire curve to a different position."
msgstr ""
"Se carregar em :kbd:`Del`, irá remover o ponto de controlo seleccionado de "
"momento da curva. Faça duplo-click sobre o |mouseleft| em qualquer ponto da "
"curva ou carregue em :kbd:`Enter` para terminar o desenho, ou ainda em :kbd:"
"`Esc` para cancelar a curva por inteiro. Poderá usar o :kbd:`Ctrl` enquanto "
"mantém o |mouseleft| carregado para mover a curva inteira para uma posição "
"diferente."

#: ../../reference_manual/tools/path.rst:29
msgid ""
"While drawing the :kbd:`Ctrl` key while dragging will push the handles both "
"ways. The :kbd:`Alt` key will create a sharp corner, and the :kbd:`Shift` "
"key will allow you to make a handle while at the end of the curve. |"
"mouseright| will undo the last added point."
msgstr ""
"Ao desenhar, se carregar no :kbd:`Ctrl` enquanto arrasta, irá empurrar as "
"pegas de ambas as formas. Por outro lado, o :kbd:`Alt` irá criar um canto "
"vincado, enquanto o :kbd:`shift` lhe permitirá criar uma pega quando estiver "
"no fim da curva. O |mouseright| irá anular o último ponto adicionado."

#: ../../reference_manual/tools/path.rst:32
msgid "Tool Options"
msgstr "Opções da Ferramenta"

#: ../../reference_manual/tools/path.rst:36
msgid "Autosmooth Curve"
msgstr "Auto-Suavizar a Curva"

#: ../../reference_manual/tools/path.rst:37
msgid ""
"Toggling this will have nodes initialize with smooth curves instead of "
"angles. Untoggle this if you want to create sharp angles for a node. This "
"will not affect curve sharpness from dragging after clicking."
msgstr ""
"Se activar isto, fará com que os nós sejam inicializados com curvas "
"suavizadas em vez de ângulos. Desligue isto se quiser criar ângulos vincados "
"para um nó. Isto não irá afectar a nitidez da curva do arrastamento após o "
"pressionar do botão."

#: ../../reference_manual/tools/path.rst:39
msgid "Angle Snapping Delta"
msgstr "Delta de Ajuste do Ângulo"

#: ../../reference_manual/tools/path.rst:40
msgid "The angle to snap to."
msgstr "O ângulo ao qual se ajustar."

#: ../../reference_manual/tools/path.rst:42
msgid ""
"Angle snap will make it easier to have the next line be at a specific angle "
"of the current. The angle is determined by the :guilabel:`Angle Snapping "
"Delta`."
msgstr ""
"O ajuste do ângulo irá facilitar o processo de ter a próxima linha com um "
"ângulo específico face à actual. O ângulo é definido pelo :guilabel:`Delta "
"de Ajuste do Ângulo`."
