msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-05 03:40+0200\n"
"PO-Revision-Date: 2019-05-08 17:34+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: images image en KritaOverviewDocker dockers alt\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Kritamouseright mouseright icons\n"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: botão direito"

#: ../../reference_manual/dockers/overview.rst:1
msgid "Overview of the overview docker."
msgstr "Introdução à área acoplável de visão geral."

#: ../../reference_manual/dockers/overview.rst:11
#: ../../reference_manual/dockers/overview.rst:16
msgid "Overview"
msgstr "Introdução"

#: ../../reference_manual/dockers/overview.rst:11
msgid "Navigation"
msgstr "Navegação"

#: ../../reference_manual/dockers/overview.rst:19
msgid ".. image:: images/dockers/Krita_Overview_Docker.png"
msgstr ".. image:: images/dockers/Krita_Overview_Docker.png"

#: ../../reference_manual/dockers/overview.rst:20
msgid ""
"This docker allows you to see a full overview of your image. You can also "
"use it to navigate and zoom in and out quickly. Dragging the view-rectangle "
"allows you quickly move the view."
msgstr ""
"Esta área acoplável permite-lhe ter uma visão geral sobre toda a imagem. "
"Também a poderá usar para navegar e ampliar ou reduzir de forma rápida. Se "
"arrastar o rectângulo de visualização, poderá mover rapidamente essa área."

#: ../../reference_manual/dockers/overview.rst:22
msgid ""
"There are furthermore basic navigation functions: Dragging the zoom-slider "
"allows you quickly change the zoom."
msgstr ""
"Existem mais algumas funções básicas de navegação: Se arrastar a barra "
"deslizante de ampliação, permite-lhe mudar rapidamente o nível de ampliação."

#: ../../reference_manual/dockers/overview.rst:26
msgid ""
"Toggling the mirror button will allow you to mirror the view of the canvas "
"(but not the full image itself) and dragging the rotate slider allows you to "
"adjust the rotation of the viewport. To reset the rotation, |mouseright| the "
"slider to edit the number, and type '0'."
msgstr ""
"Se comutar o botão de espelho, permitirá criar um espelho da área de desenho "
"(mas não da imagem completa em si) e se arrastar a barra de rotação permitir-"
"lhe-á ajustar a rotação da área de visualização. Para repor a rotação, use o "
"|mouseright| sobre a barra para editar o número e escreva '0'."
