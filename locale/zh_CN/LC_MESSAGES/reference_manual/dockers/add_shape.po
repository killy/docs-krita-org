msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___dockers___add_shape.pot\n"

#: ../../reference_manual/dockers/add_shape.rst:1
msgid "The add shape docker."
msgstr "介绍 Krita 的添加形状工具面板。"

#: ../../reference_manual/dockers/add_shape.rst:15
msgid "Add Shape"
msgstr "添加形状"

#: ../../reference_manual/dockers/add_shape.rst:18
msgid ".. image:: images/dockers/Krita_Add_Shape_Docker.png"
msgstr ""

#: ../../reference_manual/dockers/add_shape.rst:19
msgid "A docker for adding KOffice shapes to a Vector Layers."
msgstr "一个用于在矢量图层中添加 KOffice 形状的工具面板。"

#: ../../reference_manual/dockers/add_shape.rst:23
msgid "This got removed in 4.0, the :ref:`vector_library_docker` replacing it."
msgstr ""
"此面板在 4.0 版后已被移除，由 :ref:`vector_library_docker` 替代其原有功能。"
