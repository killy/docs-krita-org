msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___audio_for_animation.pot\n"

#: ../../reference_manual/audio_for_animation.rst:1
msgid "The audio playback with animation in Krita."
msgstr "介绍如何在 Krita 里面进行动画的音频回放。"

#: ../../reference_manual/audio_for_animation.rst:12
msgid "Animation"
msgstr ""

#: ../../reference_manual/audio_for_animation.rst:12
msgid "Audio"
msgstr ""

#: ../../reference_manual/audio_for_animation.rst:12
msgid "Sound"
msgstr ""

#: ../../reference_manual/audio_for_animation.rst:12
msgid "Timeline"
msgstr ""

#: ../../reference_manual/audio_for_animation.rst:17
msgid "Audio for Animation"
msgstr "动画的音频功能"

#: ../../reference_manual/audio_for_animation.rst:21
msgid ""
"Audio for animation is an unfinished feature. It has multiple bugs and may "
"not work on your system."
msgstr ""
"动画音频是一个尚未完成的功能。它仍有不少程序问题，可能无法在你的系统上正常工"
"作。"

#: ../../reference_manual/audio_for_animation.rst:23
msgid ""
"You can add audio files to your animation to help sync lips or music. This "
"functionality is available in the timeline docker."
msgstr ""
"你可以把音频文件添加到动画中，辅助进行对口型和音画同步等工作。此功能可以在时"
"间线面板中进行控制。"

#: ../../reference_manual/audio_for_animation.rst:26
msgid "Importing Audio Files"
msgstr "导入音频文件"

#: ../../reference_manual/audio_for_animation.rst:28
msgid ""
"Krita supports MP3, OGM, and WAV audio files. When you open up your timeline "
"docker, there will be a speaker button in the top left area."
msgstr ""
"Krita 支持 MP3、OGM 和 WAV 音频文件。要使用它们，打开时间线面板，在左上角可以"
"找到一个扬声器按钮。"

#: ../../reference_manual/audio_for_animation.rst:30
msgid ""
"If you press the speaker button, you will get the available audio options "
"for the animation."
msgstr "按下该扬声器按钮，将弹出可供动画使用的音频选项。"

#: ../../reference_manual/audio_for_animation.rst:32
msgid "Open"
msgstr "打开"

#: ../../reference_manual/audio_for_animation.rst:33
msgid "Mute"
msgstr "静音"

#: ../../reference_manual/audio_for_animation.rst:34
msgid "Remove audio"
msgstr "移除音频"

#: ../../reference_manual/audio_for_animation.rst:35
msgid "Volume slider"
msgstr "音量滑动条"

#: ../../reference_manual/audio_for_animation.rst:37
msgid ""
"Krita saves the location of your audio file. If you move the audio file or "
"rename it, Krita will not be able to find it. Krita will tell you the file "
"was moved or deleted the next time you try to open the Krita file up."
msgstr ""
"Krita 会在文档中保存音频文件的所在位置。如果音频文件被移动或重命名，Krita 将"
"无法找到它。在下次打开该 Krita 文件时，Krita 会提示音频文件已被移动或者删除。"

#: ../../reference_manual/audio_for_animation.rst:40
msgid "Using Audio"
msgstr "使用音频"

#: ../../reference_manual/audio_for_animation.rst:42
msgid ""
"After you import the audio, you can scrub through the timeline and it will "
"play the audio chunk at the time spot. When you press the Play button, the "
"entire the audio file will playback as it will in the final version. There "
"is no visual display of the audio file on the screen, so you will need to "
"use your ears and the scrubbing functionality to position frames."
msgstr ""
"导入音频之后，你可以通过拖动时间轴来播放在对应时间点的音频。按下“播放”按钮"
"时，整个音频文件将如同在动画的最终输出版本一样进行播放。音频信息不会在屏幕上"
"显示，所以你只能一边拖动时间线一边监听来定位对应的动画帧。"

#: ../../reference_manual/audio_for_animation.rst:46
msgid "Exporting with Audio"
msgstr "导出带音频的动画"

#: ../../reference_manual/audio_for_animation.rst:48
msgid ""
"To get the audio file included when you are exporting, you need to include "
"it in the Render Animation options. In the :menuselection:`File --> Render "
"Animation` options there is a checkbox :guilabel:`Include Audio`. Make sure "
"that is checked before you export and you should be good to go."
msgstr ""
"要在导出动画时包含音频文件，必须在导出前勾选相关选项。打开菜单栏的 :"
"menuselection:`文件-> 渲染动画` 对话框，找到 :guilabel:`包含音频` 复选框，确"
"保在导出之前勾选了此选项。"

#: ../../reference_manual/audio_for_animation.rst:51
msgid "Packages needed for Audio on Linux"
msgstr "Linux 环境下音频功能所需软件包"

#: ../../reference_manual/audio_for_animation.rst:53
msgid ""
"The following packages are necessary for having the audio support on Linux:"
msgstr "在 Linux 环境下面，实现 Krita 的音频功能支持需要安装下列软件包："

#: ../../reference_manual/audio_for_animation.rst:56
msgid "For people who build Krita on Linux:"
msgstr "用于编译 Krita："

#: ../../reference_manual/audio_for_animation.rst:58
msgid "libasound2-dev"
msgstr "libasound2-dev"

#: ../../reference_manual/audio_for_animation.rst:59
msgid "libgstreamer1.0-dev gstreamer1.0-pulseaudio"
msgstr "libgstreamer1.0-dev gstreamer1.0-pulseaudio"

#: ../../reference_manual/audio_for_animation.rst:60
msgid "libgstreamer-plugins-base1.0-dev"
msgstr "libgstreamer-plugins-base1.0-dev"

#: ../../reference_manual/audio_for_animation.rst:61
msgid "libgstreamer-plugins-good1.0-dev"
msgstr "libgstreamer-plugins-good1.0-dev"

#: ../../reference_manual/audio_for_animation.rst:62
msgid "libgstreamer-plugins-bad1.0-dev"
msgstr "libgstreamer-plugins-bad1.0-dev"

#: ../../reference_manual/audio_for_animation.rst:64
msgid "For people who use Krita on Linux:"
msgstr "用于运行 Krita："

#: ../../reference_manual/audio_for_animation.rst:66
msgid "libqt5multimedia5-plugins"
msgstr "libqt5multimedia5-plugins"

#: ../../reference_manual/audio_for_animation.rst:67
msgid "libgstreamer-plugins-base1.0"
msgstr "libgstreamer-plugins-base1.0"

#: ../../reference_manual/audio_for_animation.rst:68
msgid "libgstreamer-plugins-good1.0"
msgstr "libgstreamer-plugins-good1.0"

#: ../../reference_manual/audio_for_animation.rst:69
msgid "libgstreamer-plugins-bad1.0"
msgstr "libgstreamer-plugins-bad1.0"
