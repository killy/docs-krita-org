msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_general_concepts___colors___color_space_size.pot\n"

#: ../../general_concepts/colors/color_space_size.rst:None
msgid ""
".. image:: images/color_category/Basiccolormanagement_compare4spaces.png"
msgstr ""

#: ../../general_concepts/colors/color_space_size.rst:1
msgid "About Color Space Size"
msgstr "关于色域"

#: ../../general_concepts/colors/color_space_size.rst:10
msgid "Color"
msgstr ""

#: ../../general_concepts/colors/color_space_size.rst:10
msgid "Color Spaces"
msgstr ""

#: ../../general_concepts/colors/color_space_size.rst:15
msgid "Color Space Size"
msgstr "色域"

#: ../../general_concepts/colors/color_space_size.rst:17
msgid ""
"Using Krita's color space browser, you can see that there are many different "
"space sizes."
msgstr ""
"在 Krita 的色彩空间浏览器里，你会发现各个空间的体积存在差异。色彩空间的体积也"
"叫做色域。"

#: ../../general_concepts/colors/color_space_size.rst:25
msgid "How do these affect your image, and why would you use them?"
msgstr "不同的色域对图像有什么影响，又应如何选用？"

#: ../../general_concepts/colors/color_space_size.rst:27
msgid "There are three primary reasons to use a large space:"
msgstr "使用较大色域理由有三种："

#: ../../general_concepts/colors/color_space_size.rst:29
msgid ""
"Even though you can't see the colors, the computer program does understand "
"them and can do color maths with it."
msgstr ""
"尽管你看不见部分颜色，但计算机程序依然可以理解并使用这些颜色进行色彩运算。"

#: ../../general_concepts/colors/color_space_size.rst:30
msgid ""
"For exchanging between programs and devices: most CMYK profiles are a little "
"bigger than our default sRGB in places, while in other places, they are "
"smaller. To get the best conversion, having your image in a space that "
"encompasses both your screen profile as your printer profile."
msgstr ""
"用于在程序和设备之间交换图像：大多数 CMYK 特性文件在某些位置大于 Krita 默认"
"的 sRGB 空间，却又在某些位置小于该空间。为了最佳化色彩转换效果，图像的色彩空"
"间应该同时涵盖你的显示器和打印机的特性文件空间。"

#: ../../general_concepts/colors/color_space_size.rst:31
msgid ""
"For archival purposes. In other words, maybe monitors of the future will "
"have larger amounts of colors they can show (spoiler: they already do), and "
"this allows you to be prepared for that."
msgstr ""
"用于保存档案。因为未来的显示器应该可以显示更丰富的色彩 (剧透：有些显示器已经"
"可以做到)，使用更大的色彩空间可以让你未雨绸缪。"

#: ../../general_concepts/colors/color_space_size.rst:33
msgid "Let's compare the following gradients in different spaces:"
msgstr "让我们对比下面不同色彩空间中的渐变色："

#: ../../general_concepts/colors/color_space_size.rst:37
msgid ""
".. image:: images/color_category/Basiccolormanagement_gradientsin4spaces_v2."
"jpg"
msgstr ""

#: ../../general_concepts/colors/color_space_size.rst:40
msgid ""
".. image:: images/color_category/"
"Basiccolormanagement_gradientsin4spaces_nonmanaged.png"
msgstr ""

#: ../../general_concepts/colors/color_space_size.rst:41
msgid ""
"On the left we have an artifact-ridden color managed jpeg file with an ACES "
"sRGBtrc v2 profile attached (or not, if not, then you can see the exact "
"different between the colors more clearly). This should give an "
"approximation of the actual colors. On the right, we have an sRGB png that "
"was converted in Krita from the base file."
msgstr ""
"左边的是一张伪像较多的 JPEG 图像，它附带了一个 ACES sRGBtrc v2 特性文件 (如果"
"它没有附带这个特性文件，那么各组渐变色的差异将会显示得更明显)。右边的则是使"
"用 Krita 从源文件直接转换得到的一张 sRGB PNG 图像。"

#: ../../general_concepts/colors/color_space_size.rst:43
msgid ""
"Each of the gradients is the gradient from the max of a given channel. As "
"you can see, the mid-tone of the ACES color space is much brighter than the "
"mid-tone of the RGB colorspace, and this is because the primaries are "
"further apart."
msgstr ""
"每组渐变色都是从一个颜色通道的最大值渐变为另一个通道的最大值。你会发现 ACES "
"色彩空间的中间调远比 RGB 色彩空间中的要明亮，这是因为在这个空间里原色之间的距"
"离更远。"

#: ../../general_concepts/colors/color_space_size.rst:45
msgid ""
"What this means for us is that when we start mixing or applying filters, "
"Krita can output values higher than visible, but also generate more correct "
"mixes and gradients. In particular, when color correcting, the bigger space "
"can help with giving more precise information."
msgstr ""
"上述例子想要说明的是，Krita 可以在混色和应用滤镜时输出大于可见范围的颜色数"
"值。如果工作空间的色域更大，混色的结果和生成的渐变也会更加准确。尤其是在进行"
"色彩修正工作时，更大的色域有利于得到更精确的颜色信息。"

#: ../../general_concepts/colors/color_space_size.rst:47
msgid ""
"If you have a display profile that uses a LUT, then you can use perceptual "
"to give an indication of how your image will look."
msgstr ""
"如果你有一个使用 LUT 的显示器特性文件，你可以使用可感知模式来模拟图像在目标空"
"间的观察效果。"

#: ../../general_concepts/colors/color_space_size.rst:49
msgid ""
"Bigger spaces do have the downside they require more precision if you do not "
"want to see banding, so make sure to have at the least 16bit per channel "
"when choosing a bigger space."
msgstr ""
"更大的色域也有缺点，它们需要更高的色彩精度来避免发生颜色条纹。因此在使用更大"
"的空间时记得选用 16 位深度以上的色彩通道。"
