# Translation of docs_krita_org_user_manual___animation.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
# Josep Ma. Ferrer <txemaq@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: user_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-24 17:11+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

# skip-rule: t-sp_2p
#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: clic esquerre del ratolí"

# skip-rule: t-sp_2p
#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: clic dret del ratolí"

#: ../../user_manual/animation.rst:1
msgid "Detailed guide on the animation workflow in Krita."
msgstr "Guia detallada sobre el flux de treball amb l'animació en el Krita."

#: ../../user_manual/animation.rst:13
msgid "Animation"
msgstr "Animació"

#: ../../user_manual/animation.rst:18
msgid "Animation with Krita"
msgstr "Animació amb el Krita"

# skip-rule: t-sp_2p,t-2p_sp,t-acc_obe
#: ../../user_manual/animation.rst:20
msgid ""
"Thanks to the 2015 Kickstarter, :program:`Krita 3.0` now has animation. In "
"specific, :program:`Krita` has frame-by-frame raster animation. There's "
"still a lot of elements missing from it, like tweening, but the basic "
"workflow is there."
msgstr ""
"Gràcies al Kickstarter de 2015, el :program:`Krita 3.0` ara té animació. En "
"específic, el :program:`Krita` té animació rasteritzada fotograma per "
"fotograma. Encara falten molts elements, com la interpolació, però el flux "
"de treball bàsic hi és."

#: ../../user_manual/animation.rst:25
msgid ""
"To access the animation features, the easiest way is to change your "
"workspace to Animation. This will make the animation dockers and workflow "
"appear."
msgstr ""
"Per accedir a les característiques de l'animació, la forma més senzilla és "
"canviar el vostre espai de treball a Animació. Això farà que apareguin els "
"acobladors Animació i Flux de treball."

#: ../../user_manual/animation.rst:30
msgid "Animation curves"
msgstr "Corbes d'animació"

# skip-rule: t-sp_2p,t-2p_sp,t-acc_obe
#: ../../user_manual/animation.rst:32
msgid ""
"To create an animation curve (currently only for opacity) expand the :"
"guilabel:`New Frame` button in the :guilabel:`Animation` dock and click :"
"guilabel:`Add Opacity Keyframe`. You can now edit the keyframed value for "
"opacity directly in the “Layers” dock, adding more keyframes will by default "
"fade from the last to the next upcoming keyframe in the timeline over the "
"frames between them. See :ref:`animation curves <animation_curves_docker>` "
"for details."
msgstr ""
"Per a crear una corba d'animació (actualment només per opacitat) "
"s'expandeixi el botó :guilabel:`Fotograma nou` a l'acoblador :guilabel:"
"`Animació` i feu clic a :guilabel:`Afegeix un fotograma clau d'opacitat`. "
"Ara podreu editar el valor del fotograma clau directament per a l'opacitat a "
"l'acoblador «Capes», afegint-hi més fotogrames clau, s'esvairan de manera "
"predeterminada des de l'últim fotograma clau al següent més proper seguint "
"en la línia de temps sobre els fotogrames entre ells. Per a més detalls, "
"vegeu :ref:`corbes d'animació <animation_curves_docker>`."

#: ../../user_manual/animation.rst:40
msgid "Workflow"
msgstr "Flux de treball"

#: ../../user_manual/animation.rst:42
msgid ""
"In traditional animation workflow, what you do is that you make *key "
"frames*, which contain the important poses, and then draw frames in between "
"(\\ *tweening* in highly sophisticated animator's jargon)."
msgstr ""
"En el flux de treball tradicional de l'animació, el que fa és crear "
"*fotogrames clau*, els quals contenen les postures importants, i després "
"dibuixar fotogrames entre ells (\\ *interpolació* en l'argot altament "
"sofisticat dels animadors)."

#: ../../user_manual/animation.rst:46
msgid "For this workflow, there are three important dockers:"
msgstr "Per a aquest flux de treball, hi ha tres acobladors importants:"

# skip-rule: t-sp_2p,t-2p_sp,t-acc_obe
#: ../../user_manual/animation.rst:48
msgid ""
"The :ref:`timeline_docker`. View and control all of the frames in your "
"animation. The timeline docker also contains functions to manage your "
"layers. The layer that are created in the timeline docker also appear on the "
"normal Layer docker."
msgstr ""
"El :ref:`timeline_docker`. Veureu i controlareu tots els fotogrames de la "
"vostra animació. L'acoblador Línia de temps també conté característiques per "
"a gestionar les capes. La capa creada en l'acoblador Línia de temps també "
"apareixerà a l'acoblador Capes normal."

# skip-rule: t-sp_2p,t-2p_sp,t-acc_obe
#: ../../user_manual/animation.rst:52
msgid ""
"The :ref:`animation_docker`. This docker contains the play buttons as the "
"ability to change the frame-rate, playback speed and useful little options "
"like :guilabel:`auto-key framing`."
msgstr ""
"El :ref:`animation_docker`. Aquest acoblador conté els botons de reproducció "
"amb la possibilitat de canviar la velocitat dels fotogrames, velocitat de "
"reproducció i petites opcions d'utilitat, com ara :guilabel:`enquadrament de "
"fotogrames clau`."

# skip-rule: t-sp_2p,t-2p_sp,t-acc_obe
#: ../../user_manual/animation.rst:55
msgid ""
"The :ref:`onion_skin_docker`. This docker controls the look of the onion "
"skin, which in turn is useful for seeing the previous frame."
msgstr ""
"El :ref:`onion_skin_docker`. Aquest acoblador controla l'aspecte de la pell "
"de ceba, que al seu torn és útil per a veure el fotograma anterior."

# Nota: https://en.wikipedia.org/wiki/Walk_cycle
#: ../../user_manual/animation.rst:60
msgid "Introduction to animation: How to make a walkcycle"
msgstr "Introducció a l'animació: Com fer un cicle de caminar"

#: ../../user_manual/animation.rst:62
msgid ""
"The best way to get to understand all these different parts is to actually "
"use them. Walk cycles are considered the most basic form of a full "
"animation, because of all the different parts involved with them. Therefore, "
"going over how one makes a walkcycle should serve as a good introduction."
msgstr ""
"La millor manera d'entendre totes aquestes diferents parts és utilitzar-les. "
"Els cicles de caminar es consideren la forma més bàsica d'una animació "
"completa, a causa de les diferents parts involucrades en elles. Per tant, "
"repassar com es crea un cicle de caminar hauria de servir com una bona "
"introducció."

#: ../../user_manual/animation.rst:69
msgid "Setup"
msgstr "Disposició"

#: ../../user_manual/animation.rst:71
msgid "First, we make a new file:"
msgstr "Primer, crearem un fitxer nou:"

#: ../../user_manual/animation.rst:74
msgid ".. image:: images/animation/Introduction_to_animation_01.png"
msgstr ".. image:: images/animation/Introduction_to_animation_01.png"

# skip-rule: t-apo_ini
#: ../../user_manual/animation.rst:75
msgid ""
"On the first tab, we type in a nice ratio like 1280x1024, set the dpi to 72 "
"(we're making this for screens after all) and title the document 'walkcycle'."
msgstr ""
"A la primera pestanya, escriurem una bona relació com 1280x1024, establint "
"els ppp a 72 (després de tot, això ho fem per a les pantalles) i el títol "
"del document «cicle de caminar»."

# skip-rule: t-sp_2p,t-2p_sp,t-acc_obe
#: ../../user_manual/animation.rst:79
msgid ""
"In the second tab, we choose a nice background color, and set the background "
"to canvas-color. This means that Krita will automatically fill in any "
"transparent bits with the background color. You can change this in :"
"menuselection:`Image --> Image Properties`. This seems to be most useful to "
"people doing animation, as the layer you do animation on MUST be semi-"
"transparent to get onion skinning working."
msgstr ""
"A la segona pestanya, triem un bon color de fons i establim el fons al color "
"del llenç. Això vol dir que el Krita emplenarà automàticament tots els bits "
"transparents amb el color de fons. Ho podeu canviar a :menuselection:`Imatge "
"--> Propietats de la imatge`. Això sembla el més útil per a les persones que "
"fan animacions, ja que la capa que fa l'animació HA de ser semitransparent "
"per aconseguir que funcioni la pell de ceba."

# skip-rule: t-sp_2p,t-2p_sp,t-acc_obe
#: ../../user_manual/animation.rst:82
msgid ""
"Krita has a bunch of functionality for meta-data, starting at the :guilabel:"
"`Create Document` screen. The title will be automatically used as a "
"suggestion for saving and the description can be used by databases, or for "
"you to leave comments behind. Not many people use it individually, but it "
"can be useful for working in larger groups."
msgstr ""
"El Krita té un munt de funcionalitats per a les metadades, començant a la "
"pantalla :guilabel:`Crea un document`. El títol serà emprat automàticament "
"com un suggeriment per al desament i la descripció pot ser utilitzada per "
"les bases de dades, o perquè deixeu comentaris. Individualment, no el fa "
"servir gaire gent, però pot ser útil per a treballar en grups grans."

#: ../../user_manual/animation.rst:84
msgid "Then hit :guilabel:`Create`!"
msgstr "Després premeu :guilabel:`Crea`!"

#: ../../user_manual/animation.rst:86
msgid ""
"Then, to get all the necessary tools for animation, select the workspace "
"switcher:"
msgstr ""
"Després, per obtenir totes les eines necessàries per a l'animació, "
"seleccioneu el commutador de l'espai de treball:"

#: ../../user_manual/animation.rst:91
msgid ".. image:: images/animation/Introduction_to_animation_02.png"
msgstr ".. image:: images/animation/Introduction_to_animation_02.png"

#: ../../user_manual/animation.rst:91
msgid "The red arrow points at the workspace switcher."
msgstr "La fletxa vermella apunta al commutador de l'espai de treball."

#: ../../user_manual/animation.rst:93
msgid "And select the animation workspace."
msgstr "I seleccioneu l'espai de treball de l'animació."

#: ../../user_manual/animation.rst:95
msgid "Which should result in this:"
msgstr "El que hauria de resultar en això:"

#: ../../user_manual/animation.rst:98
msgid ".. image:: images/animation/Introduction_to_animation_03.png"
msgstr ".. image:: images/animation/Introduction_to_animation_03.png"

#: ../../user_manual/animation.rst:99
msgid ""
"The animation workspace adds the timeline, animation and onion skin dockers "
"at the bottom."
msgstr ""
"L'espai de treball de l'animació afegeix els acobladors Línia de temps, "
"Animació i Pell de ceba a la part inferior."

#: ../../user_manual/animation.rst:103
msgid "Animating"
msgstr "Animant"

# skip-rule: t-apo_ini,t-apo_fin
#: ../../user_manual/animation.rst:105
msgid ""
"We have two transparent layers set up. Let's name the bottom one "
"'environment' and the top 'walkcycle' by double clicking their names in the "
"layer docker."
msgstr ""
"Tenim configurades dues capes transparents. Anomenem el primer «entorn» i la "
"part superior «Cicle de caminar» fent doble clic en els seus noms a "
"l'acoblador Capes."

#: ../../user_manual/animation.rst:110
msgid ".. image:: images/animation/Introduction_to_animation_04.png"
msgstr ".. image:: images/animation/Introduction_to_animation_04.png"

#: ../../user_manual/animation.rst:111
msgid ""
"Use the straight line tool to draw a single horizontal line. This is the "
"ground."
msgstr ""
"Empreu l'eina de línia recta per a dibuixar una sola línia horitzontal. "
"Aquest serà el terra."

#: ../../user_manual/animation.rst:115
msgid ".. image:: images/animation/Introduction_to_animation_05.png"
msgstr ".. image:: images/animation/Introduction_to_animation_05.png"

# skip-rule: t-apo_ini,t-apo_fin
#: ../../user_manual/animation.rst:116
msgid ""
"Then, select the 'walkcycle' layer and draw a head and torso (you can use "
"any brush for this)."
msgstr ""
"Després, seleccioneu la capa «Cicle de caminar» i dibuixeu un cap i un tors "
"(per això, podeu utilitzar qualsevol pinzell)."

# skip-rule: t-apo_ini,t-apo_fin
#: ../../user_manual/animation.rst:118
msgid ""
"Now, selecting a new frame will not make a new frame automatically. Krita "
"doesn't actually see the 'walkcycle' layer as an animated layer at all!"
msgstr ""
"Ara, seleccionant un fotograma nou no es crearà un fotograma nou "
"automàticament. El Krita, en realitat no veu la capa «Cicle de caminar» com "
"una capa animada en absolut!"

#: ../../user_manual/animation.rst:123
msgid ".. image:: images/animation/Introduction_to_animation_06.png"
msgstr ".. image:: images/animation/Introduction_to_animation_06.png"

#: ../../user_manual/animation.rst:124
msgid ""
"We can make it animatable by adding a frame to the timeline. |mouseright| a "
"frame in the timeline to get a context menu. Choose :guilabel:`New Frame`."
msgstr ""
"Podem fer-ho animable afegint un fotograma a la línia de temps. Feu |"
"mouseright| sobre un fotograma a la línia de temps per obtenir un menú "
"contextual. Trieu :guilabel:`Fotograma nou`."

#: ../../user_manual/animation.rst:128
msgid ".. image:: images/animation/Introduction_to_animation_07.png"
msgstr ".. image:: images/animation/Introduction_to_animation_07.png"

#: ../../user_manual/animation.rst:129
msgid ""
"You can see it has become an animated layer because of the onion skin icon "
"showing up in the timeline docker."
msgstr ""
"Podeu veure que s'ha convertit en una capa animada pel fet que la icona de "
"la pell de ceba apareix a l'acoblador Línia de temps."

#: ../../user_manual/animation.rst:133
msgid ".. image:: images/animation/Introduction_to_animation_08.png"
msgstr ".. image:: images/animation/Introduction_to_animation_08.png"

# skip-rule: t-sp_2p,t-2p_sp,t-acc_obe
#: ../../user_manual/animation.rst:134
msgid ""
"Use the :guilabel:`Copy Frame` button to copy the first frame onto the "
"second. Then, use the with the :kbd:`Shift + ↑` shortcut to move the frame "
"contents up."
msgstr ""
"Utilitzeu el botó :guilabel:`Copia el fotograma` per a copiar el primer "
"fotograma al segon. Després, utilitzeu la drecera :kbd:`Majús. + ↑` per a "
"moure el contingut del fotograma cap amunt."

#: ../../user_manual/animation.rst:137
msgid "We can see the difference by turning on the onionskinning:"
msgstr "Podem veure la diferència activant la pell de ceba:"

#: ../../user_manual/animation.rst:140
msgid ".. image:: images/animation/Introduction_to_animation_09.png"
msgstr ".. image:: images/animation/Introduction_to_animation_09.png"

#: ../../user_manual/animation.rst:141
msgid "Now, you should see the previous frame as red."
msgstr "Ara, hauríeu de veure el fotograma anterior en vermell."

# skip-rule: t-sp_2p,t-2p_sp,t-acc_obe
#: ../../user_manual/animation.rst:144
msgid ""
"Krita sees white as a color, not as transparent, so make sure the animation "
"layer you are working on is transparent in the bits where there's no "
"drawing. You can fix the situation by use the :ref:`filter_color_to_alpha` "
"filter, but prevention is best."
msgstr ""
"El Krita veu el blanc com un color, no com a transparent, així que assegureu-"
"vos que la capa de l'animació en la qual esteu treballant sigui transparent "
"en els bits on no hi ha dibuix. Podeu arreglar-ho emprant el filtre :ref:"
"`filter_color_to_alpha`, però el millor és la prevenció."

#: ../../user_manual/animation.rst:147
msgid ".. image:: images/animation/Introduction_to_animation_10.png"
msgstr ".. image:: images/animation/Introduction_to_animation_10.png"

#: ../../user_manual/animation.rst:148
msgid ""
"Future frames are drawn in green, and both colors can be configured in the "
"onion skin docker."
msgstr ""
"Els fotogrames futurs es dibuixaran en verd i ambdós colors es poden "
"configurar a l'acoblador Pell de ceba."

#: ../../user_manual/animation.rst:152
msgid ".. image:: images/animation/Introduction_to_animation_11.png"
msgstr ".. image:: images/animation/Introduction_to_animation_11.png"

#: ../../user_manual/animation.rst:153
msgid ""
"Now, we're gonna draw the two extremes of the walkcycle. These are the pose "
"where both legs are as far apart as possible, and the pose where one leg is "
"full stretched and the other pulled in, ready to take the next step."
msgstr ""
"Ara, dibuixarem els dos extrems del cicle de caminar. Aquestes seran les "
"postures en les que totes dues cames estan el més separades possible, i la "
"postura on una cama està completament estirada i l'altra arronsada, llest "
"per a fer el següent pas."

# skip-rule: t-sp_2p,t-2p_sp,t-acc_obe
#: ../../user_manual/animation.rst:158
msgid ""
"Now, let's copy these two... We could do that with the :kbd:`Ctrl + drag` "
"shortcut, but here comes a tricky bit:"
msgstr ""
"Ara, copiem aquests dos... Podríem fer-ho amb la drecera :kbd:`Ctrl + "
"arrossega`, però aquí esdevé una mica difícil:"

#: ../../user_manual/animation.rst:162
msgid ".. image:: images/animation/Introduction_to_animation_12.png"
msgstr ".. image:: images/animation/Introduction_to_animation_12.png"

# skip-rule: t-acc_obe
#: ../../user_manual/animation.rst:163
msgid ""
":kbd:`Ctrl +` |mouseleft| also selects and deselects frames, so to copy..."
msgstr ""
":kbd:`Ctrl + feu` |mouseleft| també selecciona i desselecciona els "
"fotogrames, per a copiar..."

# skip-rule: t-acc_obe
#: ../../user_manual/animation.rst:165
msgid ":kbd:`Ctrl +` |mouseleft| to select all the frames you want to select."
msgstr ""
":kbd:`Ctrl + feu` |mouseleft| per a seleccionar tots els fotogrames que "
"voleu seleccionar."

# skip-rule: t-acc_obe,t-apo_ini
#: ../../user_manual/animation.rst:166
msgid ""
":kbd:`Ctrl + drag`. You need to make sure the first frame is 'orange', "
"otherwise it won't be copied along."
msgstr ""
":kbd:`Ctrl + arrossega`. Haureu d'assegurar-vos que el primer fotograma "
"sigui «taronja», en cas contrari no es copiarà."

#: ../../user_manual/animation.rst:169
msgid "Now then..."
msgstr "Ara, ja que..."

#: ../../user_manual/animation.rst:174
msgid ".. image:: images/animation/Introduction_to_animation_13.png"
msgstr ".. image:: images/animation/Introduction_to_animation_13.png"

#: ../../user_manual/animation.rst:174
msgid "squashed the timeline docker a bit to save space"
msgstr "s'ha aixafat l'acoblador Línia de temps per estalviar espai."

#: ../../user_manual/animation.rst:176
msgid "Copy frame 0 to frame 2."
msgstr "Copia el fotograma 0 al fotograma 2."

#: ../../user_manual/animation.rst:177
msgid "Copy frame 1 to frame 3."
msgstr "Copia el fotograma 1 al fotograma 3."

#: ../../user_manual/animation.rst:178
msgid "In the animation docker, set the frame-rate to 4."
msgstr "A l'acoblador Animació, establiu la velocitat dels fotogrames a 4."

#: ../../user_manual/animation.rst:179
msgid "Select all frames in the timeline docker by dragging-selecting them."
msgstr ""
"Seleccioneu tots els fotogrames a l'acoblador Línia de temps arrossegant la "
"selecció."

#: ../../user_manual/animation.rst:180
msgid "Press play in the animation docker."
msgstr "Premeu reprodueix a l'acoblador Animació."

#: ../../user_manual/animation.rst:181
msgid "Enjoy your first animation!"
msgstr "Gaudiu de la vostra primera animació!"

#: ../../user_manual/animation.rst:184
msgid "Expanding upon your rough walkcycle"
msgstr "Ampliar en brut el vostre cicle de caminar"

#: ../../user_manual/animation.rst:187
msgid ".. image:: images/animation/Introduction_to_animation_14.png"
msgstr ".. image:: images/animation/Introduction_to_animation_14.png"

# skip-rule: t-sp_2p,t-2p_sp,t-acc_obe
#: ../../user_manual/animation.rst:188
msgid ""
"You can quickly make some space by the :kbd:`Alt + drag` shortcut on any "
"frame. This'll move that frame and all others after it in one go."
msgstr ""
"Podeu fer una mica d'espai ràpidament amb la drecera :kbd:`Alt + arrossega` "
"en qualsevol fotograma. Això mourà aquest fotograma i tots els altres "
"després d'una sola vegada."

#: ../../user_manual/animation.rst:191
msgid "Then draw inbetweens on each frame that you add."
msgstr "Després, dibuixeu entre cada fotograma que afegiu."

#: ../../user_manual/animation.rst:194
msgid ".. image:: images/animation/Introduction_to_animation_16.png"
msgstr ".. image:: images/animation/Introduction_to_animation_16.png"

#: ../../user_manual/animation.rst:195
msgid ""
"You'll find that the more frames you add, the more difficult it becomes to "
"keep track of the onion skins."
msgstr ""
"Trobareu que com més fotogrames afegiu, més difícil serà fer un seguiment de "
"les pells de ceba."

#: ../../user_manual/animation.rst:197
msgid ""
"You can modify the onion skin by using the onion skin docker, where you can "
"change how many frames are visible at once, by toggling them on the top row. "
"The bottom row is for controlling transparency, while below there you can "
"modify the colors and extremity of the coloring."
msgstr ""
"Podeu modificar la pell de ceba utilitzant l'acoblador Pell de ceba, on "
"podreu canviar la quantitat de fotogrames que es podran veure alhora, "
"alternant-los a la fila superior. La fila inferior és per a controlar la "
"transparència, mentre que a sota poden modificar els colors i l'extremitat "
"de la coloració."

#: ../../user_manual/animation.rst:203
msgid ".. image:: images/animation/Introduction_to_animation_15.png"
msgstr ".. image:: images/animation/Introduction_to_animation_15.png"

#: ../../user_manual/animation.rst:205
msgid "Animating with multiple layers"
msgstr "Animació amb múltiples capes"

#: ../../user_manual/animation.rst:207
msgid ""
"Okay, our walkcycle is missing some hands, let's add them on a separate "
"layer. So we make a new layer, and name it hands and..."
msgstr ""
"Va bé. Al nostre cicle de caminar li falten algunes mans, les hi afegirem en "
"una capa separada. Així que crearem una capa nova i l'anomenarem mans, i..."

#: ../../user_manual/animation.rst:211
msgid ".. image:: images/animation/Introduction_to_animation_17.png"
msgstr ".. image:: images/animation/Introduction_to_animation_17.png"

#: ../../user_manual/animation.rst:212
msgid ""
"Our walkcycle is gone from the timeline docker! This is a feature actually. "
"A full animation can have so many little parts that an animator might want "
"to remove the layers they're not working on from the timeline docker. So you "
"manually have to add them."
msgstr ""
"El nostre cicle de caminar ha desaparegut de l'acoblador Línia de temps! "
"Això en realitat és una característica. Una animació completa pot tenir "
"tantes parts petites que un animador podria voler eliminar les capes que "
"estan treballant des de l'acoblador Línia de temps. De manera que haureu "
"d'afegir-les manualment."

#: ../../user_manual/animation.rst:218
msgid ".. image:: images/animation/Introduction_to_animation_18.png"
msgstr ".. image:: images/animation/Introduction_to_animation_18.png"

# skip-rule: t-sp_2p,t-2p_sp,t-acc_obe
#: ../../user_manual/animation.rst:219
msgid ""
"You can show any given layer in the timeline by doing |mouseright| on the "
"layer in the layer docker, and toggling :guilabel:`Show in Timeline`."
msgstr ""
"Podeu mostrar qualsevol capa donada a la línia de temps fent |mouseright| "
"sobre la capa a l'acoblador Capes, i alternant :guilabel:`Mostra a la línia "
"de temps`."

#: ../../user_manual/animation.rst:223
msgid ".. image:: images/animation/Introduction_to_animation_19.png"
msgstr ".. image:: images/animation/Introduction_to_animation_19.png"

#: ../../user_manual/animation.rst:225
msgid "Exporting"
msgstr "Exportant"

# skip-rule: t-sp_2p,t-2p_sp,t-acc_obe
#: ../../user_manual/animation.rst:227
msgid "When you are done, select :menuselection:`File --> Render Animation`."
msgstr ""
"Quan hàgiu acabat, seleccioneu :menuselection:`Fitxer --> Renderitza una "
"animació`."

#: ../../user_manual/animation.rst:230
msgid ".. image:: images/animation/Introduction_to_animation_20.png"
msgstr ".. image:: images/animation/Introduction_to_animation_20.png"

#: ../../user_manual/animation.rst:231
msgid ""
"It's recommended to save out your file as a png, and preferably in its own "
"folder. Krita can currently only export png sequences."
msgstr ""
"Es recomana desar el fitxer com a png i, preferentment, a la seva pròpia "
"carpeta. Actualment, el Krita només exporta les seqüències a png."

#: ../../user_manual/animation.rst:235
msgid ".. image:: images/animation/Introduction_to_animation_21.png"
msgstr ".. image:: images/animation/Introduction_to_animation_21.png"

#: ../../user_manual/animation.rst:236
msgid ""
"When pressing done, you can see the status of the export in the status bar "
"below."
msgstr ""
"En prémer fet, podreu veure l'estat de l'exportació a la barra d'estat."

#: ../../user_manual/animation.rst:240
msgid ".. image:: images/animation/Introduction_to_animation_22.png"
msgstr ".. image:: images/animation/Introduction_to_animation_22.png"

#: ../../user_manual/animation.rst:241
msgid ""
"The images should be saved out as filenameXXX.png, giving their frame number."
msgstr ""
"Les imatges s'hauran de desar com a nom_fitxerXXX.png, indicant el seu "
"número de fotograma."

#: ../../user_manual/animation.rst:244
msgid ""
"Then use something like Gimp (Linux, OSX, Windows), ImageMagick (Linux, OSX, "
"Windows), or any other gif creator to make a gif out of your image sequence:"
msgstr ""
"Després empreu quelcom com el Gimp (Linux, OSX, Windows), ImageMagick "
"(Linux, OSX, Windows) o qualsevol altre creador de gif per a crear-ne un amb "
"la vostra seqüència d'imatges:"

#: ../../user_manual/animation.rst:249
msgid ".. image:: images/animation/Introduction_to_animation_walkcycle_02.gif"
msgstr ".. image:: images/animation/Introduction_to_animation_walkcycle_02.gif"

# skip-rule: t-sp_2p,t-2p_sp,t-acc_obe
#: ../../user_manual/animation.rst:250
msgid ""
"For example, you can use `VirtualDub <http://www.virtualdub.org/>`_\\ "
"(Windows) and open all the frames and then go to :menuselection:`File --> "
"Export --> GIF`."
msgstr ""
"Per exemple, podeu utilitzar el `VirtualDub <http://www.virtualdub.org/>`_\\ "
"(Windows), obrir tots els fotogrames i després anar a :menuselection:`Fitxer "
"--> Exporta --> GIF`."

#: ../../user_manual/animation.rst:254
msgid "Enjoy your walkcycle!"
msgstr "Gaudiu del vostre cicle de caminar!"

# skip-rule: t-sp_2p,t-2p_sp,t-acc_obe
#: ../../user_manual/animation.rst:258
msgid ""
"Krita 3.1 has a render animation feature. If you're using the 3.1 beta, "
"check out the :ref:`render_animation` page for more information!"
msgstr ""
"El Krita 3.1 té una característica de renderització de l'animació. Si esteu "
"utilitzant la versió 3.1 beta, reviseu la pàgina :ref:`render_animation` per "
"obtenir més informació!"

#: ../../user_manual/animation.rst:261
msgid "Importing animation frames"
msgstr "Importar fotogrames d'animació"

#: ../../user_manual/animation.rst:263
msgid "You can import animation frames in Krita 3.0."
msgstr "Al Krita 3.0 podeu importar fotogrames d'animació."

#: ../../user_manual/animation.rst:265
msgid ""
"First let us take a sprite sheet from Open Game Art. (This is the Libre "
"Pixel Cup male walkcycle)"
msgstr ""
"Primer prenguem un full de franges d'Open Game Art. (Aquest és el cicle de "
"caminar masculí de la Libre Pixel Cup)."

# skip-rule: t-sp_2p,t-2p_sp,t-acc_obe
#: ../../user_manual/animation.rst:268
msgid ""
"And we'll use :menuselection:`Image --> Split Image` to split up the sprite "
"sheet."
msgstr ""
"I l'utilitzarem :menuselection:`Imatge --> Divideix la imatge` per a dividir "
"el full de franges."

#: ../../user_manual/animation.rst:271
msgid ".. image:: images/animation/Animation_split_spritesheet.png"
msgstr ".. image:: images/animation/Animation_split_spritesheet.png"

#: ../../user_manual/animation.rst:272
msgid ""
"The slices are even, so for a sprite sheet of 9 sprites, use 8 vertical "
"slices and 0 horizontal slices. Give it a proper name and save it as png."
msgstr ""
"Les llesques són uniformes, de manera que per a un full de franges de 9 "
"franges, utilitzeu 8 llesques verticals i 0 llesques horitzontals. Doneu-li "
"un nom adequat i deseu-lo com a png."

# skip-rule: t-sp_2p,t-2p_sp,t-acc_obe
#: ../../user_manual/animation.rst:274
msgid ""
"Then, make a new canvas, and select :menuselection:`File --> Import "
"Animation Frames`. This will give you a little window. Select :guilabel:`Add "
"images`. This should get you a file browser where you can select your images."
msgstr ""
"Després, creeu un llenç nou i seleccioneu :menuselection:`Fitxer --> Importa "
"fotogrames d'animació`. Això us donarà una petita finestra. Seleccioneu :"
"guilabel:`Afegeix imatges`. Això hauria de proporcionar-vos un explorador de "
"fitxers on podreu seleccionar les vostres imatges."

#: ../../user_manual/animation.rst:277
msgid ".. image:: images/animation/Animation_import_sprites.png"
msgstr ".. image:: images/animation/Animation_import_sprites.png"

#: ../../user_manual/animation.rst:278
msgid "You can select multiple images at once."
msgstr "Podeu seleccionar múltiples imatges alhora."

#: ../../user_manual/animation.rst:281
msgid ".. image:: images/animation/Animation_set_everything.png"
msgstr ".. image:: images/animation/Animation_set_everything.png"

#: ../../user_manual/animation.rst:282
msgid ""
"The frames are currently automatically ordered. You can set the ordering "
"with the top-left two drop-down boxes."
msgstr ""
"Actualment, els fotogrames s'ordenen automàticament. Podeu establir l'ordre "
"amb les llistes desplegables que hi ha a la part superior esquerra."

#: ../../user_manual/animation.rst:285
msgid "Start"
msgstr "Inici"

#: ../../user_manual/animation.rst:286
msgid "Indicates at which point the animation should be imported."
msgstr "Indiqueu en quin punt s'ha d'importar l'animació."

#: ../../user_manual/animation.rst:288
msgid ""
"Indicates the difference between the imported animation and the document "
"frame rate. This animation is 8 frames big, and the fps of the document is "
"24 frames, so there should be a step of 3 to keep it even. As you can see, "
"the window gives feedback on how much fps the imported animation would be "
"with the currently given step."
msgstr ""
"Indica la diferència entre l'animació importada i la velocitat dels "
"fotogrames del document. Aquesta animació té 8 fotogrames grans i els fps "
"del document són de 24 fotogrames, per la qual cosa hauria d'haver un pas de "
"3 per a mantenir-se uniforme. Com podeu veure, la finestra proporciona "
"informació sobre la quantitat de fps que tindrà l'animació importada amb el "
"pas actual fet."

#: ../../user_manual/animation.rst:292
msgid "Step"
msgstr "Pas"

#: ../../user_manual/animation.rst:294
msgid ""
"Press :guilabel:`OK`, and your animation should be imported as a new layer."
msgstr ""
"Premeu :guilabel:`D'acord` i la vostra animació s'hauria d'importar com una "
"capa nova."

#: ../../user_manual/animation.rst:297
msgid ".. image:: images/animation/Animation_import_done.png"
msgstr ".. image:: images/animation/Animation_import_done.png"

#: ../../user_manual/animation.rst:299
msgid "Reference"
msgstr "Referència"

#: ../../user_manual/animation.rst:301
msgid "https://community.kde.org/Krita/Docs/AnimationGuiFeaturesList"
msgstr "https://community.kde.org/Krita/Docs/AnimationGuiFeaturesList"

# skip-rule: t-acc_obe
#: ../../user_manual/animation.rst:302
msgid ""
"`The source for the libre pixel cup male walkmediawiki cycle <https://"
"opengameart.org/content/liberated-pixel-cup-lpc-base-assets-sprites-map-"
"tiles>`_"
msgstr ""
"`La font al MediaWiki per al cicle de caminar masculí de la Libre Pixel Cup "
"<https://opengameart.org/content/liberated-pixel-cup-lpc-base-assets-sprites-"
"map-tiles>`_"
