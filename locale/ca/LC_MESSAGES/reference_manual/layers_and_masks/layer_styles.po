# Translation of docs_krita_org_reference_manual___layers_and_masks___layer_styles.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-02 17:07+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../reference_manual/layers_and_masks/layer_styles.rst:1
msgid "How to use layer styles in Krita."
msgstr "Com emprar els estils de la capa en el Krita."

#: ../../reference_manual/layers_and_masks/layer_styles.rst:12
#: ../../reference_manual/layers_and_masks/layer_styles.rst:17
msgid "Layer Styles"
msgstr "Estils de la capa"

#: ../../reference_manual/layers_and_masks/layer_styles.rst:12
msgid "Layer Effects"
msgstr "Efectes de la capa"

#: ../../reference_manual/layers_and_masks/layer_styles.rst:12
msgid "Layer FX"
msgstr "Efectes especials de la capa"

#: ../../reference_manual/layers_and_masks/layer_styles.rst:12
msgid "ASL"
msgstr "ASL"

#: ../../reference_manual/layers_and_masks/layer_styles.rst:19
msgid ""
"Layer styles are effects that are added on top of your layer. They are "
"editable and can easily be toggled on and off. To add a layer style to a "
"layer go to :menuselection:`Layer --> Layer Style`. You can also right-click "
"a layer to access the layer styles."
msgstr ""
"Els estils de la capa són efectes que s'afegeixen a sobre de la capa. Són "
"editables i es poden activar i desactivar amb facilitat. Per afegir un estil "
"de la capa a una capa, aneu a :menuselection:`Capa --> Estil de la capa`. "
"També podreu fer clic dret sobre una capa per accedir als estils de la capa."

#: ../../reference_manual/layers_and_masks/layer_styles.rst:22
msgid ""
"When you have the layer styles window up, make sure that the :guilabel:"
"`Enable Effects` item is checked."
msgstr ""
"Quan tingueu la finestra amb els estils de la capa, assegureu-vos que està "
"marcada l'opció :guilabel:`Activa els efectes`."

#: ../../reference_manual/layers_and_masks/layer_styles.rst:24
msgid ""
"There are a variety of effects and styles you can apply to a layer. When you "
"add a style, your layer docker will show an extra \"Fx\" icon. This allows "
"you to toggle the layer style effects on and off."
msgstr ""
"Hi ha una varietat d'efectes i estils que podreu aplicar a una capa. Quan "
"afegiu un estil, l'acoblador Capes mostrarà una icona «Fx» addicional. "
"Aquesta permetrà activar i desactivar els efectes d'estil de la capa."

#: ../../reference_manual/layers_and_masks/layer_styles.rst:30
msgid ""
"This feature was added to increase support for :program:`Adobe Photoshop`. "
"The features that are included mirror what that application supports."
msgstr ""
"Aquesta característica es va afegir per augmentar el suport per al :program:"
"`Photoshop d'Adobe`. Les característiques que s'inclouen reflecteixen el que "
"admet l'aplicació."
