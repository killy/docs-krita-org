# Translation of docs_krita_org_reference_manual___dockers___digital_color_mixer.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-23 19:17+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../reference_manual/dockers/digital_color_mixer.rst:1
msgid "Overview of the digital color mixer docker."
msgstr "Vista general de l'acoblador Mesclador de colors digital."

#: ../../reference_manual/dockers/digital_color_mixer.rst:11
#: ../../reference_manual/dockers/digital_color_mixer.rst:16
msgid "Digital Color Mixer"
msgstr "Mesclador de colors digital"

#: ../../reference_manual/dockers/digital_color_mixer.rst:11
msgid "Color"
msgstr "Color"

#: ../../reference_manual/dockers/digital_color_mixer.rst:11
msgid "Color Mixing"
msgstr "Mesclar el color"

#: ../../reference_manual/dockers/digital_color_mixer.rst:11
msgid "Color Selector"
msgstr "Selector de color"

#: ../../reference_manual/dockers/digital_color_mixer.rst:19
msgid ".. image:: images/dockers/Krita_Digital_Color_Mixer_Docker.png"
msgstr ".. image:: images/dockers/Krita_Digital_Color_Mixer_Docker.png"

#: ../../reference_manual/dockers/digital_color_mixer.rst:20
msgid "This docker allows you to do simple mathematical color mixing."
msgstr "Aquest acoblador permet fer senzilles mescles de color matemàtiques."

#: ../../reference_manual/dockers/digital_color_mixer.rst:22
msgid "It works as follows:"
msgstr "Funciona de la següent manera:"

#: ../../reference_manual/dockers/digital_color_mixer.rst:24
msgid "You have on the left side the current color."
msgstr "Teniu al costat esquerre el color actual."

#: ../../reference_manual/dockers/digital_color_mixer.rst:26
msgid ""
"Next to that there are six columns. Each of these columns consists of three "
"rows: The lowest row is the color that you are mixing the current color "
"with. Ticking this button allows you to set a different color using a "
"palette and the mini-color wheel. The slider above this mixing color "
"represent the proportions of the mixing color and the current color. The "
"higher the slider, the less of the mixing color will be used in mixing. "
"Finally, the result color. Clicking this will change your current color to "
"the result color."
msgstr ""
"Al costat d'això hi ha sis columnes. Cadascuna consta de tres files: la fila "
"més baixa és el color amb el que esteu mesclant el color actual. En marcar "
"aquest botó, podreu establir un color diferent emprant una paleta i la mini "
"roda de color. El control lliscant sobre aquest color mesclat representarà "
"les proporcions de color mesclat i el color actual. Com més alt estigui el "
"control lliscant, menor serà el color mesclat que s'utilitzarà en la mescla. "
"Finalment, el color resultant. En fer-hi clic canviarà el vostre color "
"actual amb el del color resultant."
