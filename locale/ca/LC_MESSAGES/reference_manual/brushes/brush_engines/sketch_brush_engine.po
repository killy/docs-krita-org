# Translation of docs_krita_org_reference_manual___brushes___brush_engines___sketch_brush_engine.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-11 14:23+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Paint Connection Line"
msgstr "Pinta la línia de connexió"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:1
msgid "The Sketch Brush Engine manual page."
msgstr "La pàgina del manual per al motor del pinzell d'esbós."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:12
#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:17
msgid "Sketch Brush Engine"
msgstr "Motor del pinzell d'esbós"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:12
msgid "Brush Engine"
msgstr "Motor de pinzell"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:12
msgid "Harmony Brush Engine"
msgstr "Motor del pinzell amb harmonia"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:21
msgid ".. image:: images/icons/sketchbrush.svg"
msgstr ".. image:: images/icons/sketchbrush.svg"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:22
msgid ""
"A line based brush engine, based on the Harmony brushes. Very messy and fun."
msgstr ""
"Un motor de pinzell basat en la línia, basat en els pinzells amb harmonia. "
"Molt desordenat i divertit."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:25
msgid "Parameters"
msgstr "Ajustaments"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:28
msgid "Has the following parameters:"
msgstr "Té els següents ajustaments:"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:30
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:31
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:32
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:33
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:34
msgid ":ref:`option_ratio`"
msgstr ":ref:`option_ratio`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:35
msgid ":ref:`option_line_width`"
msgstr ":ref:`option_line_width`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:36
msgid ":ref:`option_offset_scale`"
msgstr ":ref:`option_offset_scale`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:37
msgid ":ref:`option_sketch_density`"
msgstr ":ref:`option_sketch_density`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:38
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:39
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:44
msgid "Line Width"
msgstr "Amplada de la línia"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:46
msgid "The width of the rendered lines."
msgstr "L'amplada de les línies renderitzades."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:49
msgid ".. image:: images/brushes/Krita_2_9_brushengine_sketch_linewidth.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_sketch_linewidth.png"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:53
msgid "Offset Scale"
msgstr "Escala de desplaçament"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:55
msgid ""
"When curve lines are formed, this value roughly determines the distance from "
"the curve lines to the connection lines:"
msgstr ""
"Quan es formen línies de corba, aquest valor determinarà aproximadament la "
"distància des de les línies de corba a les línies de connexió:"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:57
msgid ""
"This is a bit misleading, because a value of 0% and a value of 100% give "
"similar outputs, as do a value of say 30% and 70%. You could think that the "
"actual value range is between 50% and 200%."
msgstr ""
"Això és una mica enganyós, perquè un valor de 0% i un valor de 100% donen "
"resultats similars, igual que un valor de diguem el 30% i el 70%. Es podria "
"pensar que l'interval real de valor està entre el 50% i el 200%."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:58
msgid ""
"0% and 100% correspond to the curve lines touching the connection lines "
"exactly."
msgstr ""
"El 0% i el 100% es corresponen a les línies de corba que toquen exactament "
"les línies de connexió."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:59
msgid ""
"Above 100%, the curve lines will go further than the connection lines, "
"forming a fuzzy effect."
msgstr ""
"Per sobre del 100%, les línies de corba aniran més enllà que les línies de "
"connexió, formant un efecte difús."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:62
msgid ".. image:: images/brushes/Krita_2.9_brushengine_sketch_offset.png"
msgstr ".. image:: images/brushes/Krita_2.9_brushengine_sketch_offset.png"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:64
msgid ".. image:: images/brushes/Krita-sketch_offset_scale2.png"
msgstr ".. image:: images/brushes/Krita-sketch_offset_scale2.png"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:68
msgid "Density"
msgstr "Densitat"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:70
msgid ""
"The density of the lines. This one is highly affected by the Brush-tip, as "
"determined by the Distance Density toggle."
msgstr ""
"La densitat de les línies. Aquesta estarà molt afectada per la punta del "
"pinzell, segons el que determini el commutador Densitat de distància."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:73
msgid ".. image:: images/brushes/Krita_2.9_brushengine_sketch_density.png"
msgstr ".. image:: images/brushes/Krita_2.9_brushengine_sketch_density.png"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:74
msgid "Use Distance Density"
msgstr "Usa la densitat de distància"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:75
msgid ""
"The further the line covered is from the center of the area of effect, the "
"less the density of the resulting curve lines."
msgstr ""
"Com més lluny estigui la línia coberta del centre de l'àrea de l'efecte, "
"menor serà la densitat de les línies de corba resultants."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:77
msgid "Magnetify"
msgstr "Magnetitza"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:77
msgid ""
"Magnetify is *on* by default. It's what causes curve lines to form between "
"two close line sections, as though the curve lines are attracted to them "
"like magnets. With Magnetify *off*, the curve line just forms on either side "
"of the current active portion of your connection line. In other words, your "
"line becomes fuzzier when another portion of the line is nearby, but the "
"lines don't connect to said previous portion."
msgstr ""
"Magnetitza estarà *activat* de manera predeterminada. Aquest farà que es "
"formin línies de corba entre dues seccions de línies tancades, com si les "
"línies de corba fossin atretes a elles amb imants. Si està *desactivada*, la "
"línia de corba es formarà a cada costat de la part activa actual de la línia "
"de connexió. En altres paraules, la vostra línia es tornarà més borrosa quan "
"una altra part de la línia estigui a prop, però les línies no es connectaran "
"amb aquesta part anterior."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:79
msgid "Random RGB"
msgstr "RGB aleatori"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:80
msgid "Causes some slight RGB variations."
msgstr "Causa algunes variacions lleus del RGB."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:81
msgid "Random Opacity"
msgstr "Opacitat aleatòria"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:82
msgid ""
"The curve lines get random opacity. This one is barely visible, so for the "
"example I used line width 12 and 100% opacity."
msgstr ""
"Les línies de corba obtindran opacitat aleatòria. Aquesta no serà gaire "
"visible, de manera que per a l'exemple vaig emprar una amplada de línia de "
"12 amb el 100% d'opacitat."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:83
msgid "Distance Opacity"
msgstr "Opacitat de distància"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:84
msgid ""
"The distance based opacity. When you move your pen fast when painting, the "
"opacity will be calculated based on the distance from the center of the "
"effect area."
msgstr ""
"L'opacitat basada en la distància. Quan moveu el llapis amb rapidesa quan "
"pinteu, l'opacitat es calcularà en funció de la distància des del centre de "
"l'àrea de l'efecte."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:85
msgid "Simple Mode"
msgstr "Mode simple"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:86
msgid ""
"This mode exists for performance reasons, and doesn't affect the output in a "
"visible way. Check this for large brushes or thick lines for faster "
"rendering."
msgstr ""
"Aquest mode existeix per raons de rendiment i no afectarà a la sortida de "
"forma visible. Marqueu-la si hi ha pinzells grans o línies gruixudes per a "
"una representació més ràpida."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:88
msgid ""
"What appears to be the connection line is usually made up of an actual "
"connection line and many smaller curve lines. The many small curve lines "
"make up the majority of the line. For this reason, the only time this option "
"will make a visible difference is if you're drawing with 0% or near 0% "
"density, and with a thick line width. The rest of the time, this option "
"won't make a visible difference."
msgstr ""
"El que sembla la línia de connexió generalment es compon d'una línia de "
"connexió real i moltes línies de corba més petites. Les moltes línies de "
"corba petites constitueixen la majoria de la línia. Per aquesta raó, l'única "
"vegada que aquesta opció crearà una diferència visible és si esteu dibuixant "
"amb una densitat del 0% o prop del 0%, i amb una amplada de línia gruixuda. "
"La resta del temps, aquesta opció no crearà una diferència visible."
