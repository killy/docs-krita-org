# Translation of docs_krita_org_reference_manual___tools___multibrush.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-24 16:19+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: clic esquerre del ratolí"

#: ../../<rst_epilog>:40
msgid ""
".. image:: images/icons/multibrush_tool.svg\n"
"   :alt: toolmultibrush"
msgstr ""
".. image:: images/icons/multibrush_tool.svg\n"
"   :alt: eina de pinzell múltiple"

#: ../../reference_manual/tools/multibrush.rst:None
msgid ".. image:: images/tools/Krita-multibrush.png"
msgstr ".. image:: images/tools/Krita-multibrush.png"

#: ../../reference_manual/tools/multibrush.rst:1
msgid "Krita's multibrush tool reference."
msgstr "Referència de l'eina Pinzell múltiple del Krita."

#: ../../reference_manual/tools/multibrush.rst:11
#: ../../reference_manual/tools/multibrush.rst:31
msgid "Symmetry"
msgstr "Simetria"

#: ../../reference_manual/tools/multibrush.rst:11
msgid "Tools"
msgstr "Eines"

#: ../../reference_manual/tools/multibrush.rst:11
msgid "Multibrush"
msgstr "Pinzell múltiple"

#: ../../reference_manual/tools/multibrush.rst:11
msgid "Mandala"
msgstr "Mandala"

#: ../../reference_manual/tools/multibrush.rst:11
msgid "Rotational Symmetry"
msgstr "Simetria rotacional"

#: ../../reference_manual/tools/multibrush.rst:16
msgid "Multibrush Tool"
msgstr "Eina de pinzell múltiple"

#: ../../reference_manual/tools/multibrush.rst:18
msgid "|toolmultibrush|"
msgstr "|toolmultibrush|"

#: ../../reference_manual/tools/multibrush.rst:20
msgid ""
"The Multibrush tool allows you to draw using multiple instances of a "
"freehand brush stroke at once, it can be accessed from the Toolbox docker or "
"with the default shortcut :kbd:`Q`. Using the Multibrush is similar to "
"toggling the :ref:`mirror_tools`, but the Multibrush is more sophisticated, "
"for example it can mirror freehand brush strokes along a rotated axis."
msgstr ""
"L'eina de Pinzell múltiple permet dibuixar utilitzant múltiples instàncies "
"d'un traç del pinzell a mà alçada, s'hi pot accedir des de l'acoblador "
"Quadre d'eines o amb la drecera predeterminada :kbd:`Q`. El seu ús és "
"similar a alternar les :ref:`mirror_tools`, però el Pinzell múltiple és més "
"sofisticat, per exemple, pot reflectir les pinzellades del pinzell a mà "
"alçada al llarg d'un eix de gir."

#: ../../reference_manual/tools/multibrush.rst:22
msgid "The settings for the tool will be found in the tool options dock."
msgstr ""
"Els ajustaments per a l'eina es troben a l'acoblador Opcions de l'eina."

#: ../../reference_manual/tools/multibrush.rst:24
msgid ""
"The multibrush tool has three modes and the settings for each can be found "
"in the tool options dock. Symmetry and mirror reflect over an axis which can "
"be set in the tool options dock. The default axis is the center of the "
"canvas."
msgstr ""
"L'eina de Pinzell múltiple té tres modes i els ajustaments per a cadascun es "
"troben a l'acoblador Opcions de l'eina. La simetria i el mirall es "
"reflecteixen sobre un eix que es pot establir a l'acoblador Opcions de "
"l'eina. L'eix predeterminat és el centre del llenç."

#: ../../reference_manual/tools/multibrush.rst:29
msgid "The three modes are:"
msgstr "Els tres modes són:"

#: ../../reference_manual/tools/multibrush.rst:32
msgid ""
"Symmetry will reflect your brush around the axis at even intervals. The "
"slider determines the number of instances which will be drawn on the canvas."
msgstr ""
"La simetria reflectirà el pinzell al voltant de l'eix a intervals regulars. "
"El control lliscant determinarà el nombre d'instàncies que es dibuixaran en "
"el llenç."

#: ../../reference_manual/tools/multibrush.rst:33
msgid "Mirror"
msgstr "Mirall"

#: ../../reference_manual/tools/multibrush.rst:34
msgid "Mirror will reflect the brush across the X axis, the Y axis, or both."
msgstr "El mirall reflectirà el pinzell sobre l'eix «X», l'eix «Y» o ambdós."

#: ../../reference_manual/tools/multibrush.rst:35
msgid "Translate"
msgstr "Tradueix"

#: ../../reference_manual/tools/multibrush.rst:36
msgid ""
"Translate will paint the set number of instances around the cursor at the "
"radius distance."
msgstr ""
"El traduir pintarà el nombre establert d'instàncies al voltant del cursor a "
"la distància del radi."

#: ../../reference_manual/tools/multibrush.rst:37
msgid "Snowflake"
msgstr "Floc de neu"

#: ../../reference_manual/tools/multibrush.rst:38
msgid ""
"This works as a mirrored symmetry, but is a bit slower than symmetry+toolbar "
"mirror mode."
msgstr ""
"Funciona com una simetria reflectida, però és una mica més lent que el mode "
"d'emmirallar simetria + barra d'eines."

#: ../../reference_manual/tools/multibrush.rst:40
msgid "Copy Translate"
msgstr "Trasllada la còpia"

#: ../../reference_manual/tools/multibrush.rst:40
msgid ""
"This allows you to set the position of the copies relative to your own "
"cursor. To set the position of the copies, first toggle :guilabel:`Add`, and "
"then |mouseleft| the canvas to place copies relative to the multibrush "
"origin. Finally, press :guilabel:`Add` again, and start drawing to see the "
"copy translate in action."
msgstr ""
"Permet establir la posició de les còpies en relació amb el vostre cursor. "
"Per establir la posició de les còpies, primer seleccioneu :guilabel:"
"`Afegeix` i després feu |mouseleft| sobre el llenç per a posar les còpies en "
"relació amb l'origen del Pinzell múltiple. Finalment, premeu :guilabel:"
"`Afegeix` de nou, i comenceu a dibuixar per a veure Trasllada la còpia en "
"acció."

#: ../../reference_manual/tools/multibrush.rst:42
msgid ""
"The assistant and smoothing options work the same as in the :ref:"
"`freehand_brush_tool`, though only on the real brush and not its copies."
msgstr ""
"Les opcions d'assistent i suavitzat funcionen de la mateixa manera que en l':"
"ref:`freehand_brush_tool`, encara que només sobre el pinzell real i no en "
"les seves còpies."
