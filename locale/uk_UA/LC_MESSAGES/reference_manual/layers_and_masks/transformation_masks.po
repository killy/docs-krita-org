# Translation of docs_krita_org_reference_manual___layers_and_masks___transformation_masks.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___layers_and_masks___transformation_masks\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 16:20+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:1
msgid "How to use transformation masks in Krita."
msgstr "Як користуватися масками перетворення у Krita."

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:11
msgid "Layers"
msgstr "Шари"

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:11
msgid "Masks"
msgstr "Маски"

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:11
msgid "Transform"
msgstr "Перетворення"

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:16
msgid "Transformation Masks"
msgstr "Маски перетворення"

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:18
msgid ""
"Rather than working with a brush to affect the mask, transformation masks "
"allow you to transform (move, rotate, shear, scale and perspective) a layer "
"without applying the transform directly to the paint layer and making it "
"permanent."
msgstr ""
"Замість роботи пензлем для створення маски, маски перетворення надають вам "
"змогу перетворити (пересунути, обернути, перекосити, масштабувати або "
"виконати перетворення перспективи) шар без застосування перетворення "
"безпосередньо до шару малювання, отже без внесення руйнівних змін до "
"зображення."

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:20
msgid ""
"In the same way that Filter and Transparency Masks can be attached to a "
"Paint layer and are non-destructive, so too can the Transformation Mask."
msgstr ""
"Маски фільтрування і прозорості може бути пов'язано із шаром малювання для "
"неруйнівного редагування. Так само, із шаром малювання може бути пов'язано і "
"маску перетворення."

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:23
msgid "Adding a Transformation Mask"
msgstr "Додавання маски перетворення"

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:25
msgid "First add a transform mask to an existing layer."
msgstr "Спочатку додайте маску перетворення до наявного шару."

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:26
msgid "Select the transformation tool."
msgstr "Виберіть інструмент перетворення."

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:27
msgid ""
"Select any of the transform modes in the Tools Options dock and, with the "
"transform mask selected, apply them on the layer."
msgstr ""
"Виберіть будь-які з режимів на бічній панелі параметрів інструмента і, з "
"позначеною маскою перетворення, застосуйте їх до шару."

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:28
msgid "Hit apply."
msgstr "Натисніть кнопку «Застосувати»."

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:29
msgid ""
"Toggle the transform visibility to see the difference between the original "
"and the transform applied."
msgstr ""
"Увімкніть і вимкніть видимість перетворення, щоб оцінити різницю між "
"початковим і перетвореним зображенням."

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:33
msgid ""
"Affine transforms, like Move, Rotate, Shear, Scale and Perspective get "
"updated instantly once the original is updated. Other transforms like Warp, "
"Cage and Liquify take up much more processing power, and to not to waste "
"that, Krita only updates those every three seconds."
msgstr ""
"Для афінних перетворень, зокрема пересування, обертання, перекошування, "
"масштабування та перетворення перспективи, результати обробки "
"оновлюватиметься негайно, щойно буде внесено зміни до оригіналу. Для інших "
"перетворень, зокрема вихору, перетворення за ґраткою та розрідження, які "
"потребують значних процесорних потужностей, для заощадження ресурсів системи "
"Krita оновлюватиме результати кожні три секунди."

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:35
msgid ""
"To edit a transform, select the transform mask, and try to use the transform "
"tool on the layer. The transform mode will be the same as the stored "
"transform, regardless of what transform you had selected. If you switch "
"transform modes, the transformation will be undone."
msgstr ""
"Щоб редагувати перетворення, позначте пункт маски перетворення і спробуйте "
"скористатися інструментом перетворення на шарі. Режим перетворення буде тим "
"сами, який було збережено для шару, незалежно від того перетворення, яке ви "
"вибрали. Якщо ви перемкнете режим перетворення, перетворення буде скасовано."
