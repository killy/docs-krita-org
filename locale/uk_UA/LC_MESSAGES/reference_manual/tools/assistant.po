# Translation of docs_krita_org_reference_manual___tools___assistant.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___tools___assistant\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 15:28+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<generated>:1
msgid "Custom Color:"
msgstr "Нетиповий колір:"

#: ../../<rst_epilog>:42
msgid ""
".. image:: images/icons/assistant_tool.svg\n"
"   :alt: toolassistant"
msgstr ""
".. image:: images/icons/assistant_tool.svg\n"
"   :alt: toolassistant"

#: ../../reference_manual/tools/assistant.rst:1
msgid "Krita's assistant tool reference."
msgstr "Довідник із допоміжного інструмента Krita."

#: ../../reference_manual/tools/assistant.rst:12
msgid "Painting Assistants"
msgstr "Допоміжні засоби малювання"

#: ../../reference_manual/tools/assistant.rst:12
msgid "Tools"
msgstr "Інструменти"

#: ../../reference_manual/tools/assistant.rst:17
msgid "Assistant Tool"
msgstr "Допоміжний інструмент"

#: ../../reference_manual/tools/assistant.rst:19
msgid "|toolassistant|"
msgstr "|toolassistant|"

#: ../../reference_manual/tools/assistant.rst:21
msgid ""
"Create, edit, and remove drawing assistants on the canvas. There are a "
"number of different assistants that can be used from this tool. The tool "
"options allow you to add new assistants, and to save/load assistants. To add "
"a new assistant, select a type from the tool options and begin clicking on "
"the canvas. Each assistant is created a bit differently. There are also "
"additional controls on existing assistants that allow you to move and delete "
"them."
msgstr ""
"Створюйте, редагуйте і вилучайте допоміжні засоби малювання на полотні. "
"Існує декілька різних допоміжних інструментів, якими можна користуватися для "
"цього інструмента. Параметри інструмента надають вам змогу додавати нові "
"допоміжні засоби і зберігати або завантажувати допоміжні засоби. Щоб додати "
"новий допоміжний засіб, виберіть тип з параметрів інструмента і почніть "
"клацати на полотні. У кожного допоміжного засобу є свої особливості "
"створення. Також передбачено додаткові засоби керування для наявних "
"допоміжних засобів, за допомогою яких ви можете пересувати і вилучати їх."

#: ../../reference_manual/tools/assistant.rst:23
msgid ""
"The set of assistants on the current canvas can be saved to a \"\\*."
"paintingassistant\" file using the :guilabel:`Save` button in the tool "
"options. These assistants can then be loaded onto a different canvas using "
"the Open button. This functionality is also useful for creating copies of "
"the same drawing assistant(s) on the current canvas."
msgstr ""
"Набір допоміжних засобів на поточному полотні можна зберегти до файла \"\\*."
"paintingassistant\" за допомогою кнопки :guilabel:`Зберегти` у параметрах "
"інструмента. Ці допоміжні засоби потім можна завантажити до іншого полотна "
"за допомогою кнопки :guilabel:`Відкрити`. Ця функціональна можливість також "
"є корисною для створення копій тих сами допоміжних засобів малювання на "
"поточному полотні."

#: ../../reference_manual/tools/assistant.rst:25
msgid "Check :ref:`painting_with_assistants` for more information."
msgstr ""
"Ознайомтеся із розділом :ref:`painting_with_assistants`, щоб дізнатися "
"більше."

#: ../../reference_manual/tools/assistant.rst:28
msgid "Tool Options"
msgstr "Параметри інструмента"

#: ../../reference_manual/tools/assistant.rst:33
msgid "Global Color:"
msgstr "Загальний колір:"

#: ../../reference_manual/tools/assistant.rst:33
msgid ""
"Global color allows you to set the color and opacity of all assistants at "
"once."
msgstr ""
"За допомогою пункту загального кольору ви можете встановити колір і "
"непрозорість для усіх допоміжних засобів одночасно."

#: ../../reference_manual/tools/assistant.rst:38
msgid ""
"Custom color allows you to set a color and opacity per assistant, allowing "
"for different colors on an assistant. To use this functionality, first "
"'select' an assistant by tapping its move widget. Then go to the tool "
"options docker to see the :guilabel:`Custom Color` check box. Check that, "
"and then use the opacity and color buttons to pick either for this "
"particular assistant."
msgstr ""
"Нетиповий колір надає вам змогу встановити колір і непрозорість для кожної "
"допоміжної лінії. Різні допоміжні засоби можуть мати різні кольори. Щоб "
"скористатися цією функціональною можливістю, спершу позначте допоміжний "
"засіб натисканням його віджета пересування. Далі, перейдіть до бічної панелі "
"параметрів інструмента, де буде показано пункт :guilabel:`Нетиповий колір`. "
"Позначте цей пункт, а потім скористайтеся кнопками непрозорості і кольору, "
"щоб вибрати відповідні параметри для позначеної допоміжної лінії."
