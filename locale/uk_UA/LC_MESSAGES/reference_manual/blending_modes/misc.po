# Translation of docs_krita_org_reference_manual___blending_modes___misc.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___blending_modes___misc\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:48+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/blending_modes/misc.rst:1
msgid ""
"Page about the miscellaneous blending modes in Krita: Bumpmap, Combine "
"Normal Map, Copy Red, Copy Green, Copy Blue, Copy and Dissolve."
msgstr ""
"Розділ щодо інших режимів змішування у Krita: Рельєф, Створити нормальну "
"карту, Копіювання червоного, Копіювання зеленого, Копіювання синього, "
"Копіювання та Розчинити."

#: ../../reference_manual/blending_modes/misc.rst:15
msgid "Misc"
msgstr "Інше"

#: ../../reference_manual/blending_modes/misc.rst:17
msgid "Bumpmap (Blending Mode)"
msgstr "Рельєф (режим змішування)"

#: ../../reference_manual/blending_modes/misc.rst:21
msgid "Bumpmap"
msgstr "Рельєф"

#: ../../reference_manual/blending_modes/misc.rst:23
msgid "This filter seems to both multiply and respect the alpha of the input."
msgstr ""
"Використання цього фільтра призводить до множення кольорів із врахуванням "
"прозорості вхідних пікселів."

#: ../../reference_manual/blending_modes/misc.rst:25
#: ../../reference_manual/blending_modes/misc.rst:30
msgid "Combine Normal Map"
msgstr "Створити нормальне картографування"

#: ../../reference_manual/blending_modes/misc.rst:25
msgid "Normal Map"
msgstr "Нормальне картографування"

#: ../../reference_manual/blending_modes/misc.rst:32
msgid ""
"Mathematically robust blending mode for normal maps, using `Reoriented "
"Normal Map Blending <https://blog.selfshadow.com/publications/blending-in-"
"detail/>`_."
msgstr ""
"Математично стійкий режим змішування для нормальних карт з використанням "
"`переорієнтованого змішування нормальних карт <https://blog.selfshadow.com/"
"publications/blending-in-detail/>`_."

#: ../../reference_manual/blending_modes/misc.rst:34
msgid "Copy (Blending Mode)"
msgstr "Копіювання (режим змішування)"

#: ../../reference_manual/blending_modes/misc.rst:38
msgid "Copy"
msgstr "Копіювати"

#: ../../reference_manual/blending_modes/misc.rst:40
msgid ""
"Copies the previous layer exactly. Useful for when using filters and filter-"
"masks."
msgstr ""
"Точно копіює попередній шар. Корисно, коли використовуються фільтри і "
"фільтри-маски."

#: ../../reference_manual/blending_modes/misc.rst:46
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:46
msgid "Left: **Normal**. Right: **Copy**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Копіювання**."

#: ../../reference_manual/blending_modes/misc.rst:48
msgid "Copy Red"
msgstr "Копіювання червоного"

#: ../../reference_manual/blending_modes/misc.rst:48
msgid "Copy Green"
msgstr "Копіювання зеленого"

#: ../../reference_manual/blending_modes/misc.rst:48
msgid "Copy Blue"
msgstr "Копіювання синього"

#: ../../reference_manual/blending_modes/misc.rst:54
msgid "Copy Red, Green, Blue"
msgstr "Копіювання червоного, зеленого, синього"

#: ../../reference_manual/blending_modes/misc.rst:56
msgid ""
"This is a blending mode that will just copy/blend a source channel to a "
"destination channel. Specifically, it will take the specific channel from "
"the upper layer and copy that over to the lower layers."
msgstr ""
"Це режим змішування, який просто копіює/змішує канал джерела до каналу "
"призначення. Якщо точніше, у цьому режимі програма бере певний канал з "
"верхнього шару і копіює його значення до нижчих шарів."

#: ../../reference_manual/blending_modes/misc.rst:59
msgid ""
"So, if you want the brush to only affect the red channel, set the blending "
"mode to 'copy red'."
msgstr ""
"Отже, якщо ви хочете, щоб пензель впливав лише на канал червоного кольору, "
"встановіть режим змішування :guilabel:`Копіювати червоний`."

#: ../../reference_manual/blending_modes/misc.rst:65
msgid ""
".. image:: images/blending_modes/misc/Krita_Filter_layer_invert_greenchannel."
"png"
msgstr ""
".. image:: images/blending_modes/misc/Krita_Filter_layer_invert_greenchannel."
"png"

#: ../../reference_manual/blending_modes/misc.rst:65
msgid "The copy red, green and blue blending modes also work on filter-layers."
msgstr ""
"Режими змішування із копіюванням червоного, зеленого та синього також "
"працюють на шарах фільтрування."

#: ../../reference_manual/blending_modes/misc.rst:67
msgid ""
"This can also be done with filter layers. So if you quickly want to flip a "
"layer's green channel, make an invert filter layer with 'copy green' above "
"it."
msgstr ""
"Крім того, результату можна досягти за допомогою шарів фільтрування. Отже, "
"якщо ви хочете швидко віддзеркалити канал зеленого кольору шару, створіть "
"шар фільтрування із інверсією за допомогою створення шару фільтрування :"
"guilabel:`Копіювати зелений` над шаром, який слід фільтрувати."

#: ../../reference_manual/blending_modes/misc.rst:72
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Red_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Red_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:72
msgid "Left: **Normal**. Right: **Copy Red**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Копіювання червоного**."

#: ../../reference_manual/blending_modes/misc.rst:78
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Green_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Green_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:78
msgid "Left: **Normal**. Right: **Copy Green**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Копіювання зеленого**."

#: ../../reference_manual/blending_modes/misc.rst:84
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Blue_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Blue_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:84
msgid "Left: **Normal**. Right: **Copy Blue**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Копіювання синього**."

#: ../../reference_manual/blending_modes/misc.rst:86
#: ../../reference_manual/blending_modes/misc.rst:90
msgid "Dissolve"
msgstr "Розчинити"

#: ../../reference_manual/blending_modes/misc.rst:92
msgid ""
"Instead of using transparency, this blending mode will use a random "
"dithering pattern to make the transparent areas look sort of transparent."
msgstr ""
"Замість використання прозорості, цей режим змішування використовує "
"випадковий розсіяний візерунок, роблячи прозорі ділянки розсіяним набором "
"прозорих ділянок."

#: ../../reference_manual/blending_modes/misc.rst:97
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Dissolve_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Dissolve_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:97
msgid "Left: **Normal**. Right: **Dissolve**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Розчинити**."
