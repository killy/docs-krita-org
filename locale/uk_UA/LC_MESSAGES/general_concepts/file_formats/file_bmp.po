# Translation of docs_krita_org_general_concepts___file_formats___file_bmp.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___file_formats___file_bmp\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 09:29+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../general_concepts/file_formats/file_bmp.rst:1
msgid "The Bitmap file format."
msgstr "Формат растрових файлів."

#: ../../general_concepts/file_formats/file_bmp.rst:10
msgid "*.bmp"
msgstr "*.bmp"

#: ../../general_concepts/file_formats/file_bmp.rst:10
msgid "BMP"
msgstr "BMP"

#: ../../general_concepts/file_formats/file_bmp.rst:10
msgid "Bitmap Fileformat"
msgstr "Формат растрових файлів"

#: ../../general_concepts/file_formats/file_bmp.rst:15
msgid "\\*.bmp"
msgstr "\\*.bmp"

#: ../../general_concepts/file_formats/file_bmp.rst:17
msgid ""
"``.bmp``, or Bitmap, is the simplest raster file format out there, and, "
"being patent-free, most programs can open and save bitmap files."
msgstr ""
"``.bmp``, або Bitmap, є найпростішим форматом растрових файлів і, оскільки "
"його не захищено патентним правом, більшість програм можуть відкривати і "
"зберігати дані у форматі bmp."

#: ../../general_concepts/file_formats/file_bmp.rst:19
msgid ""
"However, most programs don't compress bitmap files, leading to BMP having a "
"reputation for being very heavy. If you need a lossless file format, we "
"actually recommend :ref:`file_png`."
msgstr ""
"Втім, у більшості програм файли bmp не стискають. Через це у BMP склалася "
"репутація дуже неоптимального за розміром формату файлів. Якщо вам потрібно "
"зберегти дані зображення до файла без втрат, рекомендуємо вам скористатися :"
"ref:`file_png`."
