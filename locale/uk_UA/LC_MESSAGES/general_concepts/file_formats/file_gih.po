# Translation of docs_krita_org_general_concepts___file_formats___file_gih.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___file_formats___file_gih\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 09:32+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../general_concepts/file_formats/file_gih.rst:1
msgid "The Gimp Image Hose file format in Krita."
msgstr "Формат файлів футляра зображень GIMP (GIH) у Krita."

#: ../../general_concepts/file_formats/file_gih.rst:10
msgid "Image Hose"
msgstr "Футляр пензлів"

#: ../../general_concepts/file_formats/file_gih.rst:10
msgid "Gimp Image Hose"
msgstr "Футляр пензлів GIMP"

#: ../../general_concepts/file_formats/file_gih.rst:10
msgid "GIH"
msgstr "GIH"

#: ../../general_concepts/file_formats/file_gih.rst:10
msgid "*.gih"
msgstr "*.gih"

#: ../../general_concepts/file_formats/file_gih.rst:15
msgid "\\*.gih"
msgstr "\\*.gih"

#: ../../general_concepts/file_formats/file_gih.rst:17
msgid ""
"The GIMP image hose format. Krita can open and save these, as well as import "
"via the :ref:`predefined brush tab <predefined_brush_tip>`."
msgstr ""
"Формат футляра зображень GIMP. Krita може відкривати і зберігати файли у "
"цьому форматі, а також імпортувати дані за допомогою :ref:`вкладки "
"стандартних пензлів <predefined_brush_tip>`."

#: ../../general_concepts/file_formats/file_gih.rst:19
msgid ""
"Image Hose means that this file format allows you to store multiple images "
"and then set some options to make it specify how to output the multiple "
"images."
msgstr ""
"«Футляр» означає, що у файлі цього формату можна зберігати декілька "
"зображень, встановлюючи певні параметри, які визначають спосіб виведення цих "
"зображень для показу."

#: ../../general_concepts/file_formats/file_gih.rst:25
msgid ".. image:: images/brushes/Gih-examples.png"
msgstr ".. image:: images/brushes/Gih-examples.png"

#: ../../general_concepts/file_formats/file_gih.rst:25
msgid "From top to bottom: Incremental, Pressure and Random"
msgstr "Згори вниз: «Доповнювальний», «Тиск» і «Випадковий»"

#: ../../general_concepts/file_formats/file_gih.rst:27
msgid "Gimp image hose format options:"
msgstr "Варіанти формату футляра зображень GIMP:"

#: ../../general_concepts/file_formats/file_gih.rst:29
msgid "Constant"
msgstr "Сталий"

#: ../../general_concepts/file_formats/file_gih.rst:30
msgid "This'll use the first image, no matter what."
msgstr "Цей варіант означає «перше зображення», байдуже яке."

#: ../../general_concepts/file_formats/file_gih.rst:31
msgid "Incremental"
msgstr "Доповнювальний"

#: ../../general_concepts/file_formats/file_gih.rst:32
msgid ""
"This'll paint the image layers in sequence. This is good for images that can "
"be strung together to create a pattern."
msgstr ""
"Цей варіант означає «малювати шари зображення як послідовність». Цей варіант "
"є придатним для зображень, які можна накласти одне на одне для створення "
"візерунка."

#: ../../general_concepts/file_formats/file_gih.rst:33
msgid "Pressure"
msgstr "Тиск"

#: ../../general_concepts/file_formats/file_gih.rst:34
msgid ""
"This'll paint the images depending on pressure. This is good for brushes "
"imitating the hairs of a natural brush."
msgstr ""
"Цей варіант передбачає малювання зображень залежно від тиску. Придатний для "
"пензлів, які імітують волосинки справжнього пензля."

#: ../../general_concepts/file_formats/file_gih.rst:35
msgid "Random"
msgstr "Випадковий"

#: ../../general_concepts/file_formats/file_gih.rst:36
msgid ""
"This'll draw the images randomly. This is good for image-collections used in "
"speedpainting as well as images that generate texture. Or perhaps more "
"graphical symbols."
msgstr ""
"Цей варіант передбачає випадкове малювання зображень. Придатний для збірок "
"зображень, які використовуються у малюванні на швидкість, а також зображень "
"для створення текстури, або, можливо, більш графічних символів."

#: ../../general_concepts/file_formats/file_gih.rst:38
msgid "Angle"
msgstr "Кут"

#: ../../general_concepts/file_formats/file_gih.rst:38
msgid "This'll use the dragging angle to determine with image to draw."
msgstr ""
"Цей варіант використовує кут інерції для визначення зображення, яке слід "
"намалювати."

#: ../../general_concepts/file_formats/file_gih.rst:40
msgid ""
"When exporting a Krita file as a ``.gih``, you will also get the option to "
"set the default spacing, the option to set the name (very important for "
"looking it up in the UI) and the ability to choose whether or not to "
"generate the mask from the colors."
msgstr ""
"При експортуванні файла Krita у форматі ``.gih``, ви також зможете "
"встановити типовий інтервал, назву (дуже важливо для пошуку зображення у "
"графічному інтерфейсі) та можливість визначити, чи слід створювати маску з "
"кольорів."

#: ../../general_concepts/file_formats/file_gih.rst:43
msgid "Use Color as Mask"
msgstr "Використовувати колір як маску"

#: ../../general_concepts/file_formats/file_gih.rst:43
msgid ""
"This'll turn the darkest values of the image as the ones that paint, and the "
"whitest as transparent. Untick this if you are using colored images for the "
"brush."
msgstr ""
"За допомогою цього пункту можна перетворити найтемніші кольори зображення на "
"повністю непрозорі ділянки, а найбіліші — на прозорі. Позначте цей пункт, "
"якщо використовуєте розфарбовані зображення для пензля."

#: ../../general_concepts/file_formats/file_gih.rst:45
msgid ""
"We have a :ref:`Krita Brush tip page <brush_tip_animated_brush>` on how to "
"create your own gih brush."
msgstr ""
"Ми присвятили спеціальний :ref:`розділ підказок «Кінчики пензлів Krita» "
"<brush_tip_animated_brush>` тому, як створити власний пензель gih."
