# Translation of docs_krita_org_user_manual___introduction_from_other_software.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_user_manual___introduction_from_other_software\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-23 11:39+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../user_manual/introduction_from_other_software.rst:5
msgid "Introduction Coming From Other Software"
msgstr "Вступ до переходу з іншого програмного забезпечення"

#: ../../user_manual/introduction_from_other_software.rst:7
msgid ""
"Krita is not the only digital painting application in the world. Because we "
"know our users might be approaching Krita with their experience from using "
"other software, we have made guides to illustrate differences."
msgstr ""
"Krita — не єдина програма для цифрового малювання у світі. Оскільки багато "
"наших користувачів починають малювати у Krita вже маючи досвід малювання у "
"інших програмах, ми хотіли б присвятити частину цього підручника опису "
"відмінностей, які слід мати на увазі."

#: ../../user_manual/introduction_from_other_software.rst:10
msgid "Contents:"
msgstr "Вміст:"
